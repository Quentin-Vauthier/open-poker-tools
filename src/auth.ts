import { Lucia } from 'lucia';

import { db } from '@/db';
import { playersTable, sessionTable } from '@/db/schema';
import { DrizzlePostgreSQLAdapter } from '@lucia-auth/adapter-drizzle';

import { Role } from '@/types';

const adapter = new DrizzlePostgreSQLAdapter(db, sessionTable, playersTable);

export const lucia = new Lucia(adapter, {
  sessionCookie: {
    expires: false,
    attributes: {
      secure: process.env.NODE_ENV === 'production',
    },
  },
  getUserAttributes: (attributes) => {
    return {
      // attributes has the type of DatabaseUserAttributes
      email: attributes.email,
      nickname: attributes.nickname,
      role: attributes.role,
      avatar: attributes.avatar,
    };
  },
  getSessionAttributes: (attributes) => {
    return {
      email: attributes.email,
      nickname: attributes.nickname,
      role: attributes.role,
      avatar: attributes.avatar,
    };
  },
});

// ! IMPORTANT !
declare module 'lucia' {
  interface Register {
    Lucia: typeof lucia;
    UserId: number;
    DatabaseUserAttributes: DatabaseUserAttributes;
    DatabaseSessionAttributes: DatabaseSessionAttributes;
  }
}

interface DatabaseUserAttributes {
  email: string;
  nickname: string;
  role: Role;
  avatar?: string;
}

interface DatabaseSessionAttributes extends DatabaseUserAttributes {}
