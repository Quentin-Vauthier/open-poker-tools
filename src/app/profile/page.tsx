'use client';

import { notFound, redirect } from 'next/navigation';
import { Controller, useForm } from 'react-hook-form';
import * as z from 'zod';

import AvatarEditor from '@/app/profile/avatar-editor';
import { useSession } from '@/components/SessionContext';
import { Button } from '@/components/ui/button';
import { Card, CardContent, CardHeader, CardTitle } from '@/components/ui/card';
import { Form, FormField, FormItem } from '@/components/ui/form';
import { Input } from '@/components/ui/input';
import OPTFormItem from '@/components/ui/OPTFormItem';
import { zodResolver } from '@hookform/resolvers/zod';
import { useState } from 'react';
import axios, { HttpStatusCode } from 'axios';
import { toast } from 'sonner';
import { ErrorResponse } from '../api/types';
import { Icons } from '@/components/Icons';
import { cn } from '@/lib/utils';

// Define the schema for form validation
const profileSchema = z.object({
  email: z.string().email({ message: 'Invalid email address' }),
  nickname: z
    .string()
    .min(2, { message: 'Nickname must be at least 2 characters' })
    .max(50, { message: 'Nickname must be at most 50 characters' }),
});

// Infer the type from the schema
type ProfileFormValues = z.infer<typeof profileSchema>;

export default function ProfilePage() {
  const { user } = useSession();

  const [avatarFile, setAvatarFile] = useState<File | null>(null);
  const [isUpdating, setIsUpdating] = useState(false);
  const [isAvatarUploading, setIsAvatarUploading] = useState(false);

  if (!user) {
    notFound();
  }

  // Initialize the form with React Hook Form and Zod resolver
  const form = useForm<ProfileFormValues>({
    resolver: zodResolver(profileSchema),
    defaultValues: {
      email: user.email,
      nickname: user.nickname,
    },
  });

  const uploadAvatarFile = async (file: File): Promise<string | null> => {
    setIsAvatarUploading(true);
    const formData = new FormData();
    formData.append('file', file);
    try {
      const res = await fetch('/api/avatar', {
        method: 'POST',
        body: formData,
      });
      const { url } = await res.json();
      toast.success('Avatar updated successfully');
      return url;
    } catch (error) {
      toast.error('Failed to upload avatar', {
        description:
          'Please try again later, if the problem persists, contact support.',
      });
      return null;
    } finally {
      setIsAvatarUploading(false);
    }
  };

  const onSubmit = async (data: ProfileFormValues) => {
    try {
      // If an avatar file was selected, upload it and update the avatar URL
      if (avatarFile) {
        uploadAvatarFile(avatarFile).then((url) => {
          if (url) setAvatarFile(null);
        });
      }
      if (data.email === user.email && data.nickname === user.nickname) return;
      setIsUpdating(true);
      await axios.put('/api/profile', {
        email: data.email,
        nickname: data.nickname,
      });

      toast.success('Profile information updated successfully');
    } catch (err) {
      if (axios.isAxiosError(err)) {
        const { response } = err;
        if (!response) {
          return toast.error('Failed to update profile', {
            description: 'Please check your internet connection and try again.',
          });
        }
        const status = response?.status;
        switch (status) {
          case HttpStatusCode.Conflict:
            if (
              (response.data as ErrorResponse).errors[0] ===
              'Email already in use'
            ) {
              form.setError('email', {
                type: 'manual',
                message: 'Email already in use',
              });
            } else if (
              (response.data as ErrorResponse).errors[0] ===
              'Nickname already in use'
            ) {
              form.setError('nickname', {
                type: 'manual',
                message: 'Nickname already in use',
              });
            }
            return;
          case HttpStatusCode.BadRequest:
            return toast.error('Invalid data', {
              description: 'Please check the form for errors and try again.',
            });
          case HttpStatusCode.NotFound:
            return toast.error('Failed to update profile', {
              description: 'Could not find the profile to update.',
            });
          case HttpStatusCode.Unauthorized:
            toast.error('Unauthorized', {
              description: 'You must be logged in to update your profile.',
            });
            redirect('/login');
          default:
            break;
        }
      }
      toast.error('Failed to update profile', {
        description:
          'Please try again later, if the problem persists, contact support.',
      });
    } finally {
      setIsUpdating(false);
    }
  };

  return (
    <div className='container mx-auto p-4'>
      <Card className='w-full max-w-md mx-auto'>
        <CardHeader>
          <CardTitle className='text-2xl font-bold text-center'>
            Profile
          </CardTitle>
        </CardHeader>
        <CardContent>
          <Form {...form}>
            <form onSubmit={form.handleSubmit(onSubmit)} className='space-y-4'>
              <div
                className={cn('flex flex-col items-center mb-6', {
                  'animate-pulse': isAvatarUploading,
                })}>
                <AvatarEditor
                  src={
                    user.avatar
                      ? `${process.env.NEXT_PUBLIC_GCS_BUCKET_URL}${user.avatar}`
                      : undefined
                  }
                  onFileSelect={(file) => setAvatarFile(file)}
                />
              </div>
              <FormField
                control={form.control}
                name='email'
                render={({ field }) => (
                  <OPTFormItem label='Email' required>
                    <Input placeholder='Enter your email' {...field} />
                  </OPTFormItem>
                )}
              />
              <FormField
                control={form.control}
                name='nickname'
                render={({ field }) => (
                  <OPTFormItem label='Nickname' required>
                    <Input placeholder='Enter your nickname' {...field} />
                  </OPTFormItem>
                )}
              />
              <Button type='submit' className='w-full' disabled={isUpdating}>
                {isUpdating ? <Icons.spinner className='h-4 w-4 mr-2' /> : null}
                Save Profile
              </Button>
            </form>
          </Form>
        </CardContent>
      </Card>
    </div>
  );
}
