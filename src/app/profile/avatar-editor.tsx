import { useRef, useState } from 'react';
import { Button } from '@/components/ui/button';
import {
  Avatar as AvatarUI,
  AvatarImage,
  AvatarFallback,
} from '@/components/ui/avatar';
import { Pencil } from 'lucide-react';
import { useSession } from '@/components/SessionContext';
import { notFound } from 'next/navigation';

interface AvatarEditorProps {
  src?: string;
  onFileSelect: (file: File) => void;
  disabled?: boolean;
}

export default function AvatarEditor({
  src,
  onFileSelect,
  disabled = false,
}: AvatarEditorProps) {
  const { user } = useSession();

  if (!user) notFound();

  const fileInputRef = useRef<HTMLInputElement>(null);
  const [previewSrc, setPreviewSrc] = useState(src);

  const handleClick = () => {
    fileInputRef.current?.click();
  };

  const handleFileSelect = (e: React.ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files?.[0];
    if (!file) return;

    // Create a preview of the selected image
    const reader = new FileReader();
    reader.onloadend = () => {
      setPreviewSrc(reader.result as string);
    };
    reader.readAsDataURL(file);

    // Pass the file to the parent component
    onFileSelect(file);
  };

  return (
    <div className='relative cursor-pointer group' onClick={handleClick}>
      <AvatarUI className='w-32 h-32'>
        <AvatarImage src={previewSrc} alt='Profile picture' />
        <AvatarFallback>{user.nickname.charAt(0)}</AvatarFallback>
      </AvatarUI>
      <div className='absolute inset-0 flex items-center justify-center bg-black bg-opacity-40 rounded-full opacity-0 group-hover:opacity-100 transition-opacity'>
        <Pencil className='w-8 h-8 text-white' />
      </div>
      <input
        ref={fileInputRef}
        type='file'
        className='hidden'
        accept='image/*'
        onChange={handleFileSelect}
        disabled={disabled}
      />
    </div>
  );
}
