export default function PageNotFound() {
  return (
    <section className='flex flex-grow flex-col items-center justify-center h-full'>
      <h1 className='text-4xl font-bold'>404</h1>
      <p className='text-xl'>Page Not Found</p>
    </section>
  );
}
