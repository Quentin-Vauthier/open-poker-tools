'use client';

import axios from 'axios';
import ReactModal from 'react-modal';
import { LayoutGrid, LayoutList, Pencil } from 'lucide-react';
import { useMemo, useState } from 'react';

import EventDetail from '@/app/(admin)/events/EventDetail';
import { GetEventsResponse, Event } from '@/app/api/events/types';
import { Icons } from '@/components/Icons';
import { Button } from '@/components/ui/button';
import {
  Card,
  CardContent,
  CardFooter,
  CardHeader,
  CardTitle,
} from '@/components/ui/card';
import { Input } from '@/components/ui/input';
import { Label } from '@/components/ui/label';
import { Switch } from '@/components/ui/switch';
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from '@/components/ui/table';
import { useQuery } from '@tanstack/react-query';
import Modal from '@/components/Modal';

ReactModal.setAppElement('#main');

export default function Component() {
  const [isCardLayout, setIsCardLayout] = useState<boolean>(false);
  const [searchTerm, setSearchTerm] = useState<string>('');
  const [selectedEvent, setSelectedEvent] = useState<Event | null>(null);
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false);

  const {
    data: events,
    isLoading,
    isError,
    refetch,
  } = useQuery({
    queryKey: ['events'],
    queryFn: () => fetchEvents(),
  });

  const filteredEvents = useMemo(() => {
    if (!events) return [];
    return events.filter(
      (event) =>
        event.code.toLowerCase().includes(searchTerm.toLowerCase()) ||
        event.title.toLowerCase().includes(searchTerm.toLowerCase()) ||
        event.description.toLowerCase().includes(searchTerm.toLowerCase())
    );
  }, [events, searchTerm]);

  const handleModalClose = () => {
    setIsModalOpen(false);
    setSelectedEvent(null);
  };

  const handleEdit = (event: Event) => {
    setSelectedEvent(event);
    setIsModalOpen(true);
  };

  if (isLoading) {
    return (
      <div className='flex flex-grow items-center justify-center text-lg gap-2'>
        <Icons.spinner className='animate-spin h-8 w-8' />
        Loading events...
      </div>
    );
  }

  if (isError) {
    return (
      <div className='flex flex-grow items-center justify-center text-lg'>
        Error fetching events, please try again later
      </div>
    );
  }

  return (
    <div className='container mx-auto p-6'>
      <div className='flex justify-between items-center mb-6'>
        <h1 className='text-2xl font-bold'>Manage Application Events</h1>
        <div className='flex items-center space-x-2'>
          <LayoutList
            className={`h-4 w-4 ${
              !isCardLayout ? 'text-primary' : 'text-muted-foreground'
            }`}
          />
          <Switch
            id='layout-switch'
            checked={isCardLayout}
            onCheckedChange={setIsCardLayout}
          />
          <LayoutGrid
            className={`h-4 w-4 ${
              isCardLayout ? 'text-primary' : 'text-muted-foreground'
            }`}
          />
          <Label htmlFor='layout-switch' className='sr-only'>
            Toggle layout
          </Label>
        </div>
      </div>
      <div className='flex justify-between items-center mb-6'>
        <Input
          type='text'
          placeholder='Search events...'
          className='max-w-sm'
          value={searchTerm}
          onChange={(e) => setSearchTerm(e.target.value)}
        />
      </div>
      {isCardLayout ? (
        <div className='grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6'>
          {filteredEvents.map((event) => (
            <Card key={event.code} className='flex flex-col justify-between'>
              <div>
                <CardHeader>
                  <CardTitle className='flex justify-between items-center'>
                    {event.title}
                  </CardTitle>
                  <p className='text-sm text-muted-foreground'>{event.code}</p>
                </CardHeader>
                <CardContent>
                  <p className='text-sm text-muted-foreground'>
                    {event.description}
                  </p>
                </CardContent>
              </div>
              <CardFooter className='flex justify-end space-x-2'>
                <Button
                  variant='outline'
                  size='sm'
                  onClick={() => handleEdit(event)}>
                  <Pencil className='h-4 w-4 mr-2' />
                  Edit
                </Button>
              </CardFooter>
            </Card>
          ))}
        </div>
      ) : (
        <Table>
          <TableHeader>
            <TableRow>
              <TableHead>Code</TableHead>
              <TableHead>Title</TableHead>
              <TableHead>Description</TableHead>
              <TableHead>Actions</TableHead>
            </TableRow>
          </TableHeader>
          <TableBody>
            {filteredEvents.map((event) => (
              <TableRow key={event.code}>
                <TableCell className='font-medium'>{event.code}</TableCell>
                <TableCell>{event.title}</TableCell>
                <TableCell>{event.description}</TableCell>
                <TableCell>
                  <Button
                    variant='ghost'
                    size='icon'
                    className='mr-2'
                    onClick={() => handleEdit(event)}>
                    <Pencil className='h-4 w-4' />
                    <span className='sr-only'>Edit</span>
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      )}
      <Modal
        isOpen={isModalOpen}
        onRequestClose={handleModalClose}
        size='extra-large'>
        <EventDetail
          event={selectedEvent || undefined}
          onBack={handleModalClose}
          onSave={() => refetch()}
        />
      </Modal>
    </div>
  );

  async function fetchEvents(): Promise<GetEventsResponse> {
    try {
      const { data } = await axios.get<GetEventsResponse>('/api/events');
      return data;
    } catch (error) {
      console.error(error);
      return [];
    }
  }
}
