import axios, { HttpStatusCode } from 'axios';
import { ArrowLeft, Save } from 'lucide-react';
import { useForm } from 'react-hook-form';
import { toast } from 'sonner';
import { useState } from 'react';

import { Event } from '@/app/api/events/types';
import { Button } from '@/components/ui/button';
import {
  Card,
  CardContent,
  CardFooter,
  CardHeader,
  CardTitle,
} from '@/components/ui/card';
import { Form, FormField, FormMessage } from '@/components/ui/form';
import { Input } from '@/components/ui/input';
import OPTFormItem from '@/components/ui/OPTFormItem';
import { Textarea } from '@/components/ui/textarea';
import { z } from 'zod';
import { zodResolver } from '@hookform/resolvers/zod';
import { Icons } from '@/components/Icons';

const emptyEvent: Partial<Event> = {
  code: '',
  title: '',
  description: '',
};

const eventSchema = z.object({
  code: z.string(),
  title: z.string(),
  description: z.string(),
});

interface EventDetailProps {
  event?: Partial<Event>;
  onBack: () => void;
  onSave?: (event: Event) => void;
}

export default function EventDetail({
  event = emptyEvent,
  onBack,
  onSave,
}: EventDetailProps) {
  const [isFetching, setIsFetching] = useState(false);

  const form = useForm<Event>({
    defaultValues: event,
    resolver: zodResolver(eventSchema),
  });

  const onSubmit = updateEvent;

  async function updateEvent(data: Event) {
    try {
      setIsFetching(true);
      const { status } = await axios.put(`/api/events/${event.code}`, data);

      if (status === HttpStatusCode.Ok) {
        toast.success('Event updated successfully');
        if (onSave) onSave(data);
        onBack();
        return;
      }

      toast.error(
        'Unexpected response from the server, please contact support'
      );
    } catch (error) {
      if (!axios.isAxiosError(error)) {
        toast.error('An error occurred while updating the event', {
          description: 'Please try again later or contact support',
        });
        return;
      }

      const status = error.response?.status;
      if (status === HttpStatusCode.NotFound) {
        form.setError('code', {
          message:
            'It seems the event no longer exists ? Try refreshing the page',
        });
        return;
      }

      if (!status && error.request) {
        toast.error(
          'No response from the server, please check your connection or try again later'
        );
        return;
      }

      toast.error('An unexpected error occurred while updating the event', {
        description: 'Please try again later or contact support',
      });
    } finally {
      setIsFetching(false);
    }
  }

  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)}>
        <Button variant='ghost' onClick={onBack} className='mb-4' type='button'>
          <ArrowLeft className='mr-2 h-4 w-4' />
          Back to Events
        </Button>
        <Card className='w-full max-w-2xl mx-auto'>
          <CardHeader>
            <CardTitle className='flex justify-between items-center'>
              Edit Event
            </CardTitle>
          </CardHeader>
          <CardContent className='space-y-4'>
            <FormField
              control={form.control}
              name='code'
              render={({ field }) => (
                <OPTFormItem label='Code' required>
                  <Input {...field} disabled />
                </OPTFormItem>
              )}
            />
            <FormField
              control={form.control}
              name='title'
              render={({ field }) => (
                <OPTFormItem label='Title' required>
                  <Input {...field} />
                </OPTFormItem>
              )}
            />
            <FormField
              control={form.control}
              name='description'
              render={({ field }) => (
                <OPTFormItem label='Description'>
                  <Textarea {...field} />
                </OPTFormItem>
              )}
            />
            <FormMessage>{form.formState.errors.root?.message}</FormMessage>
          </CardContent>
          <CardFooter>
            <Button className='ml-auto' type='submit' disabled={isFetching}>
              {isFetching && <Icons.spinner className='mr-2 h-5 w-5' />}
              {!isFetching && <Save className='mr-2 h-5 w-5' />}
              Save Changes
            </Button>
          </CardFooter>
        </Card>
      </form>
    </Form>
  );
}
