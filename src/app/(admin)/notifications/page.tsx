'use client';

import axios from 'axios';
import ReactModal from 'react-modal';
import {
  BadgePlus,
  LayoutGrid,
  LayoutList,
  Pencil,
  Trash2,
} from 'lucide-react';
import { useMemo, useState } from 'react';

import NotificationDetail from '@/app/(admin)/notifications/NotificationDetail';
import {
  GetNotificationsResponse,
  Notification,
} from '@/app/api/notifications/types';
import { Icons } from '@/components/Icons';
import { Badge } from '@/components/ui/badge';
import { Button } from '@/components/ui/button';
import {
  Card,
  CardContent,
  CardFooter,
  CardHeader,
  CardTitle,
} from '@/components/ui/card';
import { Input } from '@/components/ui/input';
import { Label } from '@/components/ui/label';
import { Switch } from '@/components/ui/switch';
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from '@/components/ui/table';
import { useQuery } from '@tanstack/react-query';
import Modal from '@/components/Modal';
import { toast } from 'sonner';
import { ErrorResponse } from '@/app/api/types';

ReactModal.setAppElement('#main');

export default function Component() {
  const [isCardLayout, setIsCardLayout] = useState<boolean>(false);
  const [searchTerm, setSearchTerm] = useState<string>('');
  const [selectedNotification, setSelectedNotification] =
    useState<Notification | null>(null);
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
  const [deletingCode, setDeletingCode] = useState<string | null>(null);

  const {
    data: notifications,
    isLoading,
    isError,
    refetch,
  } = useQuery({
    queryKey: ['notifications'],
    queryFn: () => fetchNotifications(),
  });

  const filteredNotifications = useMemo(() => {
    if (!notifications) return [];
    return notifications.filter(
      (notification) =>
        notification.code.toLowerCase().includes(searchTerm.toLowerCase()) ||
        notification.eventCode
          .toLowerCase()
          .includes(searchTerm.toLowerCase()) ||
        notification.title.toLowerCase().includes(searchTerm.toLowerCase()) ||
        notification.description
          .toLowerCase()
          .includes(searchTerm.toLowerCase())
    );
  }, [notifications, searchTerm]);

  const handleModalClose = () => {
    setIsModalOpen(false);
    setSelectedNotification(null);
  };

  const handleCreate = () => {
    setIsModalOpen(true);
  };

  const handleEdit = (notification: Notification) => {
    setSelectedNotification(notification);
    setIsModalOpen(true);
  };

  const handleDelete = async (notificationCode: string) => {
    setDeletingCode(notificationCode);
    try {
      await axios.delete(`/api/notifications/${notificationCode}`);
      toast.success('Notification deleted successfully');
      refetch();
    } catch (error) {
      if (axios.isAxiosError(error)) {
        const resp = error.response?.data as ErrorResponse;
        toast.error('Error deleting notification', {
          description: resp.errors.join(', '),
        });
      } else {
        toast.error('An error occurred while deleting the notification', {
          description: 'If the problem persists, please contact support',
        });
      }
      console.error('Error deleting notification:', error);
    } finally {
      setDeletingCode(null);
    }
  };

  if (isLoading) {
    return (
      <div className='flex flex-grow items-center justify-center text-lg gap-2'>
        <Icons.spinner className='animate-spin h-8 w-8' />
        Loading notifications...
      </div>
    );
  }

  if (isError) {
    return (
      <div className='flex flex-grow items-center justify-center text-lg'>
        Error fetching notifications, please try again later
      </div>
    );
  }

  return (
    <div className='container mx-auto p-6'>
      <div className='flex justify-between items-center mb-6'>
        <h1 className='text-2xl font-bold'>Manage Application Notifications</h1>
        <div className='flex items-center space-x-2'>
          <LayoutList
            className={`h-4 w-4 ${
              !isCardLayout ? 'text-primary' : 'text-muted-foreground'
            }`}
          />
          <Switch
            id='layout-switch'
            checked={isCardLayout}
            onCheckedChange={setIsCardLayout}
          />
          <LayoutGrid
            className={`h-4 w-4 ${
              isCardLayout ? 'text-primary' : 'text-muted-foreground'
            }`}
          />
          <Label htmlFor='layout-switch' className='sr-only'>
            Toggle layout
          </Label>
        </div>
      </div>
      <div className='flex justify-between items-center mb-6'>
        <Input
          type='text'
          placeholder='Search notifications...'
          className='max-w-sm'
          value={searchTerm}
          onChange={(e) => setSearchTerm(e.target.value)}
        />
        <Button onClick={handleCreate}>
          <BadgePlus className='mr-2 h-5 w-5' /> Add New Notification
        </Button>
      </div>
      {isCardLayout ? (
        <div className='grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6'>
          {filteredNotifications.map((notification) => (
            <Card
              key={notification.code}
              className='flex flex-col justify-between'>
              <div>
                <CardHeader>
                  <Badge
                    style={{
                      backgroundColor: notification.color,
                      width: 'fit-content',
                    }}>
                    {notification.eventCode}
                  </Badge>
                  <CardTitle>{notification.title}</CardTitle>
                </CardHeader>
                <CardContent>
                  <p className='text-sm font-medim mb-2'>
                    Code: {notification.code}
                  </p>
                  <p className='text-sm text-muted-foreground'>
                    {notification.description}
                  </p>
                </CardContent>
              </div>
              <CardFooter className='flex justify-end space-x-2'>
                <Button
                  variant='outline'
                  size='sm'
                  onClick={() => handleEdit(notification)}>
                  <Pencil className='h-4 w-4 mr-2' />
                  Edit
                </Button>
                <Button
                  variant='outline'
                  size='sm'
                  onClick={() => handleDelete(notification.code)}
                  disabled={!!deletingCode}>
                  {deletingCode === notification.code ? (
                    <Icons.spinner className='h-4 w-4' />
                  ) : (
                    <Trash2 className='h-4 w-4' />
                  )}
                </Button>
              </CardFooter>
            </Card>
          ))}
        </div>
      ) : (
        <Table>
          <TableHeader>
            <TableRow>
              <TableHead>Event</TableHead>
              <TableHead>Code</TableHead>
              <TableHead>Title</TableHead>
              <TableHead>Description</TableHead>
              <TableHead>Actions</TableHead>
            </TableRow>
          </TableHeader>
          <TableBody>
            {filteredNotifications.map((notification) => (
              <TableRow key={notification.code}>
                <TableCell>
                  <Badge style={{ backgroundColor: notification.color }}>
                    {notification.eventTitle}
                  </Badge>
                </TableCell>
                <TableCell className='font-medium'>
                  {notification.code}
                </TableCell>
                <TableCell>{notification.title}</TableCell>
                <TableCell>{notification.description}</TableCell>
                <TableCell>
                  <Button
                    variant='ghost'
                    size='icon'
                    className='mr-2'
                    onClick={() => handleEdit(notification)}>
                    <Pencil className='h-4 w-4' />
                    <span className='sr-only'>Edit</span>
                  </Button>
                  <Button
                    variant='ghost'
                    size='sm'
                    onClick={() => handleDelete(notification.code)}
                    disabled={!!deletingCode}>
                    {deletingCode === notification.code ? (
                      <Icons.spinner className='h-4 w-4' />
                    ) : (
                      <Trash2 className='h-4 w-4' />
                    )}
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      )}
      <Modal
        isOpen={isModalOpen}
        onRequestClose={handleModalClose}
        size='extra-large'>
        <NotificationDetail
          notification={selectedNotification || undefined}
          onBack={handleModalClose}
          onCreate={() => refetch()}
          onSave={() => refetch()}
        />
      </Modal>
    </div>
  );

  async function fetchNotifications(): Promise<GetNotificationsResponse> {
    try {
      const { data } = await axios.get<GetNotificationsResponse>(
        '/api/notifications'
      );
      return data;
    } catch (error) {
      console.error(error);
      return [];
    }
  }
}
