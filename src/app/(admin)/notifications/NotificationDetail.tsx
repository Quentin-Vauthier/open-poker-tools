import axios, { HttpStatusCode } from 'axios';
import { ArrowLeft, BadgePlus, Save } from 'lucide-react';
import { useState } from 'react';
import { useForm, useWatch } from 'react-hook-form';
import { toast } from 'sonner';
import { z } from 'zod';

import { GetEventsResponse } from '@/app/api/events/types';
import {
  CreateNotificationResponse,
  Notification,
} from '@/app/api/notifications/types';
import { Icons } from '@/components/Icons';
import Editor from '@/components/Lexical/Editor';
import { Badge } from '@/components/ui/badge';
import { Button } from '@/components/ui/button';
import {
  Card,
  CardContent,
  CardFooter,
  CardHeader,
  CardTitle,
} from '@/components/ui/card';
import { StaticCombobox } from '@/components/ui/combobox';
import { Form, FormField, FormMessage } from '@/components/ui/form';
import { Input } from '@/components/ui/input';
import OPTFormItem from '@/components/ui/OPTFormItem';
import { Textarea } from '@/components/ui/textarea';
import { OPTEvent } from '@/lib/events/types';
import { zodResolver } from '@hookform/resolvers/zod';
import { useQuery } from '@tanstack/react-query';

const emptyNotification: Partial<Notification> = {
  code: '',
  title: '',
  description: '',
  color: '#000000',
  subject: '',
  message: '',
  html: '',
};

const notifSchema = z.object({
  code: z.string(),
  eventCode: z.nativeEnum(OPTEvent, {
    message: 'Event code is required',
  }),
  title: z.string(),
  description: z.string(),
  subject: z.string(),
  message: z.string(),
  html: z.string(),
  color: z.string(),
});

function NotificationBadge({
  color,
  eventCode,
}: {
  color: string;
  eventCode: string;
}) {
  return (
    <Badge
      className={`bg-[${color}] min-h-4 min-w-16`}
      style={{ backgroundColor: color }}>
      {eventCode}
    </Badge>
  );
}

interface NotificationDetailProps {
  notification?: Partial<Notification>;
  onBack: () => void;
  onCreate?: (notification: Notification) => void;
  onSave?: (notification: Notification) => void;
}

export default function NotificationDetail({
  notification = emptyNotification,
  onBack,
  onCreate,
  onSave,
}: NotificationDetailProps) {
  const [isFetching, setIsFetching] = useState(false);

  const isCreation = notification === emptyNotification;

  const form = useForm<Notification>({
    defaultValues: notification,
    resolver: zodResolver(notifSchema),
  });

  const [badgeColor, setBadgeColor] = useState(notification.color);

  const watchedEventCode = useWatch({
    control: form.control,
    name: 'eventCode',
  });

  const onSubmit = isCreation ? createNotification : updateNotification;

  async function createNotification(data: Notification) {
    try {
      setIsFetching(true);
      const { status } = await axios.post<CreateNotificationResponse>(
        '/api/notifications',
        data
      );

      if (status === HttpStatusCode.Created) {
        toast.success('Notification created successfully');
        if (onCreate) onCreate(data);
        onBack();
        return;
      }

      toast.error(
        'Unexpected response from the server, please contact support'
      );
    } catch (error) {
      if (!axios.isAxiosError(error)) {
        toast.error('An error occurred while creating the notification');
        return;
      }

      const status = error.response?.status;
      if (status === HttpStatusCode.Conflict) {
        toast.error('A Notification with this code already exists');
        form.setError('code', {
          message: 'A Notification with this code already exists',
        });
        return;
      }

      if (!status && error.request) {
        toast.error(
          'No response from the server, please check your connection or try again later'
        );
        return;
      }

      toast.error(
        'An unexpected error occurred while creating the notification',
        {
          description: 'Please try again later or contact support',
        }
      );
    } finally {
      setIsFetching(false);
    }
  }

  async function updateNotification(data: Notification) {
    try {
      setIsFetching(true);
      const { status } = await axios.put(
        `/api/notifications/${notification.code}`,
        data
      );

      if (status === HttpStatusCode.Ok) {
        toast.success('Notification updated successfully');
        if (onSave) onSave(data);
        onBack();
        return;
      }

      toast.error(
        'Unexpected response from the server, please contact support'
      );
    } catch (error) {
      if (!axios.isAxiosError(error)) {
        toast.error('An error occurred while updating the notification', {
          description: 'Please try again later or contact support',
        });
        return;
      }

      const status = error.response?.status;
      if (status === HttpStatusCode.Conflict) {
        form.setError('code', {
          message: 'A Notification with this code already exists',
        });
        return;
      }

      if (!status && error.request) {
        toast.error(
          'No response from the server, please check your connection or try again later'
        );
        return;
      }

      toast.error(
        'An unexpected error occurred while creating the notification',
        {
          description: 'Please try again later or contact support',
        }
      );
    } finally {
      setIsFetching(false);
    }
  }

  const {
    data: events,
    isPending,
    isError,
  } = useQuery({
    queryKey: ['events'],
    queryFn: fetchEvents,
  });

  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)}>
        <Button variant='ghost' onClick={onBack} className='mb-4' type='button'>
          <ArrowLeft className='mr-2 h-4 w-4' />
          Back to Notifications
        </Button>
        <Card className='w-full max-w-2xl mx-auto'>
          <CardHeader>
            <CardTitle className='flex justify-between items-center'>
              {isCreation ? 'Create' : 'Edit'} Notification
              <NotificationBadge
                color={badgeColor ?? '#000000'}
                eventCode={watchedEventCode}
              />
            </CardTitle>
          </CardHeader>
          <CardContent className='space-y-4'>
            <div className='grid grid-cols-5 gap-4'>
              <FormField
                control={form.control}
                name='code'
                render={({ field }) => (
                  <OPTFormItem label='Code' className='col-span-2' required>
                    <Input {...field} />
                  </OPTFormItem>
                )}
              />
              <FormField
                control={form.control}
                name='eventCode'
                render={({ field }) => (
                  <OPTFormItem
                    label='Event Code'
                    className='col-span-2'
                    required>
                    <StaticCombobox
                      placeholder='Select event...'
                      emptyLabel='No events found'
                      data={events ?? []}
                      disabled={isPending || isError}
                      onSelect={(value) => field.onChange(value || null)}
                      value={field.value}
                    />
                  </OPTFormItem>
                )}
              />
              <FormField
                control={form.control}
                name='color'
                render={({ field }) => (
                  <OPTFormItem label='Color' className='col-span-1'>
                    <Input
                      {...field}
                      type='color'
                      onBlur={(e) => {
                        field.onChange(e.target.value);
                        setBadgeColor(e.target.value);
                      }}
                    />
                  </OPTFormItem>
                )}
              />
            </div>
            <FormField
              control={form.control}
              name='title'
              render={({ field }) => (
                <OPTFormItem label='Title' required>
                  <Input {...field} />
                </OPTFormItem>
              )}
            />
            <FormField
              control={form.control}
              name='description'
              render={({ field }) => (
                <OPTFormItem label='Description'>
                  <Textarea {...field} />
                </OPTFormItem>
              )}
            />
            <FormField
              control={form.control}
              name='subject'
              render={({ field }) => (
                <OPTFormItem label='Subject'>
                  <Input {...field} />
                </OPTFormItem>
              )}
            />
            <FormField
              control={form.control}
              name='message'
              render={({ field }) => (
                <OPTFormItem label='Body'>
                  <Textarea {...field} rows={5} />
                </OPTFormItem>
              )}
            />
            <Editor
              mode='html'
              initialValue={form.getValues('html')}
              onChange={(data) => form.setValue('html', data ?? '')}
            />
            <FormMessage>{form.formState.errors.root?.message}</FormMessage>
          </CardContent>
          <CardFooter>
            <Button className='ml-auto' type='submit' disabled={isFetching}>
              {isFetching && <Icons.spinner className='mr-2 h-5 w-5' />}
              {!isFetching &&
                (isCreation ? (
                  <BadgePlus className='mr-2 h-5 w-5' />
                ) : (
                  <Save className='mr-2 h-5 w-5' />
                ))}
              {isCreation ? 'Create Notification' : 'Save Changes'}
            </Button>
          </CardFooter>
        </Card>
      </form>
    </Form>
  );

  async function fetchEvents() {
    const { data } = await axios.get<GetEventsResponse>(`/api/events`);
    return data.map((event) => ({
      value: event.code,
      label: event.title,
    }));
  }
}
