import { getUser } from '@/lib/auth';
import { Role } from '@/types';
import { redirect } from 'next/navigation';

export default async function AdminLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const user = await getUser();
  if (!user) redirect('/auth/sign-in');
  if (user.role !== Role.Admin) redirect('/');
  return <>{children}</>;
}
