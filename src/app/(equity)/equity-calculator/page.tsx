'use client';

import {
  BrainCircuit,
  CircuitBoard,
  ImageIcon,
  ImageOff,
  Plus,
} from 'lucide-react';
import { useState } from 'react';
import { toast } from 'sonner';

import CardSelector from '@/components/card-selector';
import { Button } from '@/components/ui/button';
import { Card, CardContent, CardHeader, CardTitle } from '@/components/ui/card';
import { Switch } from '@/components/ui/switch';
import { monteCarloSimulation } from '@/lib/montecarlo';
import {
  Card as PokerCard,
  CardRank,
  Cards,
  CardSuit,
  Hand,
  Odds,
} from '@/lib/poker-utils';
import {
  Tooltip,
  TooltipContent,
  TooltipProvider,
  TooltipTrigger,
} from '@/components/ui/tooltip';

interface HandProps {
  handNumber: number;
  hand: Hand;
  onCardChange: (index: number, value: PokerCard) => void;
  winningOdds: number | null;
  cardImages: boolean;
}

function HandCard({
  handNumber,
  hand,
  onCardChange,
  winningOdds,
  cardImages,
}: HandProps) {
  return (
    <Card className='w-full max-w-[200px]'>
      <CardHeader className='p-4'>
        <CardTitle className='text-sm'>
          Hand {handNumber}{' '}
          {winningOdds != null && `(${(winningOdds * 100).toFixed(2)}%)`}
        </CardTitle>
      </CardHeader>
      <CardContent className='p-4'>
        <div className='flex items-center justify-center space-x-2'>
          <CardSelector
            value={hand[0]}
            onChange={(value) => onCardChange(0, value)}
            cardImages={cardImages}
          />
          <CardSelector
            value={hand[1]}
            onChange={(value) => onCardChange(1, value)}
            cardImages={cardImages}
          />
        </div>
      </CardContent>
    </Card>
  );
}

const unknownCard = new PokerCard(CardRank.None, CardSuit.None);

export default function PokerEquityCalculator() {
  const [advancedSimulation, setAdvancedSimulation] = useState<boolean>(false);
  const [cardImages, setCardImages] = useState<boolean>(false);
  const [hands, setHands] = useState<Hand[]>([
    [unknownCard, unknownCard],
    [unknownCard, unknownCard],
  ]);
  const [board, setBoard] = useState<Cards>([
    unknownCard,
    unknownCard,
    unknownCard,
    unknownCard,
    unknownCard,
  ]);
  const [result, setResult] = useState<Odds | null>(null);

  const addHand = () => {
    toast.warning('Only two hands are supported for now. Sorry :(');
    // setHands([...hands, [unknownCard, unknownCard]]);
  };

  const updateHand = (
    handIndex: number,
    cardIndex: number,
    card: PokerCard
  ) => {
    const newHands = [...hands];
    newHands[handIndex][cardIndex] = card;
    setHands(newHands);
  };

  async function calculateEquity() {
    setResult(null);
    const handOne = hands[0];
    const handTwo = hands[1];
    monteCarloSimulation(
      handOne,
      handTwo,
      board.filter((card) => !card.isUnknown()),
      undefined,
      advancedSimulation ? 100_000 : 10_000
    ).then((odds) => setResult(odds));
  }

  return (
    <div className='container mx-auto p-4'>
      <h1 className='text-3xl font-bold mb-6'>Poker Equity Calculator</h1>

      <div className='mb-6'>
        <div className='flex justify-between items-center'>
          <h2 className='text-2xl font-semibold mb-4'>Player Hands</h2>
          <div className='flex gap-6'>
            <div className='flex items-start space-x-2'>
              <CircuitBoard className='h-6 w-6' />
              <label htmlFor='advanced-simulation-switch' className='sr-only'>
                Enable or disable advanced simulation
              </label>
              <TooltipProvider>
                <Tooltip>
                  <TooltipTrigger asChild>
                    <div>
                      <Switch
                        id='advanced-simulation-switch'
                        checked={advancedSimulation}
                        onCheckedChange={setAdvancedSimulation}
                      />
                    </div>
                  </TooltipTrigger>
                  <TooltipContent>
                    <p>Advanced simulation</p>
                  </TooltipContent>
                </Tooltip>
              </TooltipProvider>
              <BrainCircuit className='h-6 w-6' />
            </div>
            <div className='flex items-start space-x-2'>
              <ImageOff className='h-6 w-6' />
              <label htmlFor='card-image-switch' className='sr-only'>
                Enable or disable card images
              </label>
              <TooltipProvider>
                <Tooltip>
                  <TooltipTrigger asChild>
                    <div>
                      <Switch
                        id='card-image-switch'
                        checked={cardImages}
                        onCheckedChange={setCardImages}
                      />
                    </div>
                  </TooltipTrigger>
                  <TooltipContent>
                    <p>Card images</p>
                  </TooltipContent>
                </Tooltip>
              </TooltipProvider>
              <ImageIcon className='h-6 w-6' />
            </div>
          </div>
        </div>
        <div className='flex flex-wrap gap-4 mb-4'>
          {hands.map((hand, index) => (
            <HandCard
              key={index}
              handNumber={index + 1}
              hand={hand}
              onCardChange={(cardIndex, value) =>
                updateHand(index, cardIndex, value)
              }
              winningOdds={result?.get(hands[index])?.win ?? null}
              cardImages={cardImages}
            />
          ))}
          <Button
            variant={'outline'}
            onClick={addHand}
            className='h-[168px] w-[200px]'>
            <Plus className='mr-2 h-6 w-6' /> Add Hand
          </Button>
        </div>
      </div>

      <Card className='mb-6'>
        <CardHeader>
          <CardTitle>
            Board{' '}
            {result != null &&
              `(${(
                result.results
                  .map((or) => or.tie)
                  .reduce((acc, tie) => acc + tie, 0) * 100
              ).toFixed(2)}%)`}
          </CardTitle>
        </CardHeader>
        <CardContent>
          <div className='flex items-center justify-center space-x-2'>
            {board.map((card, index) => (
              <CardSelector
                key={index}
                value={card}
                onChange={(value) =>
                  setBoard(board.map((c, i) => (i === index ? value : c)))
                }
                cardImages={cardImages}
              />
            ))}
          </div>
        </CardContent>
      </Card>

      <Button onClick={calculateEquity} className='mb-6'>
        Calculate Equity
      </Button>
    </div>
  );
}
