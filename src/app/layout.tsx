import type { Metadata } from 'next';
import '@/app/globals.css';

import { Inter } from 'next/font/google';

import Footer from '@/components/Footer';
import Header from '@/components/Header';
import MaxWidthWrapper from '@/components/MaxWidthWrapper';
import Providers from '@/components/Providers';
import { Toaster } from '@/components/ui/sonner';
import { SessionProvider } from '@components/SessionContext';
import { validateRequest } from '@lib/auth';

const inter = Inter({ subsets: ['latin'] });

export const metadata: Metadata = {
  title: 'Open Poker Tools',
  description:
    'Open Poker Tools is a set of tools to help you with your poker games. It includes a tournament manager, an equity calculator and a hand tracker.',
};

export default async function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const session = await validateRequest();
  return (
    <html lang='en'>
      <body className={inter.className}>
        <SessionProvider value={session}>
          <a
            href='#main'
            className='sr-only focus:not-sr-only absolute top-0 left-0 inline'>
            Skip to main content
          </a>
          <MaxWidthWrapper className='min-h-dvh flex flex-col'>
            <Header />
            <Providers>
              <main id='main' className='flex flex-col flex-grow'>
                {children}
              </main>
            </Providers>
            <Footer />
          </MaxWidthWrapper>
          <Toaster richColors />
        </SessionProvider>
      </body>
    </html>
  );
}
