import {
  and,
  count,
  eq,
  exists,
  gte,
  isNotNull,
  isNull,
  or,
  sql,
  sum,
} from 'drizzle-orm';
import {
  Crosshair,
  MailWarning,
  Play,
  ShieldCheck,
  UserCheck,
} from 'lucide-react';
import Link from 'next/link';

import { Button } from '@/components/ui/button';
import {
  Card,
  CardContent,
  CardDescription,
  CardHeader,
  CardTitle,
} from '@/components/ui/card';
import { db } from '@/db';
import {
  locationsTable,
  seriesTable,
  tournamentInvitationsTable,
  tournamentPlayersTable,
  tournamentsTable,
} from '@/db/schema';
import { cn } from '@/lib/utils';
import { Tournament, TournamentVisibility } from '@/types';
import { getUser } from '@/lib/auth';

interface TournamentCard extends Partial<Tournament> {
  location: string;
  series: string;
  currentPlayers: number;
  isRegistered: number;
  isInvited: number;
  isMod: number;
}

async function fetchTournaments(
  playerId: number | undefined
): Promise<TournamentCard[] | null> {
  const tournaments = await db
    .select({
      id: tournamentsTable.id,
      name: tournamentsTable.name,
      visibility: tournamentsTable.visibility,
      date: tournamentsTable.date,
      startTime: tournamentsTable.startTime,
      location: locationsTable.name,
      buyIn: tournamentsTable.buyIn,
      knockout: tournamentsTable.knockout,
      progressiveKo: tournamentsTable.progressiveKo,
      tableSize: tournamentsTable.tableSize,
      currentPlayers: count(tournamentPlayersTable.playerId),
      maxPlayers: tournamentsTable.maxPlayers,
      variant: tournamentsTable.variant,
      series: seriesTable.name,
      isRegistered: playerId
        ? sql<number>`cast(SUM(CASE WHEN ${tournamentPlayersTable.playerId} = ${playerId} THEN 1 ELSE 0 END) as int)`
        : sql`0`,
      isInvited: playerId
        ? sql<number>`cast(SUM(CASE WHEN ${tournamentInvitationsTable.playerId} = ${playerId} THEN 1 ELSE 0 END) as int)`
        : sql`0`,
      isMod: playerId
        ? sql<number>`cast(SUM(CASE WHEN ${tournamentPlayersTable.playerId} = ${playerId} AND ${tournamentPlayersTable.isModerator} THEN 1 ELSE 0 END) as int)`
        : sql`0`,
    })
    .from(tournamentsTable)
    .leftJoin(
      locationsTable,
      eq(tournamentsTable.locationId, locationsTable.id)
    )
    .leftJoin(seriesTable, eq(tournamentsTable.seriesId, seriesTable.id))
    .leftJoin(
      tournamentPlayersTable,
      eq(tournamentsTable.id, tournamentPlayersTable.tournamentId)
    )
    .leftJoin(
      tournamentInvitationsTable,
      and(
        eq(tournamentsTable.id, tournamentInvitationsTable.tournamentId),
        eq(tournamentInvitationsTable.playerId, playerId ?? -1)
      )
    )
    .where(
      and(
        isNotNull(tournamentsTable.date),
        or(
          gte(tournamentsTable.date, new Date()),
          and(
            or(
              isNotNull(tournamentInvitationsTable.playerId),
              isNotNull(tournamentPlayersTable.playerId)
            ),
            isNotNull(tournamentsTable.startTime),
            isNull(tournamentsTable.endTime)
          )
        ),
        or(
          eq(tournamentsTable.visibility, TournamentVisibility.Public),
          playerId // Only show private tournaments if the user is a participant or invited
            ? exists(
                db
                  .select()
                  .from(tournamentPlayersTable)
                  .where(
                    and(
                      eq(tournamentPlayersTable.playerId, playerId),
                      eq(
                        tournamentPlayersTable.tournamentId,
                        tournamentsTable.id
                      )
                    )
                  )
              )
            : sql`false`,
          playerId
            ? exists(
                db
                  .select()
                  .from(tournamentInvitationsTable)
                  .where(
                    and(
                      eq(tournamentInvitationsTable.playerId, playerId),
                      eq(
                        tournamentInvitationsTable.tournamentId,
                        tournamentsTable.id
                      )
                    )
                  )
              )
            : sql`false`
        )
      )
    )
    .groupBy(tournamentsTable.id, locationsTable.name, seriesTable.name);
  return tournaments as TournamentCard[];
}

async function TournamentCard({ tournament }: { tournament: TournamentCard }) {
  const isTournamentStarted =
    tournament.startTime && tournament.startTime < new Date();
  return (
    <Link href={`/tournament/${tournament.id}`}>
      <Card
        aria-label={`Details of tournament ${tournament.name}`}
        role='link'
        className={cn('h-full flex flex-col', {
          'bg-secondary': tournament.visibility !== TournamentVisibility.Public,
        })}>
        <CardHeader>
          <CardTitle tabIndex={0} className='flex gap-2 items-center'>
            {tournament.name}
            {isTournamentStarted && (
              <>
                <Play
                  size={16}
                  className='text-positive'
                  aria-label='Started'
                />
              </>
            )}
          </CardTitle>
          <div
            className={cn('grid', {
              'grid-cols-3': tournament.series,
              'grid-cols-2': !tournament.series,
            })}>
            <CardDescription tabIndex={0}>
              {tournament.date!.toDateString()}
            </CardDescription>
            {/* <Separator orientation='vertical' /> */}
            {tournament.series && (
              <CardDescription tabIndex={0} className='text-center'>
                {tournament.series}
              </CardDescription>
            )}
            {/* <Separator orientation='vertical' /> */}
            <CardDescription tabIndex={0} className='text-end'>
              {tournament.maxPlayers
                ? `${
                    tournament.currentPlayers > tournament.maxPlayers
                      ? `(+${
                          tournament.currentPlayers - tournament.maxPlayers
                        }) ${tournament.currentPlayers}`
                      : tournament.currentPlayers
                  }/${tournament.maxPlayers}`
                : `${tournament.currentPlayers}/∞`}
            </CardDescription>
          </div>
        </CardHeader>
        <CardContent className='flex flex-grow items-end'>
          {tournament.location && (
            <p tabIndex={0}>Location: {tournament.location}</p>
          )}
          <p className='flex gap-2 items-center' tabIndex={0}>
            Buy-in: {tournament.buyIn ? `${tournament.buyIn}€` : 'FREE'}{' '}
            {tournament.knockout && (
              <Crosshair
                aria-label='Knockout tournament'
                size={16}
                color='red'
              />
            )}
            {Boolean(tournament.isMod) ? (
              <ShieldCheck aria-label='Moderator' size={16} color='green' />
            ) : (
              Boolean(tournament.isRegistered) && (
                <UserCheck aria-label='Registered' size={16} color='green' />
              )
            )}
            {Boolean(tournament.isInvited) && (
              <MailWarning aria-label='Invited' size={16} color='orange' />
            )}
          </p>
        </CardContent>
      </Card>
    </Link>
  );
}

export default async function TournamentsPage() {
  const user = await getUser();
  const tournaments = await fetchTournaments(user?.id);

  if (!tournaments) {
    return (
      <section>
        <h1>
          There are no upcoming tournaments. Check back later for more updates!
        </h1>
      </section>
    );
  }

  return (
    <section className='flex flex-col items-stretch gap-8 my-4'>
      <div className='flex flex-col items-center'>
        <h1 className='text-xl'>Upcoming Tournaments</h1>
      </div>
      <div>
        <Link href='/tournament'>
          <Button>Create a new tournament</Button>
        </Link>
      </div>
      <div className='grid gap-2 grid-cols-1 md:grid-cols-2 md:gap-4  xl:grid-cols-3 lg:gap-8'>
        {tournaments.map((t) => (
          <TournamentCard key={t.id} tournament={t} />
        ))}
      </div>
    </section>
  );
}
