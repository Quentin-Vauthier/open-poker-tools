'use client';

import { useRouter } from 'next/navigation';
import { useCallback, useEffect, useState } from 'react';
import { toast } from 'sonner';

import { EditableDataTable } from '@/components/EditableDataTable';
import { Icons } from '@/components/Icons';
import { editableColumns } from '@/components/structure/levels/Columns';
import { Button } from '@/components/ui/button';
import { Input } from '@/components/ui/input';
import { Label } from '@/components/ui/label';
import { Level } from '@/types';

const emptyLevel: Level = {
  isBreak: false,
  sb: 0,
  bb: 0,
  ante: 0,
  duration: 0,
};

export default function CreateStructurePage() {
  const [structureName, setStructureName] = useState<string>('');
  const [currentName, setCurrentName] = useState<string>('');
  const [data, setData] = useState<Level[]>([]);
  const router = useRouter();
  const [isSaving, setIsSaving] = useState<boolean>(false);
  const [nameError, setNameError] = useState<string | null>(null);
  const [levelsErrors, setLevelsErrors] = useState<
    {
      path: (string | number)[];
      message: string;
    }[]
  >([]);
  const [hasSaved, setHasSaved] = useState<boolean>(false);
  const [isCheckingName, setIsCheckingName] = useState<boolean>(false);

  const levels = data.map((level, position) => {
    if (level.isBreak) return { ...level, sb: 0, bb: 0, ante: 0 };
    return {
      sb: level.sb,
      bb: level.bb,
      ante: level.ante,
      duration: level.duration,
      isBreak: level.isBreak,
      position,
    };
  });

  const callbackUpdateLevelsErrors = useCallback(updateLevelsErrors, [
    hasSaved,
    levels,
  ]);
  useEffect(() => {
    callbackUpdateLevelsErrors();
  }, [callbackUpdateLevelsErrors]);

  return (
    <section className='flex flex-col items-stretch gap-8'>
      <div className='flex flex-col items-center'>
        <h1 className='text-xl'>Structure creator</h1>
      </div>
      <div>
        <Label htmlFor='structure-name'>Structure name</Label>
        {isCheckingName && (
          <div className='absolute py-3 px-1'>
            <Icons.spinner className='w-4 h-4' />
          </div>
        )}
        <Input
          id='structure-name'
          value={structureName}
          className='pl-6'
          onChange={(e) => setStructureName(e.target.value)}
          onBlur={async () => {
            if (!structureName) {
              setNameError('Structure name is required');
            } else {
              if (currentName === structureName) return;
              setNameError(null);
              setIsCheckingName(true);
              setCurrentName(structureName);
              await fetch(`/api/structure/name?name=${structureName}`)
                .then((response) => response.json())
                .then(({ exists }) => {
                  setNameError(
                    exists ? 'A structure with that name already exists' : null
                  );
                });
              setIsCheckingName(false);
            }
          }}
        />
        {nameError && <p className='px-2 text-sm text-red-500'>{nameError}</p>}
      </div>
      <div className='flex flex-col items-stretch gap-2'>
        <Button
          variant='outline'
          className='w-fit'
          onClick={() => {
            setData((prev: Level[]) => [...prev, emptyLevel]);
          }}>
          Add level
        </Button>
        <EditableDataTable
          columns={editableColumns}
          data={data!}
          setData={setData}
          emptyRowObject={emptyLevel}
          diaplayNoResults={false}
          errors={levelsErrors}
          updateErrors={updateLevelsErrors}
        />
        <div className='flex justify-end mb-4'>
          <Button
            className='w-fit'
            onClick={() => createStructure()}
            disabled={
              isSaving ||
              isCheckingName ||
              nameError !== null ||
              levelsErrors.length > 0
            }>
            Save
          </Button>
        </div>
      </div>
    </section>
  );

  function updateLevelsErrors(): boolean {
    if (!hasSaved) return false;
    let hasErors = false;
    const errors: {
      path: (string | number)[];
      message: string;
    }[] = [];
    if (levels.length === 0) {
      errors.push({
        path: ['root'],
        message: 'At least one level is required',
      });
      hasErors = true;
    }
    levels.forEach((level, index) => {
      if (level.duration < 1) {
        errors.push({
          path: [index, 'duration'],
          message: 'Duration cannot be less than 1 minute',
        });
        hasErors = true;
      }
      if (level.isBreak) return;
      if (level.sb < 1) {
        errors.push({
          path: [index, 'sb'],
          message: 'SB must be greater than 0',
        });
        hasErors = true;
      }
      if (level.bb < 1) {
        errors.push({
          path: [index, 'bb'],
          message: 'BB must be greater than 0',
        });
        hasErors = true;
      }
      if (level.ante < 0) {
        errors.push({
          path: [index, 'ante'],
          message: 'Ante cannot be less than 0',
        });
        hasErors = true;
      }
    });
    setLevelsErrors(errors);
    return hasErors;
  }

  async function createStructure(): Promise<void> {
    setHasSaved(true);
    let hasNameError = false;
    if (!structureName) {
      setNameError('Structure name is required');
      hasNameError = true;
    }
    const hasLevelErrors = updateLevelsErrors();
    if (hasLevelErrors || hasNameError) return;

    setIsSaving(true);
    const response = await fetch('/api/structure', {
      method: 'POST',
      body: JSON.stringify({
        name: structureName,
        levels,
      }),
    });
    if (!response.ok) {
      if (response.status === 500) {
        toast.error(
          'An error occurred while creating the structure, please try again later.'
        );
        setIsSaving(false);
        return;
      }
      const { errors } = await response.json();
      errors.forEach((error: any) => {
        toast.error(error);
      });
      setIsSaving(false);
      return;
    }
    toast.success('Structure created successfully');
    const { structureId } = await response.json();
    router.push(`/structure/${structureId}`);
  }
}
