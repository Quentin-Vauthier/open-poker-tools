import { eq } from 'drizzle-orm';
import { notFound } from 'next/navigation';

import ViewLevelsGrid from '@/components/structure/levels/ViewLevelsGrid';
import { db } from '@/db';
import { levelsTable, structuresTable } from '@/db/schema';
import { Structure } from '@/types';

type Params = Promise<{ structureId: string }>;

export default async function ViewStructurePage(props: { params: Params }) {
  const params = await props.params;
  const structureId = parseInt(params.structureId);
  if (isNaN(structureId)) {
    notFound();
  }

  const data = await getStructureData(structureId);

  if (!data) {
    notFound();
  }

  if (data instanceof Error) {
    return (
      <section className='flex flex-col flex-grow items-center justify-center'>
        <h1 className='text-xl'>
          Error fetching structure, please try again later.
        </h1>
      </section>
    );
  }

  return (
    <section className='flex flex-col items-stretch text-center mt-8 gap-6 mb-4'>
      <h1 className='text-2xl'>{data!.name}</h1>
      <ViewLevelsGrid data={data!.levels} />
    </section>
  );

  async function getStructureData(
    structureId: number
  ): Promise<Structure | null | Error> {
    try {
      const structure = await db
        .select({
          id: structuresTable.id,
          name: structuresTable.name,
          position: levelsTable.position,
          sb: levelsTable.sb,
          bb: levelsTable.bb,
          ante: levelsTable.ante,
          isBreak: levelsTable.isBreak,
          duration: levelsTable.duration,
        })
        .from(structuresTable)
        .innerJoin(levelsTable, eq(structuresTable.id, levelsTable.structureId))
        .where(eq(structuresTable.id, structureId))
        .orderBy(levelsTable.position)
        .execute();

      if (!structure.length) {
        return null;
      }

      return {
        id: structure[0].id,
        name: structure[0].name,
        levels: structure.map((level) => ({
          position: level.position,
          id: level.id,
          sb: level.sb,
          bb: level.bb,
          ante: level.ante,
          isBreak: level.isBreak,
          duration: level.duration,
        })),
      };
    } catch (error) {
      console.error(error);
      if (error instanceof Error) return error;
      return new Error('Unknown error');
    }
  }
}
