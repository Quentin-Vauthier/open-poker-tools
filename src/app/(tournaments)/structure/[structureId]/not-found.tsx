export default function StructureNotFound() {
  return (
    <section className='flex flex-col flex-grow items-center justify-center'>
      <h1 className='text-xl'>Structure not found</h1>
    </section>
  );
}
