import { Input } from '@/components/ui/input';
import { Label } from '@/components/ui/label';
import { Location } from '@/types';
import { notFound } from 'next/navigation';

import { db } from '@/db';
import { locationsTable } from '@/db/schema';
import { eq } from 'drizzle-orm';

type Params = Promise<{ locationId: string }>;

export default async function ViewLocationPage(props: { params: Params }) {
  const params = await props.params;
  const locationId = parseInt(params.locationId);
  if (isNaN(locationId)) {
    notFound();
  }

  const data = await getLocationData(locationId);

  if (!data) {
    return notFound();
  }

  if (data instanceof Error) {
    return (
      <section className='flex flex-col flex-grow items-center justify-center'>
        <h1 className='text-xl'>
          Error fetching location, please try again later.
        </h1>
      </section>
    );
  }

  return (
    <section className='flex flex-col items-stretch mt-8 gap-6 mb-4'>
      <div className='flex flex-col items-center'>
        <h1 className='text-2xl'>{data.name}</h1>
      </div>
      <div className='flex flex-col gap-2'>
        <div className='flex flex-col gap-1'>
          <Label htmlFor='location-address'>Address</Label>
          <Input id='location-address' value={data.address} readOnly />
        </div>
        <div className='flex flex-col gap-1'>
          <Label htmlFor='location-city'>City</Label>
          <Input id='location-city' value={data.city ?? ''} readOnly />
        </div>
        <div className='flex flex-col gap-1'>
          <Label htmlFor='location-zip'>Zip</Label>
          <Input id='location-zip' value={data.zip ?? ''} readOnly />
        </div>
      </div>
    </section>
  );

  async function getLocationData(
    locationId: number
  ): Promise<Location | Error> {
    try {
      const [location] = await db
        .select({
          id: locationsTable.id,
          name: locationsTable.name,
          address: locationsTable.address,
          city: locationsTable.city,
          zip: locationsTable.zip,
        })
        .from(locationsTable)
        .where(eq(locationsTable.id, locationId))
        .execute();
      return location;
    } catch (error) {
      if (error instanceof Error) return error;
      return new Error('Unknown error');
    }
  }
}
