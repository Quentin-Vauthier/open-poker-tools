export default function LocationNotFound() {
  return (
    <section className='flex flex-col flex-grow items-center justify-center'>
      <h1 className='text-xl'>Location not found</h1>
    </section>
  );
}
