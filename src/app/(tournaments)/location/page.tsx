'use client';

import axios, { HttpStatusCode } from 'axios';
import { useRouter } from 'next/navigation';
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { toast } from 'sonner';

import { Button } from '@/components/ui/button';
import { Input } from '@/components/ui/input';
import { Label } from '@/components/ui/label';
import { createLocationSchema } from '@/lib/zod';
import { LocationFormValues } from '@/types';
import { zodResolver } from '@hookform/resolvers/zod';

export default function CreateLocationPage() {
  const {
    register,
    handleSubmit,
    setError,
    formState: { errors },
  } = useForm<LocationFormValues>({
    resolver: zodResolver(createLocationSchema),
  });

  const [isSaving, setIsSaving] = useState<boolean>(false);
  const router = useRouter();

  return (
    <section className='flex flex-col items-stretch gap-8 my-4'>
      <div className='flex flex-col items-center'>
        <h1 className='text-xl'>Create Location</h1>
      </div>
      <form
        id='create-location-form'
        onSubmit={handleSubmit(createLocation)}
        className='flex flex-col gap-2'>
        <div className='flex flex-col gap-1'>
          <Label htmlFor='location-name'>
            Name <span className='text-red-500'>*</span>
          </Label>
          <Input id='location-name' {...register('name')} />
          {errors.name && (
            <span className='text-sm text-red-500 px-2'>
              {errors.name.message}
            </span>
          )}
        </div>
        <div className='flex flex-col gap-1'>
          <Label htmlFor='location-address'>
            Address <span className='text-red-500'>*</span>
          </Label>
          <Input id='location-address' {...register('address')} />
          {errors.address && (
            <span className='text-sm text-red-500 px-2'>
              {errors.address.message}
            </span>
          )}
        </div>
        <div className='flex flex-col gap-1'>
          <Label htmlFor='location-city'>City</Label>
          <Input id='location-city' {...register('city')} />
          {errors.city && (
            <span className='text-sm text-red-500 px-2'>
              {errors.city.message}
            </span>
          )}
        </div>
        <div className='flex flex-col gap-1'>
          <Label htmlFor='location-zip'>Zip</Label>
          <Input id='location-zip' {...register('zip')} />
          {errors.zip && (
            <span className='text-sm text-red-500 px-2'>
              {errors.zip.message}
            </span>
          )}
        </div>
        <div className='flex justify-between'>
          <p className='text-sm'>
            <span className='text-red-500'>*</span> Required fields
          </p>
          <Button
            type='submit'
            className='w-fit'
            disabled={
              isSaving ||
              !!errors.name ||
              !!errors.address ||
              !!errors.city ||
              !!errors.zip
            }>
            Create
          </Button>
        </div>
      </form>
    </section>
  );

  async function createLocation(values: LocationFormValues) {
    setIsSaving(true);
    const { data, status } = await axios.post('/api/location', values);
    if (status !== HttpStatusCode.Created) {
      if (status === HttpStatusCode.BadRequest) {
        for (const error of data.error) {
          setError(error.path as keyof LocationFormValues, {
            type: 'manual',
            message: error.message,
          });
        }
      }
      toast.error('Failed to create location, please try again later.');
      setIsSaving(false);
      return;
    }
    toast.success('Location created successfully');
    router.push(`/location/${data.id}`);
  }
}
