import { eq } from 'drizzle-orm';
import { notFound } from 'next/navigation';

import { Input } from '@/components/ui/input';
import { Label } from '@/components/ui/label';
import { db } from '@/db';
import {
  locationsTable,
  playersTable,
  seriesPlayersTable,
  seriesTable,
} from '@/db/schema';
import SeriesPlayersGrid from '@/components/series/SeriesPlayersGrid';
import SeriesRegisterButton from '@/components/series/RegisterButton';
import { getUser } from '@/lib/auth';
import SeriesAddPlayerButton from '@/components/series/AddPlayerButton';

type Params = Promise<{ seriesId: string }>;

export default async function ViewSeriesPage(props: { params: Params }) {
  const params = await props.params;
  const seriesAndLocation = await fetchSeries(params.seriesId);

  if (seriesAndLocation instanceof Error) {
    return (
      <section className='flex flex-col flex-grow items-center justify-center'>
        <h1 className='text-xl'>
          Error fetching series, please try again later.
        </h1>
      </section>
    );
  }

  if (!seriesAndLocation) notFound();

  const seriesPlayers = await fetchSeriesPlayers(seriesAndLocation.id);
  const user = await getUser();
  const isMod =
    (user && seriesPlayers.find((p) => p.id === user.id)?.mod) ?? false;

  return (
    <section className='flex flex-col items-stretch mt-8 gap-6 mb-4'>
      <div className='flex flex-col items-center'>
        <h1 className='text-xl'>{seriesAndLocation.name}</h1>
      </div>
      {seriesAndLocation.location ? (
        <div>
          <div className='flex flex-col gap-2'>
            <div className='flex flex-col gap-1'>
              <Label htmlFor='location-address'>Address</Label>
              <Input
                id='location-address'
                value={seriesAndLocation.location.address}
                readOnly
              />
            </div>
            <div className='flex flex-col gap-1'>
              <Label htmlFor='location-city'>City</Label>
              <Input
                id='location-city'
                value={seriesAndLocation.location.city ?? ''}
                readOnly
              />
            </div>
            <div className='flex flex-col gap-1'>
              <Label htmlFor='location-zip'>Zip</Label>
              <Input
                id='location-zip'
                value={seriesAndLocation.location.zip ?? ''}
                readOnly
              />
            </div>
          </div>
        </div>
      ) : (
        <div className='flex flex-col items-center'>
          <h1 className='text-lg'>No location assigned</h1>
        </div>
      )}
      {!isMod && user && (
        <SeriesRegisterButton
          registered={seriesPlayers.some((p) => p.id === user.id)}
          seriesId={seriesAndLocation.id}
        />
      )}
      {isMod && <SeriesAddPlayerButton seriesId={seriesAndLocation.id} />}
      <SeriesPlayersGrid seriesPlayers={seriesPlayers} />
    </section>
  );

  async function fetchSeries(seriesId: string) {
    try {
      const [seriesAndLocation] = await db
        .select({
          id: seriesTable.id,
          name: seriesTable.name,
          locationId: seriesTable.locationId,
          location: {
            id: locationsTable.id,
            name: locationsTable.name,
            address: locationsTable.address,
            city: locationsTable.city,
            zip: locationsTable.zip,
          },
        })
        .from(seriesTable)
        .leftJoin(locationsTable, eq(seriesTable.locationId, locationsTable.id))
        .where(eq(seriesTable.id, parseInt(seriesId)));
      return seriesAndLocation;
    } catch (error) {
      console.error(error);
      if (error instanceof Error) {
        return error;
      }
      return null;
    }
  }

  async function fetchSeriesPlayers(seriesId: number) {
    const seriesPlayers = await db
      .select({
        id: playersTable.id,
        nickname: playersTable.nickname,
        mod: seriesPlayersTable.isModerator,
      })
      .from(seriesPlayersTable)
      .innerJoin(playersTable, eq(seriesPlayersTable.playerId, playersTable.id))
      .where(eq(seriesPlayersTable.seriesId, seriesId));
    return seriesPlayers;
  }
}
