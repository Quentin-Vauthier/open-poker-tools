'use client';

import axios, { AxiosError, HttpStatusCode } from 'axios';
import { useRouter } from 'next/navigation';
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { toast } from 'sonner';

import { Icons } from '@/components/Icons';
import { Button } from '@/components/ui/button';
import { DynamicCombobox } from '@/components/ui/combobox';
import { Form, FormField } from '@/components/ui/form';
import { Input } from '@/components/ui/input';
import OPTFormItem from '@/components/ui/OPTFormItem';
import { createSeriesSchema } from '@/lib/zod';
import { ApiErrorResponse, Series, SeriesFormValues } from '@/types';
import { zodResolver } from '@hookform/resolvers/zod';

export default function CreateSeriesPage() {
  const form = useForm<SeriesFormValues>({
    defaultValues: {
      name: '',
      locationId: null,
    },
    resolver: zodResolver(createSeriesSchema),
  });
  const { handleSubmit, setError } = form;

  const [isSaving, setIsSaving] = useState<boolean>(false);

  const router = useRouter();

  return (
    <section className='flex flex-col items-stretch gap-8 my-4'>
      <div className='flex flex-col items-center'>
        <h1 className='text-xl'>Create Series</h1>
      </div>
      <Form {...form}>
        <form className='flex flex-col gap-2' onSubmit={handleSubmit(onSubmit)}>
          <FormField
            control={form.control}
            name='name'
            render={({ field }) => (
              <OPTFormItem label='Name' required>
                <Input {...field} />
              </OPTFormItem>
            )}
          />
          <FormField
            control={form.control}
            name='locationId'
            render={({ field }) => (
              <OPTFormItem label='Location'>
                <DynamicCombobox
                  placeholder='Select location'
                  emptyLabel='No locations'
                  searchPlaceholder='Search location...'
                  fetchItems={(query) => fetchLocations(query)}
                  onSelect={(value) =>
                    field.onChange(value ? parseInt(value) : null)
                  }
                  value={field.value?.toString() ?? ''}
                  width='w-72'
                />
              </OPTFormItem>
            )}
          />
          <div className='mt-2'>
            <Button type='submit' disabled={isSaving} className='flex'>
              {isSaving && (
                <>
                  <Icons.spinner size={18} />
                  &nbsp;
                </>
              )}
              {isSaving ? 'Saving...' : 'Save'}
            </Button>
          </div>
        </form>
      </Form>
    </section>
  );

  async function onSubmit(values: SeriesFormValues) {
    try {
      setIsSaving(true);
      const { data, status } = await axios.post<Series | ApiErrorResponse>(
        '/api/series',
        values
      );
      if (status !== HttpStatusCode.Created) {
        setIsSaving(false);
        return toast.error('Failed to create series, please try again later.');
      }
      const series = data as Series;
      router.push(`/series/${series.id}`);
    } catch (error) {
      setIsSaving(false);
      if (error instanceof AxiosError) {
        const response = error.response;
        if (response?.status === HttpStatusCode.BadRequest) {
          const { errors } = response.data as ApiErrorResponse;
          for (const error of errors) {
            setError(error.path[0] as keyof SeriesFormValues, {
              message: error.message,
            });
          }
        }
      }
      setIsSaving(false);
      toast.error('Failed to create series, please try again later.');
    }
  }

  async function fetchLocations(query: string) {
    const { data } = await axios.get<
      {
        id: number;
        name: string;
      }[]
    >(`/api/location?query=${query}`);
    return data.map((location) => ({
      value: location.id.toString(),
      label: location.name,
    }));
  }
}
