'use client';

import axios, { AxiosError, HttpStatusCode } from 'axios';
import { Search } from 'lucide-react';
import { notFound, redirect, useRouter } from 'next/navigation';
import { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import Modal from 'react-modal';
import { toast } from 'sonner';

import { Icons } from '@/components/Icons';
import StructureModal from '@/components/modals/structure/StructureModal';
import { Button } from '@/components/ui/button';
import { Checkbox } from '@/components/ui/checkbox';
import { DynamicCombobox, StaticCombobox } from '@/components/ui/combobox';
import DateTimePicker from '@/components/ui/date-time-picker';
import { Form, FormField } from '@/components/ui/form';
import { Input } from '@/components/ui/input';
import OPTFormItem from '@/components/ui/OPTFormItem';
import { createTournamentSchema } from '@/lib/zod';
import {
  Level,
  Role,
  TournamentFormValues,
  TournamentVisibility,
} from '@/types';
import { zodResolver } from '@hookform/resolvers/zod';
import { useSession } from '@/components/SessionContext';
import Editor from '@/components/Lexical/Editor';

export default function CreateTournamentPage() {
  const { user } = useSession();
  if (!user) redirect('/auth/sign-in');

  Modal.setAppElement('#main');

  const form = useForm<TournamentFormValues>({
    defaultValues: {
      name: '',
      visibility: TournamentVisibility.NotListed,
      date: undefined,
      description: '',
      buyIn: 0,
      knockout: false,
      progressiveKo: false,
      knockoutBounty: 0,
      canReentry: false,
      reentryEnd: 4,
      canLateReg: true,
      lateRegEnd: 4,
      tableSize: 9,
      minTableSize: 3,
      maxPlayers: 0,
      variant: 'NLHE',
      startingStack: 10_000,
      finalTableSize: 9,
      seriesId: null,
      structureId: null,
      locationId: null,
    },
    resolver: zodResolver(createTournamentSchema),
  });
  const { handleSubmit, watch, setValue } = form;

  useEffect(() => {
    if (watch('knockoutBounty') === 0)
      setValue('knockoutBounty', watch('buyIn') / 2);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [watch('knockout')]);

  const [isSaving, setIsSaving] = useState<boolean>(false);
  const [structureModalOpen, setStructureModalOpen] = useState<boolean>(false);
  const router = useRouter();

  const availableVisibilities = [
    { label: 'Private', value: TournamentVisibility.Private },
    { label: 'Not listed', value: TournamentVisibility.NotListed },
  ];

  if (user.role === Role.Admin)
    availableVisibilities.unshift({
      label: 'Public',
      value: TournamentVisibility.Public,
    });

  return (
    <section className='flex flex-col items-stretch gap-8 my-4'>
      <div className='flex flex-col items-center'>
        <h1 className='text-xl'>Tournament creator</h1>
      </div>
      <Form {...form}>
        <form
          id='create-tournament-form'
          onSubmit={handleSubmit(createTournament)}
          className='grid gap-2'>
          <div className='grid grid-cols-8 gap-4'>
            <FormField
              control={form.control}
              name='name'
              render={({ field }) => (
                <OPTFormItem
                  label='Tournament name'
                  className='col-span-4'
                  required>
                  <Input {...field} />
                </OPTFormItem>
              )}
            />
            <FormField
              control={form.control}
              name='visibility'
              render={({ field }) => (
                <OPTFormItem required label='Visibility' className='col-span-2'>
                  <StaticCombobox
                    searchable={false}
                    placeholder='Select visibility'
                    // Do not show public if the user is not an admin
                    data={availableVisibilities}
                    onSelect={field.onChange}
                    value={field.value}
                  />
                </OPTFormItem>
              )}
            />
            <FormField
              control={form.control}
              name='date'
              render={({ field }) => (
                <OPTFormItem label='Date' required className='col-span-2'>
                  <DateTimePicker onChange={field.onChange} />
                </OPTFormItem>
              )}
            />
          </div>
          <div className='grid grid-cols-2 gap-4'>
            <FormField
              control={form.control}
              name='buyIn'
              render={({ field }) => (
                <OPTFormItem label='Buy-in' required>
                  <Input {...field} type='number' />
                </OPTFormItem>
              )}
            />
            <FormField
              control={form.control}
              name='startingStack'
              render={({ field }) => (
                <OPTFormItem label='Starting stack' required>
                  <Input {...field} type='number' step={1000} />
                </OPTFormItem>
              )}
            />
          </div>
          <div className='grid grid-cols-2 gap-4'>
            <div className='grid grid-cols-3 min-h-20 gap-2'>
              <FormField
                control={form.control}
                name='seriesId'
                render={({ field }) => (
                  <OPTFormItem label='Series'>
                    <DynamicCombobox
                      placeholder='Select series'
                      emptyLabel='No series found'
                      fetchItems={(query) => fetchSeries(query)}
                      onSelect={(value) =>
                        field.onChange(value ? parseInt(value) : null)
                      }
                      value={field.value?.toString() ?? ''}
                    />
                  </OPTFormItem>
                )}
              />
              <FormField
                control={form.control}
                name='locationId'
                render={({ field }) => (
                  <OPTFormItem label='Location'>
                    <DynamicCombobox
                      disabled={watch('seriesId') !== null}
                      placeholder='Select location'
                      emptyLabel='No locations found'
                      fetchItems={(query) => fetchLocations(query)}
                      onSelect={(value) =>
                        field.onChange(value ? parseInt(value) : null)
                      }
                      value={field.value?.toString() ?? ''}
                    />
                  </OPTFormItem>
                )}
              />
              <FormField
                control={form.control}
                name='structureId'
                render={({ field }) => (
                  <OPTFormItem label='Structure'>
                    <div className='flex items-center gap-2'>
                      <DynamicCombobox
                        className='flex-grow'
                        fetchItems={(query) => fetchStructures(query)}
                        placeholder='Select structure'
                        onSelect={(value) =>
                          field.onChange(value ? parseInt(value) : null)
                        }
                        value={field.value?.toString() ?? ''}
                        emptyLabel='No structure found'
                        searchPlaceholder='Search structure...'
                      />
                      <Button
                        type='button'
                        disabled={watch('structureId', null) === null}
                        onClick={() => setStructureModalOpen(true)}>
                        <Search size={16} />
                      </Button>
                      <Modal
                        isOpen={structureModalOpen}
                        onRequestClose={() => setStructureModalOpen(false)}
                        overlayClassName='fixed flex justify-center items-center top-0 left-0 w-full h-full bg-black bg-opacity-50 z-50'
                        className='w-2/3 h-[32rem] flex flex-col text-center bg-white px-4 py-4 gap-4 rounded-sm'>
                        <StructureModal structureId={watch('structureId')!} />
                      </Modal>
                    </div>
                  </OPTFormItem>
                )}
              />
            </div>
            <div className='grid grid-cols-2 min-h-20'>
              <div className='space-y-2 flex flex-col justify-center items-center'>
                <div className='flex flex-col gap-1'>
                  <FormField
                    control={form.control}
                    name='knockout'
                    render={({ field }) => (
                      <OPTFormItem
                        label='Knockout tournament'
                        className='flex flex-row items-center space-x-3 space-y-0'>
                        <Checkbox
                          checked={field.value}
                          onCheckedChange={field.onChange}
                        />
                      </OPTFormItem>
                    )}
                  />
                  <FormField
                    control={form.control}
                    name='progressiveKo'
                    render={({ field }) => (
                      <OPTFormItem
                        label='Progressive ?'
                        className='flex flex-row items-center space-x-3 space-y-0'>
                        <Checkbox
                          disabled={!watch('knockout')}
                          checked={field.value && watch('knockout')}
                          onCheckedChange={field.onChange}
                        />
                      </OPTFormItem>
                    )}
                  />
                </div>
              </div>
              <FormField
                control={form.control}
                name='knockoutBounty'
                render={({ field }) => (
                  <OPTFormItem label='Bounty' required={watch('knockout')}>
                    <Input
                      disabled={!watch('knockout')}
                      {...field}
                      type='number'
                    />
                  </OPTFormItem>
                )}
              />
            </div>
          </div>
          <div className='grid grid-cols-4 gap-4'>
            <FormField
              control={form.control}
              name='tableSize'
              render={({ field }) => (
                <OPTFormItem label='Table size'>
                  <Input {...field} type='number' />
                </OPTFormItem>
              )}
            />
            <FormField
              control={form.control}
              name='finalTableSize'
              render={({ field }) => (
                <OPTFormItem label='Final table size'>
                  <Input {...field} type='number' />
                </OPTFormItem>
              )}
            />
            <FormField
              control={form.control}
              name='minTableSize'
              render={({ field }) => (
                <OPTFormItem label='Minimum players per table'>
                  <Input {...field} type='number' />
                </OPTFormItem>
              )}
            />
            <FormField
              control={form.control}
              name='maxPlayers'
              render={({ field }) => (
                <OPTFormItem label='Maximum players' sup='(0 for unlimited)'>
                  <Input {...field} type='number' />
                </OPTFormItem>
              )}
            />
          </div>
          <div className='grid grid-cols-5 min-h-20 gap-4'>
            <div className='grid grid-cols-2 col-span-2'>
              <FormField
                control={form.control}
                name='canReentry'
                render={({ field }) => (
                  <OPTFormItem
                    label='Re-entry allowed'
                    className='flex flex-row items-center justify-center space-x-3 space-y-0'>
                    <Checkbox
                      checked={field.value}
                      onCheckedChange={field.onChange}
                    />
                  </OPTFormItem>
                )}
              />
              <FormField
                control={form.control}
                name='reentryEnd'
                render={({ field }) => (
                  <OPTFormItem
                    label='Re-entry end'
                    sup='(number of levels)'
                    required={watch('canReentry')}>
                    <Input
                      disabled={!watch('canReentry')}
                      {...field}
                      type='number'
                    />
                  </OPTFormItem>
                )}
              />
            </div>
            <div className='grid grid-cols-2 col-span-2'>
              <FormField
                control={form.control}
                name='canLateReg'
                render={({ field }) => (
                  <OPTFormItem
                    label='Late registration'
                    className='flex flex-row items-center justify-center space-x-3 space-y-0'>
                    <Checkbox
                      checked={field.value}
                      onCheckedChange={field.onChange}
                    />
                  </OPTFormItem>
                )}
              />
              <FormField
                control={form.control}
                name='lateRegEnd'
                render={({ field }) => (
                  <OPTFormItem
                    label='Late reg end'
                    sup='(number of levels)'
                    required={watch('canLateReg')}>
                    <Input
                      disabled={!watch('canLateReg')}
                      {...field}
                      type='number'
                    />
                  </OPTFormItem>
                )}
              />
            </div>
            <FormField
              control={form.control}
              name='variant'
              render={({ field }) => (
                <OPTFormItem label='Variant'>
                  <StaticCombobox
                    placeholder='Select variant'
                    emptyLabel='No variant found'
                    searchable={false}
                    data={[
                      { label: 'NLHE', value: 'NLHE' },
                      { label: 'PLO', value: 'PLO' },
                    ]}
                    onSelect={(value) => field.onChange(value)}
                    value={field.value}
                  />
                </OPTFormItem>
              )}
            />
          </div>
          <OPTFormItem label='Description' className='mb-2'>
            <Editor
              initialValue={form.getValues('description')}
              onChange={(data) => form.setValue('description', data)}
            />
          </OPTFormItem>
          <Button disabled={isSaving} type='submit'>
            {isSaving && (
              <>
                <Icons.spinner className='animate-spin h-4 w-4 ml-2' />
                &nbsp;
              </>
            )}
            Create
          </Button>
        </form>
      </Form>
    </section>
  );

  async function fetchStructures(query: string) {
    const { data } = await axios.get<
      {
        id: number;
        name: string;
        levels: Level[];
      }[]
    >(`/api/structure?query=${query}`);
    return data.map((structure) => ({
      value: structure.id.toString(),
      label: structure.name,
    }));
  }

  async function fetchSeries(query: string) {
    const { data } = await axios.get<
      {
        id: number;
        name: string;
      }[]
    >(`/api/series?query=${query}`);
    return data.map((series) => ({
      value: series.id.toString(),
      label: series.name,
    }));
  }

  async function fetchLocations(query: string) {
    const { data } = await axios.get<
      {
        id: number;
        name: string;
      }[]
    >(`/api/location?query=${query}`);
    return data.map((location) => ({
      value: location.id.toString(),
      label: location.name,
    }));
  }

  async function createTournament(values: TournamentFormValues) {
    setIsSaving(true);
    try {
      const { data, status } = await axios.post('/api/tournament', values);
      router.push(`/tournament/${data.tournamentId}`);
    } catch (e) {
      if (e instanceof AxiosError && e.status === HttpStatusCode.BadRequest) {
        toast.error('Please check the form for errors');
      } else {
        toast.error('An error occured, please try again later');
      }
      setIsSaving(false);
    }
  }
}
