export default function TournamentNotFound() {
  return (
    <section className='flex flex-col flex-grow items-center justify-center'>
      <h1 className='text-xl'>
        This tournament either does not exist or is private and you need to be
        invited
      </h1>
    </section>
  );
}
