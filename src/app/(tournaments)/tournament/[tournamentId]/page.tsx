import { eq, sql } from 'drizzle-orm';
import { notFound } from 'next/navigation';

import TournamentDetails from '@/components/tournaments/TournamentDetails';
import TournamentPlayersInfo from '@/components/tournaments/TournamentPlayersInfo';
import { db } from '@/db';
import {
  locationsTable,
  seriesTable,
  structuresTable,
  tournamentsTable,
} from '@/db/schema';
import { getUser } from '@/lib/auth';
import { playerHasAccessToTournament } from '@/lib/tournament-utils';
import { TournamentInfo } from '@/types';

type Params = Promise<{ tournamentId: string }>;

export default async function ViewTournamentPage(props: { params: Params }) {
  const params = await props.params;
  const tournamentId = params.tournamentId;
  if (!tournamentId || isNaN(parseInt(tournamentId))) return notFound();

  const user = await getUser();

  if (!(await playerHasAccessToTournament(user?.id, parseInt(tournamentId))))
    notFound();

  const tournament = await fetchTournament();

  if (!tournament) return notFound();

  return (
    <>
      <section className='flex flex-col items-stretch gap-8 my-4'>
        <div className='flex flex-col items-center'>
          <h1 className='text-2xl'>
            {tournament.name} - {tournament.date.toLocaleDateString()}
          </h1>
        </div>
      </section>
      <TournamentPlayersInfo tournament={tournament} />
      <TournamentDetails tournament={tournament} className='mt-4 mb-2' />
    </>
  );

  async function fetchTournament(): Promise<TournamentInfo | null> {
    try {
      const [tournament] = await db
        .select({
          id: tournamentsTable.id,
          name: tournamentsTable.name,
          visibility: tournamentsTable.visibility,
          date: tournamentsTable.date,
          description: tournamentsTable.description,
          waitlist: tournamentsTable.waitlist,
          startTime: tournamentsTable.startTime,
          endTime: tournamentsTable.endTime,
          buyIn: tournamentsTable.buyIn,
          knockout: tournamentsTable.knockout,
          progressiveKo: tournamentsTable.progressiveKo,
          knockoutBounty: tournamentsTable.knockoutBounty,
          canReentry: tournamentsTable.canReentry,
          reentryEnd: tournamentsTable.reentryEnd,
          canLateReg: tournamentsTable.canLateReg,
          lateRegEnd: tournamentsTable.lateRegEnd,
          tableSize: tournamentsTable.tableSize,
          minTableSize: tournamentsTable.minTableSize,
          maxPlayers: tournamentsTable.maxPlayers,
          variant: tournamentsTable.variant,
          startingStack: tournamentsTable.startingStack,
          finalTableSize: tournamentsTable.finalTableSize,
          seriesId: tournamentsTable.seriesId,
          structureId: tournamentsTable.structureId,
          series: seriesTable.name,
          structure: structuresTable.name,
          locationId: seriesTable.locationId,
          location: locationsTable.name,
        })
        .from(tournamentsTable)
        .leftJoin(
          structuresTable,
          eq(tournamentsTable.structureId, structuresTable.id)
        )
        .leftJoin(seriesTable, eq(tournamentsTable.seriesId, seriesTable.id))
        .leftJoin(
          locationsTable,
          sql`${locationsTable.id} = COALESCE(${tournamentsTable.locationId}, ${seriesTable.locationId})`
        )
        .where(eq(tournamentsTable.id, parseInt(tournamentId)));
      return tournament;
    } catch (e) {
      console.error(`Error fetching tournament ${tournamentId} details`, e);
      return null;
    }
  }
}
