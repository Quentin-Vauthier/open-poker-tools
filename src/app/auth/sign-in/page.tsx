'use client';

import { useRouter } from 'next/navigation';
import { useForm } from 'react-hook-form';

import { signIn } from '@/actions/auth';
import { Button } from '@/components/ui/button';
import { Input } from '@/components/ui/input';
import { Label } from '@/components/ui/label';
import { signInSchema } from '@/lib/zod';
import { zodResolver } from '@hookform/resolvers/zod';

interface SignInFormValues {
  email: string;
  password: string;
}

export default function SignInPage() {
  const router = useRouter();

  // Use useForm hook to manage the form state
  const {
    register,
    handleSubmit,
    setError,
    formState: { errors },
  } = useForm<SignInFormValues>({
    resolver: zodResolver(signInSchema),
  });

  const onSubmit = async (values: SignInFormValues) => {
    // Do not submit the form if there are errors
    if (errors.root || errors.email || errors.password) return;

    const signInRes = await signIn(values);

    // If sign in is successful, redirect to home page
    if (signInRes.success) {
      router.replace('/');
      return;
    }

    // Handle sign in errors
    if (signInRes.errors) {
      signInRes.errors.forEach((error) => {
        setError(error.path as keyof SignInFormValues, {
          type: 'manual',
          message: error.message,
        });
      });
    } else {
      setError('root', {
        type: 'manual',
        message: 'An error occurred. Please try again.',
      });
    }
  };

  return (
    <div className='flex flex-grow items-center justify-center'>
      <div className='w-full max-w-md space-y-8 rounded-lg'>
        <div className='text-center'>
          <h2 className='text-3xl font-bold tracking-tight'>Sign in</h2>
          <p className='mt-2 text-gray-500 dark:text-gray-400'>
            Enter your email and password to access your account.
          </p>
        </div>
        <form className='space-y-6' onSubmit={handleSubmit(onSubmit)}>
          <div>
            <Label htmlFor='email' className='block text-sm font-medium'>
              Email or username
            </Label>
            <div className='mt-1'>
              <Input
                id='email'
                type='text'
                autoComplete='email'
                placeholder='example@email.com'
                {...register('email')}
              />
              {errors.email && (
                <div className='text-red-500 text-sm text-center pt-0.5'>
                  {errors.email.message}
                </div>
              )}
            </div>
          </div>
          <div>
            <Label htmlFor='password' className='block text-sm font-medium'>
              Password
            </Label>
            <div className='mt-1'>
              <Input
                id='password'
                type='password'
                autoComplete='current-password'
                placeholder='••••••••'
                {...register('password')}
              />
              {errors.password && (
                <div className='text-red-500 text-sm text-center pt-0.5'>
                  {errors.password.message}
                </div>
              )}
            </div>
          </div>
          <div>
            <Button type='submit' className='flex w-full justify-center'>
              Sign in
            </Button>
            {errors.root && (
              <div className='text-red-500 text-sm text-center pt-0.5'>
                {errors.root.message}
              </div>
            )}
          </div>
        </form>
      </div>
    </div>
  );
}
