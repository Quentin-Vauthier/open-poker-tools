'use client';

import { useRouter } from 'next/navigation';
import { useForm } from 'react-hook-form';

import { signUpUser } from '@/actions/auth';
import { Button } from '@/components/ui/button';
import { Input } from '@/components/ui/input';
import { Label } from '@/components/ui/label';
import { cn } from '@/lib/utils';
import { signUpSchema } from '@/lib/zod';
import { zodResolver } from '@hookform/resolvers/zod';

// Define the interface for form values
interface SignupFormValues {
  email: string;
  nickname: string;
  password: string;
  confirmPassword: string;
}

export default function SignUpPage() {
  // Use useForm hook to manage the form state
  const {
    register,
    handleSubmit,
    setError,
    formState: { errors },
  } = useForm<SignupFormValues>({
    resolver: zodResolver(signUpSchema),
  });

  const router = useRouter();

  // Function to handle form submission
  const onSubmit = async (values: SignupFormValues) => {
    if (
      errors.root ||
      errors.email ||
      errors.nickname ||
      errors.password ||
      errors.confirmPassword
    ) {
      return;
    }
    const signUpResult = await signUpUser(
      values.email,
      values.nickname,
      values.password
    );
    if (signUpResult.success) {
      router.replace('/');
      return;
    }

    if (signUpResult.errors) {
      // Handle sign up errors
      signUpResult.errors.forEach((error) => {
        setError(error.path as keyof SignupFormValues, {
          type: 'manual',
          message: error.message,
        });
      });
    } else {
      setError('root', {
        type: 'manual',
        message: 'An error occurred. Please try again.',
      });
    }
  };

  return (
    <div className='flex flex-grow items-center justify-center'>
      <div className='w-full max-w-md p-6'>
        <div className='space-y-4'>
          <div className='text-center'>
            <h1 className='text-3xl font-bold'>Sign Up</h1>
            <p className='text-gray-500 dark:text-gray-400'>
              Create your account to get started.
            </p>
          </div>
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className='space-y-4'>
              <div>
                <Label htmlFor='email'>Email</Label>
                <Input
                  id='email'
                  type='text'
                  inputMode='email'
                  placeholder='example@email.com'
                  className={cn({
                    'border-red-500': errors.email,
                    'focus-visible:border-none': errors.email,
                    'focus-visible:ring-red-500': errors.email,
                  })}
                  {...register('email')}
                />
                {errors.email && (
                  <div className='text-red-500 text-sm text-center pt-0.5'>
                    {errors.email.message}
                  </div>
                )}
              </div>
              <div>
                <Label htmlFor='nickname'>Nickname</Label>
                <Input
                  id='nickname'
                  type='text'
                  placeholder='Enter your nickname'
                  className={cn({
                    'focus:border-red-500': errors.nickname,
                    'focus-visible:border-none': errors.nickname,
                    'focus-visible:ring-red-500': errors.nickname,
                  })}
                  {...register('nickname')}
                />
                {errors.nickname && (
                  <div className='text-red-500 text-sm text-center pt-0.5'>
                    {errors.nickname.message}
                  </div>
                )}
              </div>
              <div>
                <Label htmlFor='password'>Password</Label>
                <Input
                  id='password'
                  type='password'
                  className={cn({
                    'border-red-500': errors.password,
                    'focus-visible:border-none': errors.password,
                    'focus-visible:ring-red-500': errors.password,
                  })}
                  {...register('password')}
                />
                {errors.password && (
                  <div className='text-red-500 text-sm text-center pt-0.5'>
                    {errors.password.message}
                  </div>
                )}
              </div>
              <div>
                <Label htmlFor='confirmPassword'>Confirm Password</Label>
                <Input
                  id='confirmPassword'
                  type='password'
                  className={cn({
                    'border-red-500': errors.confirmPassword,
                    'focus:border-none': errors.confirmPassword,
                    'focus-visible:ring-red-500': errors.confirmPassword,
                  })}
                  {...register('confirmPassword')}
                />
                {errors.confirmPassword && (
                  <div className='text-red-500 text-sm text-center pt-0.5'>
                    {errors.confirmPassword.message}
                  </div>
                )}
              </div>
              <Button type='submit' className='w-full'>
                Sign Up
              </Button>
              {errors.root && (
                <div className='text-red-500 text-sm text-center pt-0.5'>
                  {errors.root.message}
                </div>
              )}
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
