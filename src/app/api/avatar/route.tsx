import { deleteFileFromGCS, uploadFileToGCS } from '@/lib/googleCloudStorage';
import { NextRequest, NextResponse } from 'next/server';
import { ErrorResponse } from '@/app/api/types';
import { getUser } from '@/lib/auth';
import { HttpStatusCode } from 'axios';
import { compressAndConvertToWebp } from '@/lib/imageUtil';
import { db } from '@/db';
import { playersTable } from '@/db/schema';
import { eq } from 'drizzle-orm';

// Disable Next.js built-in body parser for file uploads
export const config = {
  api: {
    bodyParser: false,
  },
};

export const POST = async (req: NextRequest) => {
  try {
    const user = await getUser();
    if (!user) {
      return NextResponse.json(
        { errors: ['Unauthorized'] },
        { status: HttpStatusCode.Unauthorized }
      ) as NextResponse<ErrorResponse>;
    }
    const body = await req.formData();
    const file = body.get('file');
    if (!file || !(file instanceof File)) {
      return NextResponse.json(
        { errors: ['No file uploaded'] },
        { status: HttpStatusCode.BadRequest }
      ) as NextResponse<ErrorResponse>;
    }

    const fileBuffer = await file.arrayBuffer();

    // Make sure that the file is an image
    if (!file.type.startsWith('image/')) {
      return NextResponse.json(
        { errors: ['Invalid file type'] },
        { status: HttpStatusCode.BadRequest }
      ) as NextResponse<ErrorResponse>;
    }

    // Compress image
    const webpBuffer = await compressAndConvertToWebp(Buffer.from(fileBuffer));

    const filePath = `avatars/${user.nickname}-${Date.now()}.webp`;
    const publicUrl = await uploadFileToGCS(webpBuffer, filePath, 'image/webp');

    if (!publicUrl) {
      return NextResponse.json(
        { errors: ['Failed to upload file'] },
        { status: HttpStatusCode.InternalServerError }
      ) as NextResponse<ErrorResponse>;
    }

    await db.transaction(async (tx) => {
      await tx
        .update(playersTable)
        .set({
          avatar: filePath,
        })
        .where(eq(playersTable.id, user.id))
        .returning({
          id: playersTable.id,
        });
      if (user.avatar) {
        try {
          await deleteFileFromGCS(user.avatar);
        } catch (error) {
          tx.rollback();
          throw error;
        }
      }
    });

    return NextResponse.json({ url: publicUrl });
  } catch (error) {
    console.error('Error updating avatar:', error);
    return NextResponse.json(
      { errors: ['Failed to update avatar'] },
      { status: HttpStatusCode.InternalServerError }
    ) as NextResponse<ErrorResponse>;
  }
};
