import { HttpStatusCode } from 'axios';
import { asc, ilike, or } from 'drizzle-orm';
import { ZodError } from 'zod';

import { db } from '@/db';
import { locationsTable } from '@/db/schema';
import { createLocationSchema } from '@/lib/zod';
import { NextRequest } from 'next/server';

export const POST = async (req: Request) => {
  try {
    const body = await req.json();
    const { name, address, city, zip } = await createLocationSchema.parseAsync(
      body
    );
    const [location] = await db
      .insert(locationsTable)
      .values({ name, address, city: city || null, zip: zip || null })
      .returning()
      .execute();
    return Response.json(location, { status: HttpStatusCode.Created });
  } catch (error) {
    if (error instanceof ZodError) {
      return Response.json(
        {
          error: error.errors.map((err) => ({
            path: err.path,
            message: err.message,
          })),
        },
        { status: HttpStatusCode.BadRequest }
      );
    }
    return Response.json(
      { errors: ['An error occurred while creating the location'] },
      { status: HttpStatusCode.InternalServerError }
    );
  }
};

export const GET = async (req: NextRequest) => {
  try {
    const searchParams = req.nextUrl.searchParams;
    const query = searchParams.get('query');
    const locations = await db
      .select({
        id: locationsTable.id,
        name: locationsTable.name,
      })
      .from(locationsTable)
      .where(
        or(
          ilike(locationsTable.name, `%${query}%`),
          ilike(locationsTable.city, `%${query}%`)
        )
      )
      .orderBy(asc(locationsTable.name));
    return Response.json(locations);
  } catch (error) {
    return Response.json(
      { errors: ['An error occurred while fetching locations'] },
      { status: HttpStatusCode.InternalServerError }
    );
  }
};
