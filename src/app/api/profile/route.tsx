import { getUser } from '@/lib/auth';
import { NextRequest, NextResponse } from 'next/server';
import { ErrorResponse, SuccessfulResponse } from '@/app/api/types';
import { HttpStatusCode } from 'axios';
import { z } from 'zod';
import { formatZodError } from '@/lib/utils';
import { db } from '@/db';
import { playersTable } from '@/db/schema';
import { eq } from 'drizzle-orm';
import { deleteFileFromGCS } from '@/lib/googleCloudStorage';

const updateProfileSchema = z.object({
  email: z.string().email({ message: 'Invalid email address' }),
  nickname: z
    .string()
    .min(2, { message: 'Nickname must be at least 2 characters' })
    .max(50, { message: 'Nickname must be at most 50 characters' }),
});

export const PUT = async (req: NextRequest) => {
  try {
    const user = await getUser();
    if (!user) {
      return NextResponse.json(
        {
          errors: [
            'You must be a logged in admin user to update notifications.',
          ],
        },
        { status: HttpStatusCode.Unauthorized }
      ) as NextResponse<ErrorResponse>;
    }

    const json = await req.json().catch(() => null);
    const { success, data, error } = updateProfileSchema.safeParse(json);

    if (!success) {
      return NextResponse.json(
        { errors: formatZodError(error) },
        { status: HttpStatusCode.BadRequest }
      ) as NextResponse<ErrorResponse>;
    }

    const [existing] = await db
      .select()
      .from(playersTable)
      .where(eq(playersTable.id, user.id));

    if (!existing) {
      return NextResponse.json(
        { errors: ['Player not found'] },
        { status: HttpStatusCode.NotFound }
      ) as NextResponse<ErrorResponse>;
    }

    if (data.email !== existing.email) {
      const [emailExists] = await db
        .select()
        .from(playersTable)
        .where(eq(playersTable.email, data.email));

      if (emailExists) {
        return NextResponse.json(
          { errors: ['Email already in use'] },
          { status: HttpStatusCode.Conflict }
        ) as NextResponse<ErrorResponse>;
      }
    }

    if (data.nickname !== existing.nickname) {
      const [nicknameExists] = await db
        .select()
        .from(playersTable)
        .where(eq(playersTable.nickname, data.nickname));

      if (nicknameExists) {
        return NextResponse.json(
          { errors: ['Nickname already in use'] },
          { status: HttpStatusCode.Conflict }
        ) as NextResponse<ErrorResponse>;
      }
    }

    const [updated] = await db
      .update(playersTable)
      .set(data)
      .where(eq(playersTable.id, user.id))
      .returning({ id: playersTable.id });

    if (!updated) {
      return NextResponse.json(
        { errors: ['Could not update profile'] },
        { status: HttpStatusCode.NotFound }
      ) as NextResponse<ErrorResponse>;
    }

    return NextResponse.json(
      { message: 'Profile updated successfully' },
      { status: HttpStatusCode.Ok }
    ) as NextResponse<SuccessfulResponse>;
  } catch (error) {
    return Response.json(
      {
        errors: ['An error occurred while updating the notification'],
      } as ErrorResponse,
      { status: HttpStatusCode.InternalServerError }
    );
  }
};
