import { HttpStatusCode } from 'axios';
import { and, count, eq, sql, sum } from 'drizzle-orm';
import { z } from 'zod';

import { db } from '@/db';
import { seriesPlayersTable } from '@/db/schema';
import { getUser } from '@/lib/auth';

type Params = Promise<{ seriesId: string }>;

export const POST = async (_req: Request, segmentData: { params: Params }) => {
  try {
    const params = await segmentData.params;
    const seriesId = z.coerce.number().min(1).parse(params.seriesId);
    const user = await getUser();
    if (!user)
      return Response.json(
        { message: 'Unauthorized' },
        { status: HttpStatusCode.Unauthorized }
      );

    // Get the player from the series
    const [player] =
      (await db
        .select({ mod: seriesPlayersTable.isModerator })
        .from(seriesPlayersTable)
        .where(
          and(
            eq(seriesPlayersTable.seriesId, seriesId),
            eq(seriesPlayersTable.playerId, user.id)
          )
        )) ?? [];

    if (!player) {
      return Response.json(
        { message: 'Player not found in the series' },
        { status: HttpStatusCode.NotFound }
      );
    }

    if (player.mod) {
      // Count mods in the series
      const [queryModCount] = await db
        .select({
          modCount: sql<number>`SUM(CASE WHEN ${seriesPlayersTable.isModerator} = true THEN 1 ELSE 0 END`,
        })
        .from(seriesPlayersTable)
        .where(eq(seriesPlayersTable.seriesId, seriesId));
      if (queryModCount.modCount === 1) {
        return Response.json(
          { message: 'Cannot unregister the last moderator' },
          { status: HttpStatusCode.BadRequest }
        );
      }
    }

    await db
      .delete(seriesPlayersTable)
      .where(
        and(
          eq(seriesPlayersTable.seriesId, seriesId),
          eq(seriesPlayersTable.playerId, user.id)
        )
      );
    return Response.json({ message: 'Player unregistered' });
  } catch (e) {
    if (e instanceof z.ZodError) {
      return Response.json(
        { message: 'Bad request', errors: e.errors },
        { status: HttpStatusCode.BadRequest }
      );
    }
    console.error(e);
    return Response.json(
      { message: 'Internal server error' },
      { status: HttpStatusCode.InternalServerError }
    );
  }
};
