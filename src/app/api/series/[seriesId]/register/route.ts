import { HttpStatusCode } from 'axios';
import { and, eq } from 'drizzle-orm';
import { z } from 'zod';

import { db } from '@/db';
import { seriesPlayersTable } from '@/db/schema';
import { getUser } from '@/lib/auth';

const registerBody = z.object({
  playerId: z.coerce.number().optional().nullable(),
});

type Params = Promise<{ seriesId: string }>;

export const POST = async (req: Request, segmentData: { params: Params }) => {
  try {
    const params = await segmentData.params;
    const body = registerBody.parse(await req.json().catch(() => {}));
    const seriesId = z.coerce.number().min(1).parse(params.seriesId);
    const user = await getUser();
    if (!user)
      return Response.json(
        { message: 'Unauthorized' },
        { status: HttpStatusCode.Unauthorized }
      );

    // Check if playerId is provided, diff from user and if user is a mod
    if (
      body.playerId &&
      body.playerId !== user.id &&
      !checkMod(user.id, seriesId)
    ) {
      return Response.json(
        { message: 'Unauthorized' },
        { status: HttpStatusCode.Unauthorized }
      );
    }

    await db
      .insert(seriesPlayersTable)
      .values({
        seriesId,
        playerId: body.playerId ?? user.id,
      })
      .onConflictDoNothing();
    return Response.json({ message: 'Player registered' });
  } catch (e) {
    if (e instanceof z.ZodError) {
      return Response.json(
        { message: 'Bad request', errors: e.errors },
        { status: HttpStatusCode.BadRequest }
      );
    }
    console.error(e);
    return Response.json(
      { message: 'Internal server error' },
      { status: HttpStatusCode.InternalServerError }
    );
  }
};

async function checkMod(playerId: number, seriesId: number) {
  const [player] =
    (await db
      .select({ isMod: seriesPlayersTable.isModerator })
      .from(seriesPlayersTable)
      .where(
        and(
          eq(seriesPlayersTable.playerId, playerId),
          eq(seriesPlayersTable.seriesId, seriesId)
        )
      )) ?? [];
  return player.isMod;
}
