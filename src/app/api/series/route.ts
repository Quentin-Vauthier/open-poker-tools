import { HttpStatusCode } from 'axios';
import { asc, ilike } from 'drizzle-orm';
import { NextRequest } from 'next/server';
import { ZodError } from 'zod';

import { db } from '@/db';
import { seriesPlayersTable, seriesTable } from '@/db/schema';
import { getUser } from '@/lib/auth';
import { createSeriesSchema } from '@/lib/zod';

export const POST = async (req: Request) => {
  try {
    const user = await getUser();
    if (!user) {
      return Response.json(
        { message: 'You must be logged in to create a series.' },
        { status: HttpStatusCode.Unauthorized }
      );
    }

    const body = await req.json();
    const { name, locationId } = createSeriesSchema.parse(body);
    const [series] = await db
      .insert(seriesTable)
      .values({ name, locationId })
      .returning({
        id: seriesTable.id,
        name: seriesTable.name,
        locationId: seriesTable.locationId,
      });
    await db.insert(seriesPlayersTable).values({
      seriesId: series.id,
      playerId: user.id,
      isModerator: true,
    });
    return Response.json(series, {
      status: HttpStatusCode.Created,
    });
  } catch (error) {
    if (error instanceof ZodError) {
      return Response.json(
        {
          errors: error.errors.map((error) => ({
            message: error.message,
            path: error.path,
          })),
        },
        { status: HttpStatusCode.BadRequest }
      );
    }
    // TODO: Logger
    console.error(error);
    return Response.json(
      { error: 'Internal Server Error' },
      { status: HttpStatusCode.InternalServerError }
    );
  }
};

export const GET = async (req: NextRequest) => {
  try {
    const searchParams = req.nextUrl.searchParams;
    const query = searchParams.get('query');
    const locations = await db
      .select({
        id: seriesTable.id,
        name: seriesTable.name,
      })
      .from(seriesTable)
      .where(ilike(seriesTable.name, `%${query}%`))
      .orderBy(asc(seriesTable.name));
    return Response.json(locations);
  } catch (error) {
    return Response.json(
      { errors: ['An error occurred while fetching locations'] },
      { status: HttpStatusCode.InternalServerError }
    );
  }
};
