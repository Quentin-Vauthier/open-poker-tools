import { HttpStatusCode } from 'axios';
import { eq } from 'drizzle-orm';
import { z } from 'zod';

import { db } from '@/db';
import { eventsTable, notificationsTable } from '@/db/schema';
import { getUser } from '@/lib/auth';
import { OPTEvent } from '@/lib/events/types';
import { formatZodError, sanitizeInput } from '@/lib/utils';
import { Role } from '@/types';
import { BaseResponse, ErrorResponse } from '@/app/api/types';

export const GET = async () => {
  try {
    const user = await getUser();
    if (!user) {
      return Response.json(
        {
          message: 'You must be a logged in admin user to view notifications.',
        },
        { status: HttpStatusCode.Unauthorized }
      );
    } else if (user.role !== Role.Admin) {
      return Response.json(
        { message: 'You must be an admin to view notifications.' },
        { status: HttpStatusCode.Unauthorized }
      );
    }
    const notifications = await db
      .select({
        code: notificationsTable.code,
        eventCode: notificationsTable.eventCode,
        eventTitle: eventsTable.title,
        eventDesc: eventsTable.description,
        title: notificationsTable.title,
        description: notificationsTable.description,
        subject: notificationsTable.subject,
        message: notificationsTable.message,
        html: notificationsTable.html,
        color: notificationsTable.color,
      })
      .from(notificationsTable)
      .innerJoin(
        eventsTable,
        eq(notificationsTable.eventCode, eventsTable.code)
      )
      .orderBy(notificationsTable.code);
    return Response.json(notifications);
  } catch (error) {
    return Response.json(
      { errors: ['An error occurred while fetching notifications'] },
      { status: HttpStatusCode.InternalServerError }
    );
  }
};

const createNotifBody = z
  .object({
    code: z.string(),
    eventCode: z.nativeEnum(OPTEvent),
    title: z.string(),
    description: z.string(),
    subject: z.string(),
    message: z.string(),
    html: z.string(),
    color: z.string(),
  })
  .transform((data) => {
    return {
      ...data,
      html: data.html ? sanitizeInput(data.html) : '',
    };
  });

export const POST = async (req: Request) => {
  try {
    const user = await getUser();
    if (!user) {
      return Response.json(
        {
          message:
            'You must be a logged in admin user to create notifications.',
        },
        { status: HttpStatusCode.Unauthorized }
      );
    } else if (user.role !== Role.Admin) {
      return Response.json(
        { message: 'You must be an admin to create notifications.' },
        { status: HttpStatusCode.Unauthorized }
      );
    }
    const json = await req.json().catch(() => null);
    const { success, data, error } = createNotifBody.safeParse(json);
    if (!success) {
      return Response.json(
        { errors: formatZodError(error) },
        { status: HttpStatusCode.BadRequest }
      );
    }

    const [created] = await db
      .insert(notificationsTable)
      .values(data)
      .onConflictDoNothing()
      .returning({ code: notificationsTable.code });

    if (!created) {
      return Response.json(
        {
          errors: ['A Notification with this code already exists'],
        } as ErrorResponse,
        { status: HttpStatusCode.Conflict }
      );
    }

    return Response.json(
      { message: 'Notification created', code: created.code } as BaseResponse<{
        code: string;
      }>,
      { status: HttpStatusCode.Created }
    );
  } catch (error) {
    return Response.json(
      {
        errors: ['An error occurred while creating the notification'],
      } as ErrorResponse,
      { status: HttpStatusCode.InternalServerError }
    );
  }
};
