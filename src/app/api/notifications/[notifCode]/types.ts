import { ErrorResponse, SuccessfulResponse } from '@/app/api/types';

export type UpdateNotifResponse = SuccessfulResponse | ErrorResponse;
