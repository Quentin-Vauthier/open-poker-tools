import { HttpStatusCode } from 'axios';
import { eq } from 'drizzle-orm';
import { z } from 'zod';

import { ErrorResponse, SuccessfulResponse } from '@/app/api/types';
import { db } from '@/db';
import { notificationsTable } from '@/db/schema';
import { getUser } from '@/lib/auth';
import { formatZodError, sanitizeInput } from '@/lib/utils';
import { Role } from '@/types';

type Params = Promise<{ notifCode: string }>;

const updateNotifSchema = z
  .object({
    code: z.string().optional(),
    title: z.string().optional(),
    description: z.string().optional(),
    subject: z.string().optional(),
    message: z.string().optional(),
    html: z.string().optional(),
    color: z.string().optional(),
  })
  .transform((data) => {
    return {
      ...data,
      html: data.html ? sanitizeInput(data.html) : undefined,
    };
  });

export const PUT = async (req: Request, segmentData: { params: Params }) => {
  try {
    const user = await getUser();
    const { notifCode } = await segmentData.params;
    if (!user) {
      return Response.json(
        {
          errors: [
            'You must be a logged in admin user to update notifications.',
          ],
        } as ErrorResponse,
        { status: HttpStatusCode.Unauthorized }
      );
    } else if (user.role !== Role.Admin) {
      return Response.json(
        {
          errors: ['You must be an admin to update notifications.'],
        } as ErrorResponse,
        { status: HttpStatusCode.Unauthorized }
      );
    }

    const json = await req.json().catch(() => null);
    const { success, data, error } = updateNotifSchema.safeParse(json);

    if (!success) {
      return Response.json({ errors: formatZodError(error) } as ErrorResponse, {
        status: HttpStatusCode.BadRequest,
      });
    }

    if (data.code && data.code !== notifCode) {
      // Check if the new code already exists
      const existingWithNewCode = await db
        .select()
        .from(notificationsTable)
        .where(eq(notificationsTable.code, data.code));
      if (existingWithNewCode.length) {
        return Response.json(
          {
            errors: ['Notification code already exists'],
          } as ErrorResponse,
          { status: HttpStatusCode.Conflict }
        );
      }
    }

    const existing = await db
      .select()
      .from(notificationsTable)
      .where(eq(notificationsTable.code, notifCode));
    if (!existing.length) {
      return Response.json(
        {
          errors: ['Notification not found'],
        } as ErrorResponse,
        { status: HttpStatusCode.NotFound }
      );
    }

    const updated = await db
      .update(notificationsTable)
      .set(data)
      .where(eq(notificationsTable.code, notifCode))
      .returning({ code: notificationsTable.code });

    if (!updated.length) {
      return Response.json(
        {
          errors: ['An error occurred while updating the notification'],
        } as ErrorResponse,
        { status: HttpStatusCode.InternalServerError }
      );
    }

    return Response.json({
      message: 'Notification updated',
    } as SuccessfulResponse);
  } catch (error) {
    return Response.json(
      {
        errors: ['An error occurred while updating the notification'],
      } as ErrorResponse,
      { status: HttpStatusCode.InternalServerError }
    );
  }
};

export const DELETE = async (req: Request, segmentData: { params: Params }) => {
  try {
    const user = await getUser();
    const { notifCode } = await segmentData.params;
    if (!user) {
      return Response.json(
        {
          errors: [
            'You must be a logged in admin user to delete notifications.',
          ],
        } as ErrorResponse,
        { status: HttpStatusCode.Unauthorized }
      );
    } else if (user.role !== Role.Admin) {
      return Response.json(
        {
          errors: ['You must be an admin to delete notifications.'],
        } as ErrorResponse,
        { status: HttpStatusCode.Unauthorized }
      );
    }

    await db
      .delete(notificationsTable)
      .where(eq(notificationsTable.code, notifCode));
    return Response.json({
      message: 'Notification deleted',
    } as SuccessfulResponse);
  } catch (error) {
    return Response.json(
      {
        errors: ['An error occurred while deleting the notification'],
      } as ErrorResponse,
      { status: HttpStatusCode.InternalServerError }
    );
  }
};
