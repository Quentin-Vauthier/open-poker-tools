import { BaseResponse } from '@/app/api/types';

export type Notification = {
  code: string;
  eventCode: string;
  title: string;
  description: string;
  subject: string;
  message: string;
  html: string;
  color: string;
};

type NotificationWithEvent = Notification & {
  eventTitle: string;
  eventDesc: string;
};

export type GetNotificationsResponse = NotificationWithEvent[];

export type CreateNotificationResponse = BaseResponse<{ code: string }>;
