import { sanitizeInput } from '@/lib/utils';
import { HttpStatusCode } from 'axios';
import { and, eq, ne, sql } from 'drizzle-orm';
import { ZodError } from 'zod';

import { db } from '@/db';
import {
  seriesPlayersTable,
  tournamentPlayersTable,
  tournamentsTable,
} from '@/db/schema';
import { getUser } from '@/lib/auth';
import EventManager from '@/lib/events';
import { OPTEvent } from '@/lib/events/types';
import { createTournamentSchema } from '@/lib/zod';
import { Role, TournamentVisibility } from '@/types';

export const POST = async (req: Request) => {
  try {
    const user = await getUser();

    if (!user) {
      return Response.json(
        { message: 'You must be logged in to create a tournament.' },
        { status: HttpStatusCode.Unauthorized }
      );
    }

    const rawBody = await req.json();
    const body = createTournamentSchema.parse(rawBody);

    const tournamentDetails = {
      name: body.name,
      visibility: body.visibility,
      date: body.date,
      description: body.description,
      buyIn: body.buyIn,
      knockout: body.knockout,
      progressiveKo: body.progressiveKo,
      knockoutBounty: body.knockoutBounty,
      canReentry: body.canReentry,
      reentryEnd: body.reentryEnd,
      canLateReg: body.canLateReg,
      lateRegEnd: body.lateRegEnd,
      tableSize: body.tableSize,
      minTableSize: body.minTableSize,
      finalTableSize: body.finalTableSize,
      maxPlayers: body.maxPlayers,
      seriesId: body.seriesId,
      structureId: body.structureId,
      startingStack: body.startingStack,
      variant: body.variant,
      locationId: body.locationId,
    };

    if (
      user.role !== Role.Admin &&
      body.visibility === TournamentVisibility.Public
    ) {
      return Response.json(
        { message: 'You do not have permission to create public tournaments.' },
        { status: HttpStatusCode.Forbidden }
      );
    }

    const [createdTournament] = await db
      .insert(tournamentsTable)
      .values(tournamentDetails)
      .returning({ id: tournamentsTable.id });

    EventManager.trigger(OPTEvent.GLOBAL_TOURNAMENT_CREATE, {
      objectId: createdTournament.id,
      ...tournamentDetails,
    });

    await db.insert(tournamentPlayersTable).values({
      tournamentId: createdTournament.id,
      playerId: user.id,
      isModerator: true,
    });

    EventManager.trigger(OPTEvent.PLAYER_JOIN_TOURNAMENT, {
      objectId: user.id,
      tournamentId: createdTournament.id,
      ...tournamentDetails,
    });

    if (body.seriesId) {
      // Add all players from the series to the tournament (except the current user)
      const seriesPlayers = await db
        .insert(tournamentPlayersTable)
        .select(
          db
            .select({
              tournamentId: sql`${createdTournament.id}`.as(
                tournamentPlayersTable.tournamentId.name
              ),
              playerId: seriesPlayersTable.playerId,
              isModerator: seriesPlayersTable.isModerator,
              createdAt: sql`NOW()`.as(tournamentPlayersTable.createdAt.name),
            })
            .from(seriesPlayersTable)
            .where(
              and(
                eq(seriesPlayersTable.seriesId, body.seriesId),
                ne(seriesPlayersTable.playerId, user.id)
              )
            )
        )
        .returning({
          tournamentId: tournamentPlayersTable.tournamentId,
          playerId: seriesPlayersTable.playerId,
          isModerator: seriesPlayersTable.isModerator,
        });

      for (const seriesPlayer of seriesPlayers) {
        EventManager.trigger(OPTEvent.PLAYER_JOIN_TOURNAMENT, {
          objectId: seriesPlayer.playerId.toString(),
          tournamentId: seriesPlayer.tournamentId,
          ...tournamentDetails,
        });
      }
    }

    return Response.json({ tournamentId: createdTournament.id });
  } catch (e) {
    if (e instanceof ZodError) {
      return Response.json(
        { errors: e.errors },
        { status: HttpStatusCode.BadRequest }
      );
    }
    // TODO : Logger
    console.error(e);
    return Response.json(
      { message: 'An unknown error occured, please try again later.' },
      { status: HttpStatusCode.InternalServerError }
    );
  }
};
