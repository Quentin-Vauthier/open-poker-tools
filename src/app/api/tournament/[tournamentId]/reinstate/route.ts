import { HttpStatusCode } from 'axios';
import { and, eq, gt, sql } from 'drizzle-orm';
import { NextRequest, NextResponse } from 'next/server';
import { z } from 'zod';

import { ErrorResponse, SuccessfulResponse } from '@/app/api/types';
import { db } from '@/db';
import { tournamentRankingsTable } from '@/db/schema';
import { getUser } from '@/lib/auth';
import { playerIsTournamentMod } from '@/lib/tournament-utils';
import { formatZodError } from '@/lib/utils';

type Params = Promise<{ tournamentId: string }>;

const reinstatePlayerSchema = z.object({
  playerId: z.number().min(1),
});

export const POST = async (
  req: NextRequest,
  segmentData: { params: Params }
) => {
  try {
    const params = await segmentData.params;
    const user = await getUser();
    if (!user)
      return NextResponse.json(
        { errors: ['Unauthorized'] },
        { status: HttpStatusCode.Unauthorized }
      ) as NextResponse<ErrorResponse>;

    // Check if the user is a tournament mod
    if (!(await playerIsTournamentMod(user.id, Number(params.tournamentId)))) {
      return NextResponse.json(
        { errors: ['You do not have permission to reinstate players'] },
        { status: HttpStatusCode.Forbidden }
      ) as NextResponse<ErrorResponse>;
    }

    // Read the request body
    const json = await req.json().catch(() => null);
    const {
      success,
      data: body,
      error,
    } = reinstatePlayerSchema.safeParse(json);

    if (!success) {
      return NextResponse.json(
        { errors: formatZodError(error) },
        { status: HttpStatusCode.BadRequest }
      ) as NextResponse<ErrorResponse>;
    }

    // Get the current position of the player being reinstated
    const playerRanking = await db
      .select({
        position: tournamentRankingsTable.position,
      })
      .from(tournamentRankingsTable)
      .where(
        and(
          eq(tournamentRankingsTable.tournamentId, Number(params.tournamentId)),
          eq(tournamentRankingsTable.playerId, body.playerId)
        )
      )
      .then((results) => results[0]);

    if (!playerRanking?.position) {
      return NextResponse.json(
        { errors: ['Player is not eliminated'] },
        { status: HttpStatusCode.BadRequest }
      ) as NextResponse<ErrorResponse>;
    }

    // Update positions of players eliminated after this player
    await db
      .update(tournamentRankingsTable)
      .set({
        position: sql<number>`${tournamentRankingsTable.position} + 1`,
      })
      .where(
        and(
          eq(tournamentRankingsTable.tournamentId, Number(params.tournamentId)),
          gt(tournamentRankingsTable.position, playerRanking.position)
        )
      );

    // Remove position from reinstated player
    await db
      .update(tournamentRankingsTable)
      .set({
        position: null,
      })
      .where(
        and(
          eq(tournamentRankingsTable.tournamentId, Number(params.tournamentId)),
          eq(tournamentRankingsTable.playerId, body.playerId)
        )
      );

    return NextResponse.json({
      message: 'Player has been reinstated to the tournament',
    }) as NextResponse<SuccessfulResponse>;
  } catch (e) {
    console.error(e);
    return NextResponse.json(
      { errors: ['Unable to reinstate player, please try again later'] },
      { status: HttpStatusCode.InternalServerError }
    ) as NextResponse<ErrorResponse>;
  }
};
