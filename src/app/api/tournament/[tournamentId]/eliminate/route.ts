import { HttpStatusCode } from 'axios';
import { and, eq, isNotNull, sql } from 'drizzle-orm';
import { NextRequest, NextResponse } from 'next/server';
import { z } from 'zod';

import { ErrorResponse, SuccessfulResponse } from '@/app/api/types';
import { db } from '@/db';
import { tournamentPlayersTable, tournamentRankingsTable } from '@/db/schema';
import { getUser } from '@/lib/auth';
import { playerIsTournamentMod } from '@/lib/tournament-utils';

type Params = Promise<{ tournamentId: string }>;

const eliminatePlayerSchema = z.object({
  playerId: z.number().min(1),
});

export const POST = async (
  req: NextRequest,
  segmentData: { params: Params }
) => {
  try {
    const params = await segmentData.params;
    const user = await getUser();
    if (!user)
      return NextResponse.json(
        { errors: ['Unauthorized'] },
        { status: HttpStatusCode.Unauthorized }
      ) as NextResponse<ErrorResponse>;

    // Check if the user is a tournament mod
    if (!(await playerIsTournamentMod(user.id, Number(params.tournamentId)))) {
      return NextResponse.json(
        { errors: ['You do not have permission to eliminate players'] },
        { status: HttpStatusCode.Forbidden }
      ) as NextResponse<ErrorResponse>;
    }

    // Read the request body
    const json = await req.json().catch(() => null);
    const {
      success,
      data: body,
      error,
    } = eliminatePlayerSchema.safeParse(json);

    if (!success) {
      return NextResponse.json(
        { errors: error.errors.map((err) => err.message) },
        { status: HttpStatusCode.BadRequest }
      ) as NextResponse<ErrorResponse>;
    }

    // Calculate the position of the eliminated player
    const totalCheckedInPlayers = await db
      .select({
        count: sql<number>`COUNT(*)`,
      })
      .from(tournamentPlayersTable)
      .leftJoin(
        tournamentRankingsTable,
        and(
          eq(tournamentPlayersTable.playerId, tournamentRankingsTable.playerId),
          eq(
            tournamentPlayersTable.tournamentId,
            tournamentRankingsTable.tournamentId
          )
        )
      )
      .where(
        and(
          eq(tournamentPlayersTable.tournamentId, Number(params.tournamentId)),
          sql<boolean>`CAST(CASE WHEN ${tournamentRankingsTable.playerId} IS NOT NULL THEN 1 ELSE 0 END AS BOOLEAN)`
        )
      )
      .then((result) => result[0].count);

    const currentEliminatedPlayers = await db
      .select({
        count: sql<number>`COUNT(*)`,
      })
      .from(tournamentRankingsTable)
      .where(
        and(
          eq(tournamentRankingsTable.tournamentId, Number(params.tournamentId)),
          isNotNull(tournamentRankingsTable.position)
        )
      )
      .then((result) => result[0].count);

    console.log(totalCheckedInPlayers, currentEliminatedPlayers);

    const position = totalCheckedInPlayers - currentEliminatedPlayers;

    // Eliminate the player
    await db
      .update(tournamentRankingsTable)
      .set({
        position,
      })
      .where(
        and(
          eq(tournamentRankingsTable.tournamentId, Number(params.tournamentId)),
          eq(tournamentRankingsTable.playerId, body.playerId)
        )
      );

    return NextResponse.json({
      message: 'Player has been eliminated from the tournament',
    }) as NextResponse<SuccessfulResponse>;
  } catch (e) {
    console.error(e);
    return NextResponse.json(
      { errors: ['Unable to eliminate player, please try again later'] },
      { status: HttpStatusCode.InternalServerError }
    ) as NextResponse<ErrorResponse>;
  }
};
