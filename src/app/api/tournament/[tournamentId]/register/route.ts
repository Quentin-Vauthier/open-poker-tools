import { HttpStatusCode } from 'axios';
import { and, count, eq, inArray } from 'drizzle-orm';
import { z } from 'zod';

import { db } from '@/db';
import {
  tournamentInvitationsTable,
  tournamentPlayersTable,
  tournamentsTable,
} from '@/db/schema';
import { getUser } from '@/lib/auth';
import { playerHasAccessToTournament } from '@/lib/tournament-utils';
import { NextResponse } from 'next/server';

type Params = Promise<{ tournamentId: string }>;

export const POST = async (req: Request, segmentData: { params: Params }) => {
  try {
    const params = await segmentData.params;
    const user = await getUser();
    if (!user)
      return Response.json(
        { message: 'Unauthorized' },
        { status: HttpStatusCode.Unauthorized }
      );

    const body = await req.json().catch(() => null);
    const playerId = body?.playerId ?? user.id;
    const tournamentId = z.coerce.number().min(1).parse(params.tournamentId);

    // Check if the user has access to the tournament
    if (!(await playerHasAccessToTournament(user.id, tournamentId))) {
      return NextResponse.json(
        { errors: ['You do not have access to this tournament'] },
        { status: HttpStatusCode.Forbidden }
      );
    }

    const players = await db
      .select({
        playerId: tournamentPlayersTable.playerId,
        mod: tournamentPlayersTable.isModerator,
      })
      .from(tournamentPlayersTable)
      .where(
        and(
          eq(tournamentPlayersTable.tournamentId, tournamentId),
          inArray(tournamentPlayersTable.playerId, [playerId, user.id])
        )
      );

    // If the user is registering another player, they must be a mod
    if (
      playerId !== user.id &&
      !players.find((p) => p.playerId === user.id && p.mod)
    ) {
      return Response.json(
        { message: 'Unauthorized' },
        { status: HttpStatusCode.Unauthorized }
      );
    }

    // If the player is already registered, return an error
    if (players.find((p) => p.playerId === playerId)) {
      return Response.json(
        { message: 'Player already registered' },
        { status: HttpStatusCode.BadRequest }
      );
    }

    // Check if the tournament is full
    const [[tournament], [currentPlayers]] = await Promise.all([
      db
        .select({
          maxPlayers: tournamentsTable.maxPlayers,
          waitlist: tournamentsTable.waitlist,
        })
        .from(tournamentsTable)
        .where(eq(tournamentsTable.id, tournamentId)),
      db
        .select({
          count: count(),
        })
        .from(tournamentPlayersTable)
        .where(eq(tournamentPlayersTable.tournamentId, tournamentId)),
    ]);

    if (currentPlayers.count >= tournament.maxPlayers && !tournament.waitlist) {
      return Response.json(
        { message: 'Tournament is full' },
        { status: HttpStatusCode.BadRequest }
      );
    }

    // Make a transaction to insert the player and delete the invite
    await db
      .transaction(async (tx) => {
        const insertPlayer = tx
          .insert(tournamentPlayersTable)
          .values({
            tournamentId,
            playerId,
          })
          .onConflictDoNothing();
        const deleteInvit = tx
          .delete(tournamentInvitationsTable)
          .where(
            and(
              eq(tournamentInvitationsTable.tournamentId, tournamentId),
              eq(tournamentInvitationsTable.playerId, playerId)
            )
          );
        await Promise.all([insertPlayer, deleteInvit]);
      })
      .catch((err) => {
        throw new Error('Failed to register player', err);
      });

    return Response.json({ message: 'Player registered' });
  } catch (e) {
    if (e instanceof z.ZodError) {
      return Response.json(
        { message: 'Bad request', errors: e.errors },
        { status: HttpStatusCode.BadRequest }
      );
    }
    console.error(e);
    return Response.json(
      { message: 'Internal server error, please try again later' },
      { status: HttpStatusCode.InternalServerError }
    );
  }
};
