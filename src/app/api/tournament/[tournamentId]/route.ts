import { HttpStatusCode } from 'axios';
import { eq } from 'drizzle-orm';
import { NextRequest } from 'next/server';

import { db } from '@/db';
import { seriesTable, structuresTable, tournamentsTable } from '@/db/schema';
import { getUser } from '@/lib/auth';

type Params = Promise<{ tournamentId: string }>;

export const GET = async (
  req: NextRequest,
  segmentData: { params: Params }
) => {
  try {
    const params = await segmentData.params;
    const tournamentId = params.tournamentId;
    const user = await getUser();
    const searchParams = req.nextUrl.searchParams;

    if (!user) {
      return Response.json(
        { message: 'You must be logged in to view tournaments.' },
        { status: HttpStatusCode.Unauthorized }
      );
    }

    const [tournament] = searchParams.has('expanded')
      ? await db
          .select({
            id: tournamentsTable.id,
            name: tournamentsTable.name,
            date: tournamentsTable.date,
            startTime: tournamentsTable.startTime,
            endTime: tournamentsTable.endTime,
            buyIn: tournamentsTable.buyIn,
            knockout: tournamentsTable.knockout,
            progressiveKo: tournamentsTable.progressiveKo,
            knockoutBounty: tournamentsTable.knockoutBounty,
            canReentry: tournamentsTable.canReentry,
            reentryEnd: tournamentsTable.reentryEnd,
            canLateReg: tournamentsTable.canLateReg,
            lateRegEnd: tournamentsTable.lateRegEnd,
            tableSize: tournamentsTable.tableSize,
            minTableSize: tournamentsTable.minTableSize,
            maxPlayers: tournamentsTable.maxPlayers,
            variant: tournamentsTable.variant,
            startingStack: tournamentsTable.startingStack,
            finalTableSize: tournamentsTable.finalTableSize,
            seriesId: tournamentsTable.seriesId,
            structureId: tournamentsTable.structureId,
            series: seriesTable.name,
            structure: structuresTable.name,
            locationId: seriesTable.locationId,
          })
          .from(tournamentsTable)
          .leftJoin(
            structuresTable,
            eq(tournamentsTable.structureId, structuresTable.id)
          )
          .leftJoin(seriesTable, eq(tournamentsTable.seriesId, seriesTable.id))
          .where(eq(tournamentsTable.id, parseInt(tournamentId)))
      : await db
          .select()
          .from(tournamentsTable)
          .where(eq(tournamentsTable.id, parseInt(tournamentId)));

    if (!tournament) {
      return Response.json(
        { message: 'Tournament not found' },
        { status: HttpStatusCode.NotFound }
      );
    }

    return Response.json(tournament);
  } catch (e) {
    // TODO : Logger
    console.error(e);
    return Response.json(
      { message: 'An unknown error occured, please try again later.' },
      { status: HttpStatusCode.InternalServerError }
    );
  }
};
