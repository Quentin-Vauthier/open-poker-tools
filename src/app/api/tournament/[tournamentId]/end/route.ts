import { HttpStatusCode } from 'axios';
import { and, eq, sql } from 'drizzle-orm';
import { NextRequest, NextResponse } from 'next/server';

import { ErrorResponse, SuccessfulResponse } from '@/app/api/types';
import { db } from '@/db';
import { tournamentRankingsTable, tournamentsTable } from '@/db/schema';
import { getUser } from '@/lib/auth';
import { playerIsTournamentMod } from '@/lib/tournament-utils';

type Params = Promise<{ tournamentId: string }>;

export const POST = async (
  req: NextRequest,
  segmentData: { params: Params }
) => {
  try {
    const params = await segmentData.params;
    const user = await getUser();
    if (!user)
      return NextResponse.json(
        { errors: ['Unauthorized'] },
        { status: HttpStatusCode.Unauthorized }
      ) as NextResponse<ErrorResponse>;

    if (!(await playerIsTournamentMod(user.id, Number(params.tournamentId)))) {
      return NextResponse.json(
        { errors: ['You do not have permission to end this tournament'] },
        { status: HttpStatusCode.Forbidden }
      ) as NextResponse<ErrorResponse>;
    }

    // Set end time
    await db
      .update(tournamentsTable)
      .set({ endTime: new Date() })
      .where(eq(tournamentsTable.id, Number(params.tournamentId)));

    // Find lowest position to calculate delta
    const [positions] = await db
      .select({
        min: sql<number>`min(${tournamentRankingsTable.position})`,
        max: sql<number>`max(${tournamentRankingsTable.position})`,
      })
      .from(tournamentRankingsTable)
      .where(
        and(
          eq(tournamentRankingsTable.tournamentId, Number(params.tournamentId)),
          sql`${tournamentRankingsTable.position} is not null`
        )
      );

    if (positions?.min && positions?.max) {
      const delta = positions.min - 1;

      // Only update if there's a gap to close
      if (delta > 0) {
        await db
          .update(tournamentRankingsTable)
          .set({
            position: sql`${tournamentRankingsTable.position} - ${delta}`,
          })
          .where(
            and(
              eq(
                tournamentRankingsTable.tournamentId,
                Number(params.tournamentId)
              ),
              sql`${tournamentRankingsTable.position} is not null`
            )
          );
      }
    }

    // Delete entries for players without a position
    await db
      .delete(tournamentRankingsTable)
      .where(
        and(
          eq(tournamentRankingsTable.tournamentId, Number(params.tournamentId)),
          sql`${tournamentRankingsTable.position} is null`
        )
      );

    return NextResponse.json(
      { message: 'Tournament has ended and rankings updated' },
      { status: HttpStatusCode.Ok }
    ) as NextResponse<SuccessfulResponse>;
  } catch (error) {
    console.error(error);
    return NextResponse.json(
      { errors: ['Unable to end the tournament'] },
      { status: HttpStatusCode.InternalServerError }
    ) as NextResponse<ErrorResponse>;
  }
};
