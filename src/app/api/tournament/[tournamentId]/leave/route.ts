import { db } from '@/db';
import { tournamentRankingsTable } from '@/db/schema';
import { HttpStatusCode } from 'axios';
import { and, eq } from 'drizzle-orm';

type Params = Promise<{ tournamentId: string }>;

export const POST = async (req: Request, segmentData: { params: Params }) => {
  try {
    const params = await segmentData.params;
    const body = await req.json();
    const playerId = body.playerId;
    const tournamentId = parseInt(params.tournamentId);
    if (isNaN(tournamentId))
      return Response.json(
        { body: 'Invalid tournament ID' },
        { status: HttpStatusCode.BadRequest }
      );
    if (!playerId)
      return Response.json(
        { body: 'Player ID is required' },
        { status: HttpStatusCode.BadRequest }
      );
    if (isNaN(playerId))
      return Response.json(
        { body: 'Invalid player ID' },
        { status: HttpStatusCode.BadRequest }
      );
    const deleted = await db
      .delete(tournamentRankingsTable)
      .where(
        and(
          eq(tournamentRankingsTable.playerId, playerId),
          eq(tournamentRankingsTable.tournamentId, tournamentId)
        )
      )
      .returning();
    if (deleted)
      return Response.json(
        { body: 'Player left tournament' },
        { status: HttpStatusCode.Ok }
      );
    throw new Error('Failed to leave tournament');
  } catch (e) {
    console.error(e);
    return Response.json(
      { body: 'Failed to leave tournament' },
      { status: HttpStatusCode.InternalServerError }
    );
  }
};
