import { HttpStatusCode } from 'axios';
import { and, eq, inArray, sql } from 'drizzle-orm';
import { z } from 'zod';

import { db } from '@/db';
import { tournamentPlayersTable, tournamentRankingsTable } from '@/db/schema';
import { getUser } from '@/lib/auth';
import {
  playerHasAccessToTournament,
  playerIsTournamentMod,
} from '@/lib/tournament-utils';
import { NextResponse } from 'next/server';
import { ErrorResponse } from '@/app/api/types';

const unregisterBodySchema = z.object({
  playerId: z.coerce.number().min(1).nullable().optional(),
});

type Params = Promise<{ tournamentId: string }>;

export const POST = async (req: Request, segmentData: { params: Params }) => {
  try {
    const params = await segmentData.params;
    const tournamentId = z.coerce.number().min(1).parse(params.tournamentId);
    const body = unregisterBodySchema.parse(await req.json().catch(() => null));
    const user = await getUser();
    if (!user)
      return Response.json(
        { message: 'Unauthorized' },
        { status: HttpStatusCode.Unauthorized }
      );

    if (!playerHasAccessToTournament(user.id, tournamentId)) {
      return NextResponse.json(
        { errors: ['You do not have access to this tournament'] },
        { status: HttpStatusCode.Forbidden }
      ) as NextResponse<ErrorResponse>;
    }

    // Get the player from the tournament
    const players =
      (await db
        .select({
          id: tournamentPlayersTable.playerId,
          mod: tournamentPlayersTable.isModerator,
        })
        .from(tournamentPlayersTable)
        .where(
          and(
            eq(tournamentPlayersTable.tournamentId, tournamentId),
            inArray(
              tournamentPlayersTable.playerId,
              body.playerId ? [user.id, body.playerId] : [user.id]
            )
          )
        )) ?? [];

    const playerToUnregister = players.find(
      (p) => p.id === (body.playerId ?? user.id)
    );

    if (!playerToUnregister) {
      return Response.json(
        { message: 'Player not found in the tournament' },
        { status: HttpStatusCode.NotFound }
      );
    }

    if (playerToUnregister.mod) {
      // Count mods in the series
      const [queryModCount] = await db
        .select({
          modCount: sql<string>`SUM(CASE WHEN ${tournamentPlayersTable.isModerator} = true THEN 1 ELSE 0 END)`,
        })
        .from(tournamentPlayersTable)
        .where(eq(tournamentPlayersTable.tournamentId, tournamentId));
      if (parseInt(queryModCount.modCount) === 1) {
        return Response.json(
          { message: 'Cannot unregister the last moderator' },
          { status: HttpStatusCode.BadRequest }
        );
      }
    }

    if (
      body.playerId &&
      body.playerId !== user.id &&
      !playerIsTournamentMod(user.id, tournamentId)
    ) {
      return Response.json(
        { message: 'You are not allowed to unregister other players' },
        { status: HttpStatusCode.BadRequest }
      );
    }

    await Promise.all([
      db
        .delete(tournamentPlayersTable)
        .where(
          and(
            eq(tournamentPlayersTable.tournamentId, tournamentId),
            eq(tournamentPlayersTable.playerId, body.playerId ?? user.id)
          )
        ),
      db
        .delete(tournamentRankingsTable)
        .where(
          and(
            eq(tournamentRankingsTable.tournamentId, tournamentId),
            eq(tournamentRankingsTable.playerId, body.playerId ?? user.id)
          )
        ),
    ]);

    return Response.json({ message: 'Player unregistered' });
  } catch (e) {
    if (e instanceof z.ZodError) {
      return Response.json(
        { message: 'Bad request', errors: e.errors },
        { status: HttpStatusCode.BadRequest }
      );
    }
    console.error(e);
    return Response.json(
      { message: 'Internal server error' },
      { status: HttpStatusCode.InternalServerError }
    );
  }
};
