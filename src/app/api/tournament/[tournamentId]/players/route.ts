import { HttpStatusCode } from 'axios';
import { and, asc, eq, sql } from 'drizzle-orm';
import { NextRequest, NextResponse } from 'next/server';

import { ErrorResponse } from '@/app/api/types';
import { db } from '@/db';
import {
  playersTable,
  tournamentInvitationsTable,
  tournamentPlayersTable,
  tournamentRankingsTable,
} from '@/db/schema';
import { getUser } from '@/lib/auth';
import { playerHasAccessToTournament } from '@/lib/tournament-utils';
import { TournamentPlayer } from '@/types';

type Params = Promise<{ tournamentId: string }>;

export const GET = async (
  req: NextRequest,
  segmentData: { params: Params }
) => {
  try {
    const params = await segmentData.params;
    const tournamentId = parseInt(params.tournamentId);

    const user = await getUser();
    if (!(await playerHasAccessToTournament(user?.id, tournamentId))) {
      return NextResponse.json(
        { errors: ['You do not have access to this tournament'] },
        { status: HttpStatusCode.Forbidden }
      ) as NextResponse<ErrorResponse>;
    }

    const selectInvited = req.nextUrl.searchParams.has('invited');
    if (selectInvited) {
      const invitedPlayers = await db
        .select({
          id: tournamentInvitationsTable.playerId,
          name: playersTable.nickname,
        })
        .from(tournamentInvitationsTable)
        .innerJoin(
          playersTable,
          eq(tournamentInvitationsTable.playerId, playersTable.id)
        )
        .where(and(eq(tournamentInvitationsTable.tournamentId, tournamentId)))
        .orderBy(asc(tournamentInvitationsTable.invitedAt));
      return Response.json(invitedPlayers);
    } else {
      const players = await db
        .select({
          id: tournamentPlayersTable.playerId,
          name: playersTable.nickname,
          mod: tournamentPlayersTable.isModerator,
          checkedIn: sql<boolean>`CAST(CASE WHEN ${tournamentRankingsTable.playerId} IS NOT NULL THEN 1 ELSE 0 END AS BOOLEAN)`,
          position: tournamentRankingsTable.position,
          prize: tournamentRankingsTable.prize,
          bounty: tournamentRankingsTable.bounty,
          koCount: tournamentRankingsTable.koCount,
        })
        .from(tournamentPlayersTable)
        .innerJoin(
          playersTable,
          eq(tournamentPlayersTable.playerId, playersTable.id)
        )
        .leftJoin(
          tournamentRankingsTable,
          and(
            eq(
              tournamentPlayersTable.playerId,
              tournamentRankingsTable.playerId
            ),
            eq(
              tournamentPlayersTable.tournamentId,
              tournamentRankingsTable.tournamentId
            )
          )
        )
        .where(eq(tournamentPlayersTable.tournamentId, tournamentId))
        .orderBy(asc(tournamentPlayersTable.createdAt));
      return Response.json(players as TournamentPlayer[]);
    }
  } catch (e) {
    console.error(e);
    return Response.json(
      { message: 'Error fetching players' },
      { status: 500 }
    );
  }
};
