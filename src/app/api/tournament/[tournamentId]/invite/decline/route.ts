import { HttpStatusCode } from 'axios';
import { and, eq } from 'drizzle-orm';
import { NextRequest, NextResponse } from 'next/server';
import { z } from 'zod';

import { ErrorResponse, SuccessfulResponse } from '@/app/api/types';
import { db } from '@/db';
import { tournamentInvitationsTable } from '@/db/schema';
import { getUser } from '@/lib/auth';
import { playerIsInvitedToTournament } from '@/lib/tournament-utils';
import { formatZodError } from '@/lib/utils';

type Params = Promise<{ tournamentId: string }>;

export const POST = async (
  _req: NextRequest,
  segmentData: { params: Params }
) => {
  try {
    const params = await segmentData.params;
    const {
      data: tournamentId,
      success,
      error,
    } = z.coerce.number().safeParse(params.tournamentId);
    if (!success)
      return NextResponse.json(
        { errors: formatZodError(error) },
        { status: HttpStatusCode.BadRequest }
      ) as NextResponse<ErrorResponse>;

    const user = await getUser();
    if (!user)
      return NextResponse.json(
        { errors: ['Unauthorized'] },
        { status: HttpStatusCode.Unauthorized }
      ) as NextResponse<ErrorResponse>;

    if (!playerIsInvitedToTournament(user.id, tournamentId))
      return NextResponse.json(
        { errors: ['You are not invited to this tournament'] },
        { status: HttpStatusCode.BadRequest }
      ) as NextResponse<ErrorResponse>;

    // Decline the invitation
    const deleted = await db
      .delete(tournamentInvitationsTable)
      .where(
        and(
          eq(tournamentInvitationsTable.tournamentId, tournamentId),
          eq(tournamentInvitationsTable.playerId, user.id)
        )
      );
    if (!deleted)
      return NextResponse.json(
        { errors: ['Failed to decline invitation'] },
        { status: HttpStatusCode.InternalServerError }
      ) as NextResponse<ErrorResponse>;

    return NextResponse.json({
      message: 'Invitation declined',
    }) as NextResponse<SuccessfulResponse>;
  } catch (e) {
    if (e instanceof z.ZodError)
      return NextResponse.json(
        { errors: ['Unauthorized'] },
        { status: HttpStatusCode.Unauthorized }
      ) as NextResponse<ErrorResponse>;
  }
};
