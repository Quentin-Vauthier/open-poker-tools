import { HttpStatusCode } from 'axios';
import { NextRequest, NextResponse } from 'next/server';
import { z } from 'zod';

import { ErrorResponse, SuccessfulResponse } from '@/app/api/types';
import { db } from '@/db';
import { tournamentInvitationsTable } from '@/db/schema';
import { getUser } from '@/lib/auth';
import EventManager from '@/lib/events';
import { OPTEvent } from '@/lib/events/types';
import {
  playerIsRegisteredOrInvitedToTournament,
  playerIsTournamentMod,
} from '@/lib/tournament-utils';
import { formatZodError } from '@/lib/utils';

type Params = Promise<{ tournamentId: string }>;

const invitePlayerSchema = z.object({
  playerId: z.number().min(1),
});

export const POST = async (
  req: NextRequest,
  segmentData: { params: Params }
) => {
  try {
    const params = await segmentData.params;
    const user = await getUser();
    if (!user)
      return NextResponse.json(
        { errors: ['Unauthorized'] },
        { status: HttpStatusCode.Unauthorized }
      ) as NextResponse<ErrorResponse>;

    // Check if the user is a tournament mod
    if (!(await playerIsTournamentMod(user.id, Number(params.tournamentId)))) {
      return NextResponse.json(
        { errors: ['You do not have permission to invite players'] },
        { status: HttpStatusCode.Forbidden }
      ) as NextResponse<ErrorResponse>;
    }

    // Read the request body
    const json = await req.json().catch(() => null);
    const { success, data: body, error } = invitePlayerSchema.safeParse(json);

    if (!success) {
      return NextResponse.json(
        { errors: formatZodError(error) },
        { status: HttpStatusCode.BadRequest }
      ) as NextResponse<ErrorResponse>;
    }

    // Check if the player is already registered or invited to the tournament
    if (
      await playerIsRegisteredOrInvitedToTournament(
        body.playerId,
        Number(params.tournamentId)
      )
    ) {
      return NextResponse.json(
        {
          errors: ['Player is already registered or invited to the tournament'],
        },
        { status: HttpStatusCode.BadRequest }
      ) as NextResponse<ErrorResponse>;
    }

    // Invite the player
    await db
      .insert(tournamentInvitationsTable)
      .values({
        tournamentId: Number(params.tournamentId),
        playerId: body.playerId,
        invitedBy: user.id,
      })
      .onConflictDoNothing();

    EventManager.trigger(OPTEvent.PLAYER_INVITED_TO_TOURNAMENT, {
      objectId: body.playerId,
      tournamentId: Number(params.tournamentId),
      invitedBy: user.id,
    });
    return NextResponse.json({
      message: 'Player has been invited to the tournament',
    }) as NextResponse<SuccessfulResponse>;
  } catch (e) {
    console.error(e);
    return NextResponse.json(
      { errors: ['Unable to invite player, please try again later'] },
      { status: HttpStatusCode.InternalServerError }
    ) as NextResponse<ErrorResponse>;
  }
};
