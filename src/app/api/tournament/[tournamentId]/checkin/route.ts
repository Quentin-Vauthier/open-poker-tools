import { HttpStatusCode } from 'axios';
import { and, eq, inArray } from 'drizzle-orm';

import { db } from '@/db';
import { tournamentPlayersTable, tournamentRankingsTable } from '@/db/schema';
import { getUser } from '@/lib/auth';

type Params = Promise<{ tournamentId: string }>;

export const POST = async (req: Request, segmentData: { params: Params }) => {
  try {
    const params = await segmentData.params;
    const user = await getUser();
    if (!user)
      return Response.json(
        { message: 'Unauthorized' },
        { status: HttpStatusCode.Unauthorized }
      );

    const body = await req.json();
    const playerId = body.playerId ?? user.id;
    const tournamentId = parseInt(params.tournamentId);

    if (isNaN(tournamentId))
      return Response.json(
        { body: 'Invalid tournament ID' },
        { status: HttpStatusCode.BadRequest }
      );
    if (!playerId)
      return Response.json(
        { body: 'Player ID is required' },
        { status: HttpStatusCode.BadRequest }
      );
    if (isNaN(playerId))
      return Response.json(
        { body: 'Invalid player ID' },
        { status: HttpStatusCode.BadRequest }
      );

    const players = await db
      .select({
        playerId: tournamentPlayersTable.playerId,
        mod: tournamentPlayersTable.isModerator,
      })
      .from(tournamentPlayersTable)
      .where(
        and(
          eq(tournamentPlayersTable.tournamentId, tournamentId),
          inArray(tournamentPlayersTable.playerId, [playerId, user.id])
        )
      );

    // Current user must be a mod to check players in
    if (!players.find((p) => p.playerId === user.id && p.mod)) {
      return Response.json(
        { message: 'Unauthorized' },
        { status: HttpStatusCode.Unauthorized }
      );
    }

    await db
      .insert(tournamentRankingsTable)
      .values({ playerId, tournamentId })
      .onConflictDoNothing();
    return Response.json(
      { body: 'Player checked in' },
      { status: HttpStatusCode.Created }
    );
  } catch (e) {
    console.error(e);
    return Response.json(
      { body: 'Failed to check in player' },
      { status: HttpStatusCode.InternalServerError }
    );
  }
};
