import { HttpStatusCode } from 'axios';
import { eq } from 'drizzle-orm';
import { NextRequest, NextResponse } from 'next/server';

import { ErrorResponse, SuccessfulResponse } from '@/app/api/types';
import { db } from '@/db';
import { tournamentsTable } from '@/db/schema';
import { getUser } from '@/lib/auth';
import { playerIsTournamentMod } from '@/lib/tournament-utils';

type Params = Promise<{ tournamentId: string }>;

export const POST = async (
  req: NextRequest,
  segmentData: { params: Params }
) => {
  try {
    const params = await segmentData.params;
    const user = await getUser();
    if (!user)
      return NextResponse.json(
        { errors: ['Unauthorized'] },
        { status: HttpStatusCode.Unauthorized }
      ) as NextResponse<ErrorResponse>;

    // Check if the user is a tournament mod
    if (!(await playerIsTournamentMod(user.id, Number(params.tournamentId)))) {
      return NextResponse.json(
        { errors: ['You do not have permission to start the tournament'] },
        { status: HttpStatusCode.Forbidden }
      ) as NextResponse<ErrorResponse>;
    }

    // Start the tournament
    await db
      .update(tournamentsTable)
      .set({ startTime: new Date() })
      .where(eq(tournamentsTable.id, Number(params.tournamentId)));

    return NextResponse.json({
      message: 'Tournament has been started',
    }) as NextResponse<SuccessfulResponse>;
  } catch (e) {
    console.error(e);
    return NextResponse.json(
      { errors: ['Unable to start the tournament, please try again later'] },
      { status: HttpStatusCode.InternalServerError }
    ) as NextResponse<ErrorResponse>;
  }
};
