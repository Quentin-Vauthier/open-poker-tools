import { HttpStatusCode } from 'axios';
import { and, asc, eq, exists, ilike, notExists } from 'drizzle-orm';
import { NextRequest } from 'next/server';
import { z } from 'zod';

import { db } from '@/db';
import {
  playersTable,
  seriesPlayersTable,
  tournamentInvitationsTable,
  tournamentPlayersTable,
} from '@/db/schema';

export const dynamic = 'force-dynamic';
export const GET = async (req: NextRequest) => {
  try {
    const searchParams = req.nextUrl.searchParams;
    const query = searchParams.get('query') || '';
    const objectType = z
      .enum(['series', 'tournament', 'tournament-and-invited'])
      .nullable()
      .optional()
      .parse(searchParams.get('objectType'));
    const objectId = z.coerce
      .number()
      .nullable()
      .parse(searchParams.get('objectId'));
    const operator = searchParams.has('operator')
      ? z.enum(['in', 'notIn']).parse(searchParams.get('operator'))
      : 'in';
    const limit = z.coerce
      .number()
      .nullable()
      .optional()
      .parse(searchParams.get('limit'));

    //! Initial query
    let sql = db
      .select({
        id: playersTable.id,
        nickname: playersTable.nickname,
      })
      .from(playersTable);

    if (!objectType || !objectId)
      return Response.json(
        await sql
          .where(ilike(playersTable.nickname, `%${query}%`))
          .orderBy(asc(playersTable.nickname))
          .execute()
      );

    const whereClauses = query
      ? [ilike(playersTable.nickname, `%${query}%`)]
      : [];

    const fnExistsOrNot = operator === 'in' ? exists : notExists;
    switch (objectType) {
      case 'series':
        whereClauses.push(
          fnExistsOrNot(
            db
              .select()
              .from(seriesPlayersTable)
              .where(
                and(
                  eq(seriesPlayersTable.playerId, playersTable.id),
                  eq(seriesPlayersTable.seriesId, objectId)
                )
              )
          )
        );
        break;
      case 'tournament':
        whereClauses.push(
          fnExistsOrNot(
            db
              .select()
              .from(tournamentPlayersTable)
              .where(
                and(
                  eq(tournamentPlayersTable.playerId, playersTable.id),
                  eq(tournamentPlayersTable.tournamentId, objectId)
                )
              )
          )
        );
        break;
      case 'tournament-and-invited':
        whereClauses.push(
          fnExistsOrNot(
            db
              .select()
              .from(tournamentPlayersTable)
              .where(
                and(
                  eq(tournamentPlayersTable.playerId, playersTable.id),
                  eq(tournamentPlayersTable.tournamentId, objectId)
                )
              )
          ),
          fnExistsOrNot(
            db
              .select()
              .from(tournamentInvitationsTable)
              .where(
                and(
                  eq(tournamentInvitationsTable.playerId, playersTable.id),
                  eq(tournamentInvitationsTable.tournamentId, objectId)
                )
              )
          )
        );
        break;
      default:
        break;
    }

    //! Not removable conditions
    sql.where(and(...whereClauses)).orderBy(asc(playersTable.nickname));
    if (limit) sql.limit(limit);
    const locations = await sql.execute();
    return Response.json(locations);
  } catch (error) {
    // TODO: Logger
    console.error(error);
    return Response.json(
      { errors: ['An error occurred while fetching players'] },
      { status: HttpStatusCode.InternalServerError }
    );
  }
};
