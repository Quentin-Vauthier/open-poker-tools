export type SuccessfulResponse = {
  message: string;
};

export interface ErrorResponse {
  errors: string[];
}

export type DataResponse<T> = T;

export type BaseResponse<T> = (SuccessfulResponse & T) | ErrorResponse;
