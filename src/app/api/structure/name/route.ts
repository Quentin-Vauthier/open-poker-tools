import { count, eq } from 'drizzle-orm';
import { NextRequest } from 'next/server';

import { db } from '@/db';
import { structuresTable } from '@/db/schema';

export const dynamic = 'force-dynamic';
export const GET = async (req: NextRequest) => {
  try {
    const searchParams = req.nextUrl.searchParams;
    const name = searchParams.get('name');
    if (!name) {
      return new Response('Name is required', { status: 400 });
    }
    const [structureNameQuery] = await db
      .select({ count: count() })
      .from(structuresTable)
      .where(eq(structuresTable.name, name))
      .execute();
    return new Response(
      JSON.stringify({ exists: structureNameQuery.count > 0 })
    );
  } catch (err) {
    console.error(err);
    return new Response('Internal Server Error', { status: 500 });
  }
};
