import { HttpStatusCode } from 'axios';
import { asc, ilike } from 'drizzle-orm';
import { NextRequest } from 'next/server';
import { z } from 'zod';

import { db } from '@/db';
import { levelsTable, structuresTable } from '@/db/schema';
import { createStructureSchema } from '@/lib/zod';
import { Level } from '@/types';

export const POST = async (req: Request) => {
  try {
    const body = await req.json();
    const { name, levels } = await createStructureSchema.parseAsync(body);

    const [queryInsertStructure] = await db
      .insert(structuresTable)
      .values({
        name,
      })
      .returning({ id: structuresTable.id })
      .execute();

    await db
      .insert(levelsTable)
      .values(
        levels.map((level: Level, index: number) => ({
          structureId: queryInsertStructure.id,
          sb: level.isBreak ? 0 : level.sb,
          bb: level.isBreak ? 0 : level.bb,
          ante: level.isBreak ? 0 : level.ante,
          isBreak: level.isBreak,
          duration: level.duration,
          position: level.position ?? index,
        }))
      )
      .execute();
    return Response.json(
      {
        message: 'Structure created successfully',
        structureId: queryInsertStructure.id,
      },
      { status: 201 }
    );
  } catch (error) {
    if (error instanceof z.ZodError) {
      return Response.json(
        {
          errors: error.errors.map((err) => ({
            path: err.path,
            message: err.message,
          })),
        },
        { status: 400 }
      );
    }
    // TODO: Logger
    console.error(error);
    return Response.json(
      {
        errors: [
          'An error occurred while creating the structure, please try again later.',
        ],
      },
      { status: 500 }
    );
  }
};

export const GET = async (req: NextRequest) => {
  try {
    const searchParams = req.nextUrl.searchParams;
    const query = searchParams.get('query');
    const structures = await db
      .select({
        id: structuresTable.id,
        name: structuresTable.name,
      })
      .from(structuresTable)
      .where(ilike(structuresTable.name, `%${query}%`))
      .orderBy(asc(structuresTable.name));
    return Response.json(structures);
  } catch (error) {
    // TODO: Logger
    console.error(error);
    return Response.json(
      { errors: ['An error occurred while fetching locations'] },
      { status: HttpStatusCode.InternalServerError }
    );
  }
};
