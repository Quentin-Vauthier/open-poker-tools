import { HttpStatusCode } from 'axios';
import { asc, eq } from 'drizzle-orm';

import { db } from '@/db';
import { levelsTable, structuresTable } from '@/db/schema';
import { Structure } from '@/types';

type Params = Promise<{ structureId: string }>;

export const GET = async (_req: Request, segmentData: { params: Params }) => {
  try {
    const params = await segmentData.params;
    const structureId = params.structureId;
    const intStructureId = parseInt(structureId);
    if (isNaN(intStructureId)) {
      return Response.json(
        { errors: ['Invalid structure id'] },
        { status: HttpStatusCode.BadRequest }
      );
    }
    const structureWithDetails = await db
      .select({
        id: structuresTable.id,
        name: structuresTable.name,
        sb: levelsTable.sb,
        bb: levelsTable.bb,
        ante: levelsTable.ante,
        duration: levelsTable.duration,
        isBreak: levelsTable.isBreak,
      })
      .from(structuresTable)
      .innerJoin(levelsTable, eq(structuresTable.id, levelsTable.structureId))
      .where(eq(structuresTable.id, intStructureId))
      .orderBy(asc(structuresTable.name));
    const grouped = Object.groupBy(structureWithDetails, (s) => s.id);
    const structures: Structure[] = [];
    Object.entries(grouped).forEach(([_, val]) => {
      if (!val) return;
      structures.push({
        id: val[0].id,
        name: val[0].name,
        levels: val.map((s) => ({
          sb: s.sb,
          bb: s.bb,
          ante: s.ante,
          duration: s.duration,
          isBreak: s.isBreak,
        })),
      });
    });
    const structure = structures[0];
    if (!structure) {
      return Response.json(
        { errors: ['Structure not found'] },
        { status: HttpStatusCode.NotFound }
      );
    }
    return Response.json(structure);
  } catch (error) {
    // TODO: Logger
    console.error(error);
    return Response.json(
      { errors: ['An error occurred while fetching locations'] },
      { status: HttpStatusCode.InternalServerError }
    );
  }
};
