export type Event = {
  code: string;
  title: string;
  description: string;
};
export type GetEventsResponse = Event[];
