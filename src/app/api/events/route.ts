import { db } from '@/db';
import { eventsTable } from '@/db/schema';
import { getUser } from '@/lib/auth';
import { HttpStatusCode } from 'axios';

export const GET = async () => {
  try {
    const user = await getUser();
    if (!user) {
      return Response.json(
        {
          message: 'You must be logged in to view events.',
        },
        { status: HttpStatusCode.Unauthorized }
      );
    }
    const events = await db
      .select({
        code: eventsTable.code,
        title: eventsTable.title,
        description: eventsTable.description,
      })
      .from(eventsTable)
      .orderBy(eventsTable.code);
    return Response.json(events);
  } catch (error) {
    return Response.json(
      { errors: ['An error occurred while fetching events'] },
      { status: HttpStatusCode.InternalServerError }
    );
  }
};
