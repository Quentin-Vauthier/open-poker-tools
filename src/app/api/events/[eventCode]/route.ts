import { HttpStatusCode } from 'axios';
import { eq } from 'drizzle-orm';
import { z } from 'zod';

import { ErrorResponse, SuccessfulResponse } from '@/app/api/types';
import { db } from '@/db';
import { eventsTable } from '@/db/schema';
import { getUser } from '@/lib/auth';
import { formatZodError } from '@/lib/utils';
import { Role } from '@/types';
import { OPTEvent } from '@/lib/events/types';

type Params = Promise<{ eventCode: OPTEvent }>;

const updateEventSchema = z.object({
  title: z.string().optional(),
  description: z.string().optional(),
});

export const PUT = async (req: Request, segmentData: { params: Params }) => {
  try {
    const user = await getUser();
    const { eventCode } = await segmentData.params;

    // Check authentication
    if (!user) {
      return Response.json(
        {
          errors: ['You must be a logged in admin user to update events.'],
        } as ErrorResponse,
        { status: HttpStatusCode.Unauthorized }
      );
    } else if (user.role !== Role.Admin) {
      return Response.json(
        {
          errors: ['You must be an admin to update events.'],
        } as ErrorResponse,
        { status: HttpStatusCode.Unauthorized }
      );
    }

    // Check if the event code is valid
    if (!eventCode || !Object.values(OPTEvent).includes(eventCode))
      return Response.json({ errors: ['Event not found'] } as ErrorResponse, {
        status: HttpStatusCode.NotFound,
      });

    // Read the request body
    const json = await req.json().catch(() => null);
    const { success, data, error } = updateEventSchema.safeParse(json);

    if (!success) {
      return Response.json({ errors: formatZodError(error) } as ErrorResponse, {
        status: HttpStatusCode.BadRequest,
      });
    }

    const existing = await db
      .select()
      .from(eventsTable)
      .where(eq(eventsTable.code, eventCode));
    if (!existing.length) {
      return Response.json(
        {
          errors: ['Event not found'],
        } as ErrorResponse,
        { status: HttpStatusCode.NotFound }
      );
    }

    const updated = await db
      .update(eventsTable)
      .set(data)
      .where(eq(eventsTable.code, eventCode))
      .returning({ code: eventsTable.code });

    if (!updated.length) {
      return Response.json(
        {
          errors: ['An error occurred while updating the event'],
        } as ErrorResponse,
        { status: HttpStatusCode.InternalServerError }
      );
    }

    return Response.json({
      message: 'Event updated',
    } as SuccessfulResponse);
  } catch (error) {
    return Response.json(
      {
        errors: ['An error occurred while updating the event'],
      } as ErrorResponse,
      { status: HttpStatusCode.InternalServerError }
    );
  }
};
