import Link from 'next/link';

export default function Home() {
  return (
    <section className='w-full pt-12 md:pt-24 lg:pt-32'>
      <div className='container space-y-10 xl:space-y-16 px-4 md:px-6'>
        <div className='grid gap-4 md:grid-cols-2 md:gap-16'>
          <div>
            <h1 className='lg:leading-tighter text-3xl font-bold tracking-tighter sm:text-4xl md:text-5xl xl:text-[3.4rem] 2xl:text-[3.75rem]'>
              Open Source Poker Tournament Manager
            </h1>
            <p className='mx-auto max-w-[700px] text-gray-500 md:text-xl dark:text-gray-400'>
              Streamline your poker tournament management with our open-source
              solution. Track player standings, manage payouts, and more.
              Contribute to the project and help improve the experience for all.
            </p>
          </div>
          <div className='flex flex-col items-start space-y-4'>
            <div className='inline-block rounded-lg bg-gray-100 px-3 py-1 text-sm dark:bg-gray-800'>
              Key Features
            </div>
            <ul className='space-y-2 text-gray-500 dark:text-gray-400'>
              <li className='flex items-center gap-2'>
                <CheckIcon className='h-4 w-4 text-gray-900 dark:text-gray-50' />
                Tournament Management
              </li>
              <li className='flex items-center gap-2'>
                <CheckIcon className='h-4 w-4 text-gray-900 dark:text-gray-50' />
                Equity Calculator
              </li>
              <li className='flex items-center gap-2'>
                <CheckIcon className='h-4 w-4 text-gray-900 dark:text-gray-50' />
                Hand Tracking
              </li>
              <li className='flex items-center gap-2'>
                <CheckIcon className='h-4 w-4 text-gray-900 dark:text-gray-50' />
                Player Leaderboards
              </li>
            </ul>
            <div className='space-x-4'>
              <Link
                href='#'
                className='inline-flex h-9 items-center justify-center rounded-md bg-gray-900 px-4 py-2 text-sm font-medium text-gray-50 shadow transition-colors hover:bg-gray-900/90 focus-visible:outline-none focus-visible:ring-1 focus-visible:ring-gray-950 disabled:pointer-events-none disabled:opacity-50 dark:bg-gray-50 dark:text-gray-900 dark:hover:bg-gray-50/90 dark:focus-visible:ring-gray-300'>
                Get Started
              </Link>
              <Link
                href='https://gitlab.com/Quentin-Vauthier/open-poker-tools/-/blob/main/CONTRIBUTING.md?ref_type=heads'
                className='inline-flex h-9 items-center justify-center rounded-md border border-gray-200 bg-white px-4 py-2 text-sm font-medium shadow-sm transition-colors hover:bg-gray-100 hover:text-gray-900 focus-visible:outline-none focus-visible:ring-1 focus-visible:ring-gray-950 disabled:pointer-events-none disabled:opacity-50 dark:border-gray-800 dark:bg-gray-950 dark:hover:bg-gray-800 dark:hover:text-gray-50 dark:focus-visible:ring-gray-300'
                prefetch={false}>
                Contribute
              </Link>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

function CheckIcon(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      {...props}
      xmlns='http://www.w3.org/2000/svg'
      width='24'
      height='24'
      viewBox='0 0 24 24'
      fill='none'
      stroke='currentColor'
      strokeWidth='2'
      strokeLinecap='round'
      strokeLinejoin='round'>
      <path d='M20 6 9 17l-5-5' />
    </svg>
  );
}
