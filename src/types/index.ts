export interface PlayingLevel {
  sb: number;
  bb: number;
  ante: number;
  isBreak: false;
}

export interface BreakLevel {
  isBreak: true;
}

export type Level = (BreakLevel | PlayingLevel) & {
  position?: number;
  duration: number;
};

export type Structure = {
  id: number;
  name: string;
  levels: Array<Level>;
};

export type LocationFormValues = {
  name: string;
  address: string;
  city?: string;
  zip?: string;
};

export type Location = {
  id: number;
  name: string;
  address: string;
  city: string | null;
  zip: string | null;
};

export type SeriesFormValues = {
  name: string;
  locationId: number | null;
};

export type Series = {
  id: number;
  name: string;
  locationId: number | null;
};

export type ApiErrorResponse = {
  errors: {
    message: string;
    path: string;
  }[];
};

export type TournamentFormValues = {
  name: string;
  visibility: TournamentVisibility;
  date: Date;
  description?: string;
  buyIn: number;
  knockout?: boolean;
  progressiveKo?: boolean;
  knockoutBounty?: number;
  canReentry?: boolean;
  reentryEnd?: number;
  canLateReg?: boolean;
  lateRegEnd?: number;
  tableSize?: number;
  minTableSize?: number;
  maxPlayers?: number;
  variant?: string;
  startingStack?: number;
  finalTableSize?: number;
  seriesId: number | null;
  structureId: number | null;
  locationId: number | null;
};

export type Tournament = {
  id: number;
  name: string;
  visibility: TournamentVisibility;
  date: Date;
  description: string | null;
  waitlist: boolean;
  startTime: Date | null;
  endTime: Date | null;
  buyIn: number;
  knockout: boolean;
  progressiveKo: boolean;
  knockoutBounty: number;
  canReentry: boolean;
  reentryEnd: number | null;
  canLateReg: boolean;
  lateRegEnd: number | null;
  tableSize: number;
  minTableSize: number;
  maxPlayers: number;
  variant: string;
  startingStack: number;
  finalTableSize: number;
  seriesId: number | null;
  structureId: number | null;
};

export interface TournamentInfo extends Tournament {
  structure: string | null;
  series: string | null;
  location: string | null;
  locationId: number | null;
}

export interface TournamentPlayer {
  id: number;
  name: string;
  mod: boolean;
  checkedIn: boolean;
  position: number;
  prize: number;
  bounty: number;
  koCount: number;
}

export interface InvitedPlayer {
  id: number;
  name: string;
}

export interface SeriesPlayerForGrid {
  nickname: string;
  mod: boolean;
}

export enum Role {
  Admin = 'admin',
  Player = 'player',
}

export enum TournamentVisibility {
  Public = 'public',
  NotListed = 'not_listed',
  Private = 'private',
}
