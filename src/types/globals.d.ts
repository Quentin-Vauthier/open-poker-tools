interface OPTRequiredEnv {
  DATABASE_URL: string;
  GCS_BUCKET_NAME: string;
  GCS_PROJECT_ID: string;
  GCS_KEY_FILE: string;
  NEXT_PUBLIC_GCS_BUCKET_URL: string;
}

interface OPTOptionalEnv {
  DISCORD_TOKEN: string;
  RESEND_APIKEY: string;
}

interface OPTEnv extends OPTRequiredEnv, OPTOptionalEnv {}

namespace NodeJS {
  interface ProcessEnv extends OPTEnv {}
}
