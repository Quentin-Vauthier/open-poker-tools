import { notInArray } from 'drizzle-orm';

import { db } from '@/db';
import { eventsTable, InsertEvent } from '@/db/schema';
import { OPTEvent } from '@/lib/events/types';
import { colorError, colorWarn, ConsoleColor } from '@/lib/utils';

// Function to insert missing events into the events table
async function insertMissingEvents() {
  const eventCodes = Object.values(OPTEvent);
  const events: InsertEvent[] = eventCodes.map((eventCode) => ({
    code: eventCode,
    title: eventCode.replace(/_/g, ' ').toLowerCase(),
    description: `Description for ${eventCode}`,
  }));
  const inserted = await db
    .insert(eventsTable)
    .values(events)
    .onConflictDoNothing()
    .returning({ code: eventsTable.code });
  return inserted.map((event) => event.code);
}

async function deleteOldEvents() {
  const eventCodes = Object.values(OPTEvent);
  const deleted = await db
    .delete(eventsTable)
    .where(notInArray(eventsTable.code, eventCodes))
    .returning({ code: eventsTable.code });
  return deleted.map((event) => event.code);
}

const requiredEnv = [
  'DATABASE_URL',
  'GCS_BUCKET_NAME',
  'GCS_PROJECT_ID',
  'GCS_KEY_FILE',
  'NEXT_PUBLIC_GCS_BUCKET_URL',
];
const optionalEnv = ['DISCORD_TOKEN', 'RESEND_APIKEY'];
async function checkEnvVars() {
  const missingVars = requiredEnv.filter((envVar) => !process.env[envVar]);
  if (missingVars.length > 0) {
    colorError(
      ConsoleColor.FgRed,
      `Missing required environment variables: ${missingVars}, THE APP WILL NOT START`
    );
  }
  const warnVars = optionalEnv.filter((envVar) => !process.env[envVar]);
  if (warnVars.length > 0) {
    colorWarn(
      ConsoleColor.FgYellow,
      `Missing optional environment variables: ${warnVars}, some features may not work`
    );
  }
  if (missingVars.length > 0) process.exit(1);
}

// Run the function
insertMissingEvents()
  .then((insertedCodes) => {
    if (insertedCodes.length === 0) console.log('No events to insert');
    else
      console.log(`Events inserted successfully (${insertedCodes.join(', ')})`);
  })
  .catch((error) => {
    console.error('Error during events insertion:', error);
  });

deleteOldEvents()
  .then((deletedCodes) => {
    if (deletedCodes.length === 0) console.log('No events to delete');
    else
      console.log(
        `Old events deleted successfully (${deletedCodes.join(', ')})`
      );
  })
  .catch((error) => {
    console.error('Error during events deletion:', error);
  });

checkEnvVars();
