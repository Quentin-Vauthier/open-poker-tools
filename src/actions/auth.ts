'use server';

import { count, eq, or } from 'drizzle-orm';
import { cookies } from 'next/headers';
import { redirect } from 'next/navigation';
import { z } from 'zod';

import { lucia } from '@/auth';
import { db } from '@/db';
import { playersTable } from '@/db/schema';
import { validateRequest } from '@/lib/auth';
import EventManager from '@/lib/events';
import { OPTEvent } from '@/lib/events/types';
import { comparePasswords, hashPassword } from '@/lib/passwords';

const createUserSchema = z
  .object({
    email: z
      .string({
        message: 'Email is required',
      })
      .email({
        message: 'Invalid email address',
      })
      .refine(
        async (email) => {
          const [query] = await db
            .select({ count: count() })
            .from(playersTable)
            .where(eq(playersTable.email, email));
          return query.count === 0;
        },
        {
          message: 'A user with this email already exists', //! Possible user enumeration vulnerability, to be checked
        }
      ),
    nickname: z
      .string({
        message: 'Nickname is required',
      })
      .min(3, {
        message: 'Nickname must be at least 3 characters long',
      })
      .max(32, {
        message: 'Nickname must be at most 32 characters long',
      })
      .regex(/^[a-zA-Z0-9_-]+$/, {
        message:
          'Nickname can only contain letters, numbers, underscores, and hyphens',
      })
      .refine(
        async (nickname) => {
          const [query] = await db
            .select({ count: count() })
            .from(playersTable)
            .where(eq(playersTable.nickname, nickname));
          return query.count === 0;
        },
        {
          message: 'A user with this nickname already exists',
        }
      ),
    password: z.string().min(8),
  })
  .strict();

type CreateUserResult = {
  success: boolean;
  errors?: { message: string; path: string }[];
};

export async function signUpUser(
  rawEmail: string,
  rawNickname: string,
  rawPassword: string
): Promise<CreateUserResult> {
  try {
    const { email, nickname, password } = await createUserSchema.parseAsync({
      email: rawEmail,
      nickname: rawNickname,
      password: rawPassword,
    });

    const hashedPassword = await hashPassword(password);
    // TODO: Need to run the queries in a transaction
    const [createdUser] = await db
      .insert(playersTable)
      .values({
        email,
        nickname,
        password: hashedPassword,
      })
      .returning({ id: playersTable.id, role: playersTable.role });

    const session = await lucia.createSession(createdUser.id, {
      role: createdUser.role,
      email,
      nickname,
    });

    const sessionCookie = lucia.createSessionCookie(session.id);

    const cookieStore = await cookies();
    cookieStore.set(
      sessionCookie.name,
      sessionCookie.value,
      sessionCookie.attributes
    );

    EventManager.trigger(OPTEvent.GLOBAL_USER_CREATED, {
      objectId: createdUser.id,
      email,
      nickname,
    });
    return { success: true };
  } catch (error) {
    if (error instanceof z.ZodError) {
      return {
        success: false,
        errors: error.errors.map((err) => ({
          message: err.message,
          path: err.path[0]?.toString(),
        })),
      };
    }
    // TODO: Logger
    console.error('Sign up error', error);
    return {
      success: false,
      errors: [
        {
          message: 'An unknown error occurred',
          path: 'root',
        },
      ],
    };
  }
}

const signInUserSchema = z
  .object({
    email: z // Email or username
      .string({
        message: 'Email is required',
      }),
    password: z.string().min(8),
  })
  .strict();
type SignInResult = {
  success: boolean;
  errors?: { message: string; path: string }[];
};
export async function signIn(credentials: {
  email: string;
  password: string;
}): Promise<SignInResult> {
  try {
    const { email, password } = await signInUserSchema.parseAsync(credentials);

    const [user] = await db
      .select({
        id: playersTable.id,
        email: playersTable.email,
        role: playersTable.role,
        nickname: playersTable.nickname,
        playerId: playersTable.id,
        password: playersTable.password,
      })
      .from(playersTable)
      .where(
        or(eq(playersTable.email, email), eq(playersTable.nickname, email))
      );
    if (!user) {
      return {
        success: false,
        errors: [{ message: 'Invalid email or password', path: 'root' }],
      };
    }

    const passwordMatch = await comparePasswords(password, user.password);

    if (!passwordMatch) {
      return {
        success: false,
        errors: [{ message: 'Invalid email or password', path: 'root' }],
      };
    }

    const session = await lucia.createSession(user.id, {
      email: user.email,
      role: user.role,
      nickname: user.nickname,
    });

    const sessionCookie = lucia.createSessionCookie(session.id);

    const cookieStore = await cookies();
    cookieStore.set(
      sessionCookie.name,
      sessionCookie.value,
      sessionCookie.attributes
    );
    return { success: true };
  } catch (error) {
    if (error instanceof z.ZodError) {
      return {
        success: false,
        errors: error.errors.map((err) => ({
          message: err.message,
          path: err.path[0]?.toString(),
        })),
      };
    }
    // TODO: Logger
    console.error('Sign in error', error);
    return {
      success: false,
      errors: [
        {
          message: 'An unknown error occurred',
          path: 'root',
        },
      ],
    };
  }
}

export async function signOut() {
  const { session } = await validateRequest();
  if (!session) {
    return {
      error: 'Unauthorized',
    };
  }

  await lucia.invalidateSession(session.id);

  const sessionCookie = lucia.createBlankSessionCookie();
  const cookieStore = await cookies();
  cookieStore.set(
    sessionCookie.name,
    sessionCookie.value,
    sessionCookie.attributes
  );
  return redirect('/');
}
