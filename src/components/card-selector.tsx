import * as React from 'react';
import { Check } from 'lucide-react';

import { cn } from '@/lib/utils';
import { Button } from '@/components/ui/button';
import {
  Command,
  CommandEmpty,
  CommandGroup,
  CommandInput,
  CommandItem,
} from '@/components/ui/command';
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from '@/components/ui/popover';
import { Card, getFullDeck } from '@/lib/poker-utils';
import Image from 'next/image';
import { useState } from 'react';

const cards = getFullDeck();

interface CardSelectorProps {
  value: Card;
  onChange: (value: Card) => void;
  cardImages?: boolean;
}

export default function CardSelector({
  value,
  onChange,
  cardImages = false,
}: CardSelectorProps) {
  const [open, setOpen] = useState<boolean>(false);

  const card = cardImages ? (
    <Image
      src={`/assets/cards/hand-drawn/${value.toShortString()}.png`}
      alt={value.toString()}
      width={60}
      height={80}
    />
  ) : (
    value.toShortString()
  );

  return (
    <Popover open={open} onOpenChange={setOpen}>
      <PopoverTrigger asChild>
        <Button
          variant='outline'
          role='combobox'
          aria-expanded={open}
          className='w-[60px] h-[80px] justify-center items-center text-xl font-bold p-0'>
          {value.isUnknown() ? '?' : card}
        </Button>
      </PopoverTrigger>
      <PopoverContent className='w-[200px] p-0'>
        <Command>
          <CommandInput placeholder='Search card...' />
          <CommandEmpty>No card found.</CommandEmpty>
          <CommandGroup>
            {cards.map((card) => (
              <CommandItem
                key={card.toShortString()}
                onSelect={() => {
                  onChange(card.equals(value) ? Card.unknown : card);
                  setOpen(false);
                }}>
                <Check
                  className={cn(
                    'mr-2 h-4 w-4',
                    card.equals(value) ? 'opacity-100' : 'opacity-0'
                  )}
                />
                {card.toShortString()}
              </CommandItem>
            ))}
          </CommandGroup>
        </Command>
      </PopoverContent>
    </Popover>
  );
}
