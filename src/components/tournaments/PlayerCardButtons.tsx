'use client';

import axios, { HttpStatusCode } from 'axios';
import {
  DoorOpen,
  UserCheck,
  X,
  Crosshair,
  ArrowLeftCircle,
} from 'lucide-react';
import { useState } from 'react';
import { toast } from 'sonner';

import { Button } from '@/components/ui/button';
import {
  Tooltip,
  TooltipContent,
  TooltipProvider,
  TooltipTrigger,
} from '@/components/ui/tooltip';
import { TournamentPlayer } from '@/types';

import {
  Dialog,
  DialogClose,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from '@components/ui/dialog';
import { Icons } from '../Icons';

interface PlayerCardButtonsProps {
  player: TournamentPlayer;
  tournamentId: number;
  isMod: boolean;
  canUnregister: boolean;
  isTournamentStarted: boolean;
  refetch: () => void;
}

export default function PlayerCardButtons({
  player,
  tournamentId,
  isMod,
  canUnregister,
  isTournamentStarted,
  refetch,
}: PlayerCardButtonsProps) {
  const [checkedIn, setCheckedIn] = useState<boolean>(player.checkedIn);
  const [checkingIn, setCheckingIn] = useState<boolean>(false);
  const [leaving, setLeaving] = useState<boolean>(false);
  const [unRegistering, setUnRegistering] = useState<boolean>(false);
  const [eliminating, setEliminating] = useState<boolean>(false);
  const [reinstating, setReinstating] = useState<boolean>(false);

  return (
    <TooltipProvider>
      <div className='flex gap-2'>
        {isMod && (
          <TooltipProvider>
            <Tooltip>
              <TooltipTrigger asChild>
                <Button
                  variant='default'
                  disabled={checkedIn || checkingIn || leaving}
                  onClick={() => checkIn(player.id)}>
                  {checkingIn ? (
                    <Icons.spinner size={16} />
                  ) : (
                    <UserCheck size={16} />
                  )}
                </Button>
              </TooltipTrigger>
              <TooltipContent>
                <p>Check-in</p>
              </TooltipContent>
            </Tooltip>
            {!player.position && (
              <Tooltip>
                <TooltipTrigger asChild>
                  <Button
                    variant='outline'
                    disabled={!checkedIn || leaving || checkingIn}
                    onClick={() => leave(player.id)}>
                    {leaving ? (
                      <Icons.spinner size={16} />
                    ) : (
                      <DoorOpen size={16} />
                    )}
                  </Button>
                </TooltipTrigger>
                <TooltipContent>
                  <p>Leave</p>
                </TooltipContent>
              </Tooltip>
            )}
            {isTournamentStarted && checkedIn && !player.position && (
              <Tooltip>
                <TooltipTrigger asChild>
                  <Button
                    variant='destructive'
                    onClick={() => eliminatePlayer(player.id)}
                    aria-label={`Eliminate ${player.name}`}>
                    {eliminating ? (
                      <Icons.spinner size={16} />
                    ) : (
                      <Crosshair size={16} />
                    )}
                  </Button>
                </TooltipTrigger>
                <TooltipContent>
                  <p>Eliminate</p>
                </TooltipContent>
              </Tooltip>
            )}
            {isTournamentStarted && !!player.position && (
              <Tooltip>
                <TooltipTrigger asChild>
                  <Button
                    variant='default'
                    onClick={() => reinstatePlayer(player.id)}
                    aria-label={`Reinstate ${player.name}`}>
                    {reinstating ? (
                      <Icons.spinner size={16} />
                    ) : (
                      <ArrowLeftCircle size={16} />
                    )}
                  </Button>
                </TooltipTrigger>
                <TooltipContent>
                  <p>Reinstate</p>
                </TooltipContent>
              </Tooltip>
            )}
          </TooltipProvider>
        )}
        {canUnregister && (
          <Dialog>
            <DialogTrigger asChild>
              <Button variant='destructive' type='button'>
                {unRegistering ? <Icons.spinner size={16} /> : <X size={16} />}
              </Button>
            </DialogTrigger>
            <DialogContent>
              <DialogHeader>
                <DialogTitle>Are you sure?</DialogTitle>
                <DialogDescription>
                  If you unregister, you will lose your spot in the tournament.
                  And you might need an invite to join again.
                </DialogDescription>
              </DialogHeader>
              <DialogFooter>
                <DialogClose asChild>
                  <Button type='button' variant='secondary'>
                    Close
                  </Button>
                </DialogClose>
                <DialogClose asChild>
                  <Button
                    variant='default'
                    onClick={() => unRegister(player.id)}>
                    Unregister
                  </Button>
                </DialogClose>
              </DialogFooter>
            </DialogContent>
          </Dialog>
        )}
      </div>
    </TooltipProvider>
  );

  async function checkIn(playerId: number) {
    setCheckingIn(true);
    try {
      const { status } = await axios.post(
        `/api/tournament/${tournamentId}/checkin`,
        { playerId }
      );
      if (status === HttpStatusCode.Created) {
        setCheckedIn(true);
      }
    } catch (e) {
      console.error(e);
    } finally {
      setCheckingIn(false);
    }
  }

  async function leave(playerId: number) {
    setLeaving(true);
    try {
      const { status } = await axios.post(
        `/api/tournament/${tournamentId}/leave`,
        { playerId }
      );
      if (status === HttpStatusCode.Ok) {
        setCheckedIn(false);
      }
    } catch (e) {
      console.error(e);
      toast.error('Failed to leave tournament');
    } finally {
      setLeaving(false);
    }
  }

  async function unRegister(playerId: number) {
    setUnRegistering(true);
    try {
      const { status } = await axios.post(
        `/api/tournament/${tournamentId}/unregister`,
        { playerId }
      );
      if (status === HttpStatusCode.Ok) {
        toast.success('Player unregistered');
        refetch();
      }
    } catch (e) {
      console.error(e);
      toast.error('Failed to unregister, please try again later');
    } finally {
      setUnRegistering(false);
    }
  }

  async function eliminatePlayer(playerId: number) {
    setEliminating(true);
    try {
      const { status } = await axios.post(
        `/api/tournament/${tournamentId}/eliminate`,
        { playerId }
      );
      if (status !== HttpStatusCode.Ok) {
        toast.error('Could not eliminate the player, please try again later');
      } else {
        toast.success('The player has been eliminated');
        refetch();
      }
    } catch (e) {
      console.error(e);
      if (axios.isAxiosError(e)) {
        if (e.response?.status === HttpStatusCode.Forbidden) {
          toast.error('You do not have permission to eliminate the player');
        } else {
          toast.error('An unexpected error occurred, please try again later');
        }
        return;
      }
      toast.error('An unexpected error occurred, please try again later');
    } finally {
      setEliminating(false);
    }
  }

  async function reinstatePlayer(playerId: number) {
    setReinstating(true);
    try {
      const { status } = await axios.post(
        `/api/tournament/${tournamentId}/reinstate`,
        { playerId }
      );
      if (status !== HttpStatusCode.Ok) {
        toast.error('Could not reinstate the player, please try again later');
      } else {
        toast.success('The player has been reinstated');
        refetch();
      }
    } catch (e) {
      console.error(e);
      if (axios.isAxiosError(e)) {
        if (e.response?.status === HttpStatusCode.Forbidden) {
          toast.error('You do not have permission to reinstate the player');
        } else {
          toast.error('An unexpected error occurred, please try again later');
        }
        return;
      }
      toast.error('An unexpected error occurred, please try again later');
    } finally {
      setReinstating(false);
    }
  }
}
