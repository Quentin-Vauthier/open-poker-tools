'use client';

import axios from 'axios';
import { Check, X } from 'lucide-react';
import { useRouter } from 'next/navigation';
import { useState } from 'react';
import { toast } from 'sonner';

import { Button } from '@/components/ui/button';
import {
  Tooltip,
  TooltipContent,
  TooltipProvider,
  TooltipTrigger,
} from '@components/ui/tooltip';

import { Icons } from '../Icons';

interface InvitedPlayerCardButtonsProps {
  playerId: number;
  tournamentId: number;
  refetch: () => void;
}

export default function InvitedPlayerCardButtons({
  playerId,
  tournamentId,
  refetch,
}: InvitedPlayerCardButtonsProps) {
  const [registering, setRegistering] = useState<boolean>(false);
  const [declining, setDeclining] = useState<boolean>(false);
  const router = useRouter();

  return (
    <TooltipProvider>
      <div className='flex gap-2'>
        <Tooltip>
          <TooltipTrigger asChild>
            <Button
              variant='destructive'
              disabled={declining || registering}
              onClick={decline}>
              {declining ? (
                <Icons.spinner aria-label='Declining invite' />
              ) : (
                <X aria-label='Decline' />
              )}
            </Button>
          </TooltipTrigger>
          <TooltipContent>
            <p>Decline invite</p>
          </TooltipContent>
        </Tooltip>
        <Tooltip>
          <TooltipTrigger asChild>
            <Button
              variant='default'
              disabled={registering || declining}
              onClick={() => {
                register().then(refetch);
              }}>
              {registering ? (
                <Icons.spinner aria-label='Accepting invite' />
              ) : (
                <Check aria-label='Accept' />
              )}
            </Button>
          </TooltipTrigger>
          <TooltipContent>
            <p>Accept invite</p>
          </TooltipContent>
        </Tooltip>
      </div>
    </TooltipProvider>
  );

  async function register() {
    setRegistering(true);
    try {
      await axios.post(`/api/tournament/${tournamentId}/register`);
      toast.success('Invitation accepted successfully');
    } catch (e) {
      console.error(e);
      toast.error('Could not check-in, please try again later');
    } finally {
      setRegistering(false);
    }
  }

  async function decline() {
    setDeclining(true);
    try {
      await axios.post(`/api/tournament/${tournamentId}/invite/decline`);
      toast.success('Invitation declined successfully');
      router.replace('/tournaments');
    } catch (e) {
      console.error(e);
      toast.error('Could not decline invite, please try again later');
    } finally {
      setDeclining(false);
    }
  }
}
