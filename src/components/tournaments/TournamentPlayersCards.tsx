'use client';

import axios from 'axios';
import { ShieldCheck } from 'lucide-react';

import { Card, CardContent, CardHeader, CardTitle } from '@/components/ui/card';
import { cn } from '@/lib/utils';
import {
  InvitedPlayer,
  TournamentInfo,
  TournamentPlayer,
  TournamentVisibility,
} from '@/types';
import { useSession } from '@components/SessionContext';
import PlayerCardButtons from '@components/tournaments/PlayerCardButtons';
import { Skeleton } from '@components/ui/skeleton';
import { useQuery } from '@tanstack/react-query';

import InvitedPlayerCardButtons from './InvitedPlayerCardButtons';

interface TournamentPlayersCardsProps {
  tournament: TournamentInfo;
  players: TournamentPlayer[];
  refetch: () => void; // Only refetch the players
  isTournamentEnded: boolean; // Add this prop
}

export default function TournamentPlayersCards({
  tournament,
  players,
  refetch,
  isTournamentEnded, // Add this prop
}: TournamentPlayersCardsProps) {
  const {
    data: invitedPlayers,
    isPending: isInvitedPlayersPending,
    isError: isInvitedPlayersError,
    refetch: refetchInvitedPlayers,
  } = useQuery({
    queryKey: ['tournament', tournament.id, 'invited'],
    queryFn: () => {
      if (!tournament?.id) return null;
      return fetchInvitedPlayers();
    },
    refetchInterval: 30_000, // Update every 30 seconds
    enabled: tournament.visibility === TournamentVisibility.Private,
  });

  const { user } = useSession();

  const isMod = players?.find((p) => p.id === user?.id)?.mod ?? false;

  const isTournamentStarted = Boolean(tournament.startTime);

  return (
    <>
      <Card>
        <CardHeader>
          <CardTitle>Registered players</CardTitle>
        </CardHeader>
        <CardContent>
          <div
            className={cn('grid gap-2', {
              'grid-cols:2 md:grid-cols-3 lg:grid-cols-4': !isTournamentStarted,
              'grid-cols:1 md:grid-cols-2 lg:grid-cols-3': isTournamentStarted,
            })}>
            {players?.map((player, i) => (
              <Card
                key={player.id}
                className={cn({
                  // Highlight the player card if the player is in the waitlist or eliminated
                  'bg-secondary':
                    tournament.maxPlayers > 0 &&
                    i >= tournament.maxPlayers &&
                    !player.checkedIn,
                  'opacity-70 bg-secondary': player.position,
                })}>
                <CardHeader>
                  <CardTitle className='flex flex-row items-center gap-2'>
                    <span
                      className={cn('decoration-destructive', {
                        'line-through': !!player.position,
                      })}>
                      {player.name}
                    </span>
                    {player.mod && (
                      <ShieldCheck
                        aria-label='Moderator'
                        size={16}
                        color='green'
                      />
                    )}
                    {tournament.maxPlayers > 0 &&
                      i >= tournament.maxPlayers && (
                        <sup className='text-sm text-secondary-foreground'>
                          waitlist ({i - tournament.maxPlayers + 1}/
                          {players.length - tournament.maxPlayers})
                        </sup>
                      )}
                  </CardTitle>
                </CardHeader>
                {!isTournamentEnded && ( // Add this condition
                  <CardContent className='flex gap-2 justify-end'>
                    <PlayerCardButtons
                      tournamentId={tournament.id}
                      player={player}
                      isMod={isMod}
                      isTournamentStarted={isTournamentStarted}
                      // Can unregister if the player is not the last mod
                      // And the current user is moderator or the player is the current user
                      // Moderators can't unregister other moderators
                      canUnregister={
                        !isLastMod(player) &&
                        (player.id === user?.id || (isMod && !player.mod))
                      }
                      refetch={refetch}
                    />
                  </CardContent>
                )}
              </Card>
            ))}
          </div>
        </CardContent>
      </Card>
      {tournament.visibility === TournamentVisibility.Private && (
        <Card className='mt-4'>
          <CardHeader>
            <CardTitle>Invited Players</CardTitle>
          </CardHeader>
          <CardContent>
            {isInvitedPlayersPending && <LoadingPlayers count={2} />}
            {isInvitedPlayersError && (
              <div className='text-lg flex items-center justify-center col-span-2 md:col-span-3 lg:col-span-4'>
                Error loading invited players
              </div>
            )}
            <div className='grid gap-2 grid-cols:2 md:grid-cols-3 lg:grid-cols-4'>
              {invitedPlayers?.length === 0 && (
                <div className='text-lg flex items-center justify-center col-span-2 md:col-span-3 lg:col-span-4'>
                  No players invited
                </div>
              )}
              {invitedPlayers &&
                invitedPlayers.map((player, i) => (
                  <Card
                    key={player.id}
                    className={cn({
                      'bg-secondary':
                        tournament.maxPlayers > 0 && i >= tournament.maxPlayers,
                    })}>
                    <CardHeader>
                      <CardTitle className='flex flex-row items-center gap-2'>
                        {player.name}
                      </CardTitle>
                    </CardHeader>
                    {player.id === user?.id && (
                      <CardContent className='flex gap-2 justify-end'>
                        <InvitedPlayerCardButtons
                          tournamentId={tournament.id}
                          playerId={player.id}
                          refetch={() => {
                            refetch();
                            refetchInvitedPlayers();
                          }}
                        />
                      </CardContent>
                    )}
                  </Card>
                ))}
            </div>
          </CardContent>
        </Card>
      )}
    </>
  );

  function isLastMod(player: TournamentPlayer) {
    if (!players) return false;
    return players.filter((p) => p.mod).length === 1 && player.mod;
  }

  async function fetchInvitedPlayers(): Promise<InvitedPlayer[] | null> {
    try {
      const { data } = await axios.get(
        `/api/tournament/${tournament.id}/players?invited`
      );
      return data;
    } catch (e) {
      if (axios.isAxiosError(e) && e.response?.status === 404) return [];
      console.error(e);
      return null;
    }
  }

  function LoadingPlayers({ count = 5 }) {
    return (
      <div
        className={cn('grid gap-2', {
          'grid-cols:2 md:grid-cols-3 lg:grid-cols-4': !isTournamentStarted,
          'grid-cols:1 md:grid-cols-2 lg:grid-cols-3': isTournamentStarted,
        })}>
        {[...Array(count)].map((_, i) => (
          <div
            key={i}
            className='border rounded-lg p-4 flex flex-col items-start space-y-4'>
            <div className='flex justify-between w-full h-16'>
              <Skeleton className='h-6 w-24' /> {/* Player name */}
              <Skeleton className='h-4 w-20' /> {/* Waitlist status */}
            </div>
          </div>
        ))}
      </div>
    );
  }
}
