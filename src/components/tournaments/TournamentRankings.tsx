import { Card, CardContent, CardHeader, CardTitle } from '@/components/ui/card';
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from '@/components/ui/table';
import { TournamentPlayer } from '@/types';

interface TournamentRankingsProps {
  players: TournamentPlayer[];
}

export default function TournamentRankings({
  players,
}: TournamentRankingsProps) {
  if (!players?.length) return null;

  const rankings = players
    .filter((player) => player.position !== null)
    .sort((a, b) => (a.position || 0) - (b.position || 0))
    .map((player) => ({
      playerId: player.id,
      playerName: player.name,
      position: player.position || 0,
    }));

  const activePlayers = players
    .filter((player) => player.checkedIn && player.position === null)
    .map((_, index) => ({
      playerId: `active-${index}`,
      position:
        Math.min(...rankings.map((r) => r.position), players.length) - index,
    }));

  return (
    <Card>
      <CardHeader>
        <CardTitle>Rankings</CardTitle>
      </CardHeader>
      <CardContent>
        <Table>
          <TableHeader>
            <TableRow>
              <TableHead>Position</TableHead>
              <TableHead>Player</TableHead>
            </TableRow>
          </TableHeader>
          <TableBody>
            {rankings.map((ranking) => (
              <TableRow key={ranking.playerId}>
                <TableCell>{ranking.position}</TableCell>
                <TableCell>{ranking.playerName}</TableCell>
              </TableRow>
            ))}
            {activePlayers.map((player) => (
              <TableRow
                key={player.playerId}
                className='text-muted-foreground italic'>
                <TableCell>{player.position}</TableCell>
                <TableCell>Player {player.position}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </CardContent>
    </Card>
  );
}
