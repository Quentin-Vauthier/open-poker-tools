'use client';

import axios, { AxiosError, HttpStatusCode } from 'axios';
import { Play, StopCircle } from 'lucide-react';
import { useState } from 'react';
import { toast } from 'sonner';

import { Icons } from '@/components/Icons';
import { useSession } from '@/components/SessionContext';
import { Button } from '@/components/ui/button';
import { cn } from '@/lib/utils';
import {
  TournamentInfo,
  TournamentPlayer,
  TournamentVisibility,
} from '@/types';
import InvitePlayerButton from '@components/tournaments/InvitePlayerButton';
import TournamentPlayersCards from '@components/tournaments/TournamentPlayersCards';
import TournamentRankings from '@components/tournaments/TournamentRankings';
import { Card, CardContent, CardHeader, CardTitle } from '@components/ui/card';
import {
  Dialog,
  DialogClose,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
} from '@components/ui/dialog';
import { Skeleton } from '@components/ui/skeleton';
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from '@components/ui/table';
import { useQuery } from '@tanstack/react-query';

interface TournamentPlayersInfoProps {
  tournament: TournamentInfo;
}

export default function TournamentPlayersInfo({
  tournament,
}: TournamentPlayersInfoProps) {
  const {
    data: players,
    isPending,
    isError,
    refetch,
  } = useQuery<TournamentPlayer[]>({
    queryKey: ['tournament', tournament.id, 'players'],
    queryFn: () => fetchTournamentPlayers(tournament.id),
    refetchInterval: 30_000, // Update every 30 seconds
  });

  const { user } = useSession();
  const isMod = players?.find((p) => p.id === user?.id)?.mod ?? false;
  const isTournamentStarted = Boolean(tournament.startTime);
  const isTournamentEnded = Boolean(tournament.endTime);
  const currentUserIsRegistered = players?.some((p) => p.id === user?.id);
  const tournamentIsPrivate =
    tournament.visibility === TournamentVisibility.Private;

  if (isError) return <div>Error loading players</div>;

  return (
    <section>
      <div className='flex gap-2'>
        {isPending ? (
          <LoadingButtons />
        ) : (
          <>
            {isMod && <ModButtons />}
            {!currentUserIsRegistered && !tournamentIsPrivate && (
              <TournamentRegisterButton
                tournament={tournament}
                players={players}
                refetch={refetch}
              />
            )}
          </>
        )}
      </div>
      <div
        className={cn('mt-4', {
          'grid gap-6 grid-cols-1 lg:grid-cols-[2fr,1fr]':
            !!tournament.startTime,
          'space-y-6': !tournament.startTime,
        })}>
        {isPending ? (
          <LoadingPlayers isTournamentStarted={isTournamentStarted} />
        ) : players.length ? (
          <TournamentPlayersCards
            tournament={tournament}
            players={players || []}
            refetch={refetch}
            isTournamentEnded={isTournamentEnded}
          />
        ) : (
          <div className='text-lg flex items-center justify-center col-span-2 md:col-span-3 lg:col-span-4'>
            No players registered
          </div>
        )}

        {!!tournament.startTime && (
          <>
            {isPending ? (
              <LoadingRankings />
            ) : (
              <TournamentRankings players={players!} />
            )}
          </>
        )}
      </div>
    </section>
  );

  function ModButtons() {
    return (
      <>
        {!isTournamentStarted && (
          <InvitePlayerButton tournamentId={tournament.id} />
        )}
        {!isTournamentStarted && (
          <StartTournamentButton
            tournamentId={tournament.id}
            refetch={refetch}
          />
        )}
        {isTournamentStarted && !isTournamentEnded && (
          <EndTournamentButton
            tournamentId={tournament.id}
            players={players}
            refetch={refetch}
          />
        )}
      </>
    );
  }
}

async function fetchTournamentPlayers(
  tournamentId: number
): Promise<TournamentPlayer[]> {
  try {
    const { data } = await axios.get(`/api/tournament/${tournamentId}/players`);
    return data;
  } catch (e) {
    if (axios.isAxiosError(e) && e.response?.status === 404) return [];
    console.error(e);
    throw e;
  }
}

function EndTournamentButton({
  tournamentId,
  players,
  refetch,
}: {
  tournamentId: number;
  players: TournamentPlayer[] | undefined;
  refetch: () => void;
}) {
  const [ending, setEnding] = useState<boolean>(false);
  const [showDialog, setShowDialog] = useState<boolean>(false);

  const notEliminatedCount =
    players?.filter((player) => player.checkedIn && !player.position).length ??
    0;

  return (
    <>
      <Button variant='destructive' onClick={handleEndTournament}>
        {ending ? <Icons.spinner /> : <StopCircle size={16} />}&nbsp;End
        tournament
      </Button>
      {showDialog && (
        <Dialog open={showDialog} onOpenChange={setShowDialog}>
          <DialogContent>
            <DialogHeader>
              <DialogTitle>Are you sure?</DialogTitle>
              <DialogDescription>
                There are still {notEliminatedCount} players checked in but not
                eliminated. Are you sure you want to end the tournament?
                <br />
                This will close the tournament and update the rankings. The
                players that are not marked as eliminated will be out of the
                tournament.
                <br />
                The current winner is:{' '}
                {players?.reduce((prev, current) =>
                  (prev.position ?? Infinity) < (current.position ?? Infinity)
                    ? prev
                    : current
                )?.name ?? 'Unknown as there is no eliminated player'}
              </DialogDescription>
            </DialogHeader>
            <DialogFooter>
              <DialogClose asChild>
                <Button
                  type='button'
                  variant='secondary'
                  onClick={() => setShowDialog(false)}>
                  Cancel
                </Button>
              </DialogClose>
              <Button variant='destructive' onClick={endTournament}>
                {ending ? <Icons.spinner /> : <StopCircle size={16} />}&nbsp;End
                tournament
              </Button>
            </DialogFooter>
          </DialogContent>
        </Dialog>
      )}
    </>
  );

  function handleEndTournament() {
    if (notEliminatedCount > 0) {
      setShowDialog(true);
    } else {
      endTournament();
    }
  }

  async function endTournament() {
    setEnding(true);
    try {
      const { status } = await axios.post(
        `/api/tournament/${tournamentId}/end`
      );
      if (status !== HttpStatusCode.Ok) {
        toast.error('Could not end the tournament, please try again later');
      } else {
        toast.success('The tournament has ended');
        refetch();
      }
    } catch (e) {
      console.error(e);
      if (axios.isAxiosError(e)) {
        if (e.response?.status === HttpStatusCode.Forbidden) {
          toast.error('You do not have permission to end the tournament');
        } else {
          toast.error('An unexpected error occurred, please try again later');
        }
        return;
      }
      toast.error('An unexpected error occurred, please try again later');
    } finally {
      setEnding(false);
      setShowDialog(false);
    }
  }
}

interface TournamentRegisterButtonProps {
  tournament: TournamentInfo;
  players: TournamentPlayer[];
  refetch: () => void;
}
function TournamentRegisterButton({
  tournament,
  players,
  refetch,
}: TournamentRegisterButtonProps) {
  const [registering, setRegistering] = useState<boolean>(false);
  const canRegister =
    tournament.waitlist || players.length < tournament.maxPlayers;
  return (
    <Button variant='default' onClick={registerPlayer} disabled={!canRegister}>
      {registering && <Icons.spinner />}&nbsp;Register
    </Button>
  );

  async function registerPlayer() {
    setRegistering(true);
    try {
      const { status } = await axios.post(
        `/api/tournament/${tournament.id}/register`
      );
      if (status !== HttpStatusCode.Ok) {
        toast.error(
          'Could not register into the tournament, please try again later'
        );
      }
      toast.success('You have been registered into the tournament');
      refetch();
    } catch (e) {
      if (e instanceof AxiosError) {
        console.error(e);
        toast.error(
          'Could not register into the tournament, please try again later'
        );
        return;
      }
      console.error(e);
      toast.error('An unexpected error occurred, please try again later');
    } finally {
      setRegistering(false);
    }
  }
}

interface StartTournamentButtonProps {
  tournamentId: number;
  refetch: () => void;
}
function StartTournamentButton({
  tournamentId,
  refetch,
}: StartTournamentButtonProps) {
  const [registering, setRegistering] = useState<boolean>(false);

  return (
    <Button variant='positive' onClick={startTournament}>
      <Play size={16} /> Start tournament
    </Button>
  );

  async function startTournament() {
    try {
      const { status } = await axios.post(
        `/api/tournament/${tournamentId}/start`
      );
      if (status !== HttpStatusCode.Ok) {
        toast.error('Could not start the tournament, please try again later');
      } else {
        toast.success('The tournament has started');
        refetch();
      }
    } catch (e) {
      console.error(e);
      if (axios.isAxiosError(e)) {
        if (e.response?.status === HttpStatusCode.Forbidden) {
          toast.error(
            'You do not have permission to start the tournament, try refreshing the page or logging in again'
          );
          return;
        }
        toast.error('Could not start the tournament, please try again later');
        return;
      }
      toast.error('An unexpected error occurred, please try again later');
    }
  }
}

function LoadingPlayers({ count = 5, isTournamentStarted = false }) {
  return (
    <>
      <Card>
        <CardHeader>
          <CardTitle>Registered players</CardTitle>
        </CardHeader>
        <CardContent
          className={cn('grid gap-2', {
            'grid-cols:2 md:grid-cols-3 lg:grid-cols-4': !isTournamentStarted,
            'grid-cols:1 md:grid-cols-2 lg:grid-cols-3': isTournamentStarted,
          })}>
          {[...Array(count)].map((_, i) => (
            <div
              key={i}
              className='border rounded-lg p-4 flex flex-col items-start space-y-4'>
              <div className='flex justify-between w-full h-16'>
                <Skeleton className='h-6 w-24' /> {/* Player name */}
                <Skeleton className='h-4 w-20' /> {/* Waitlist status */}
              </div>
            </div>
          ))}
        </CardContent>
      </Card>
    </>
  );
}

function LoadingRankings() {
  return (
    <Card>
      <CardHeader>
        <CardTitle>Rankings</CardTitle>
      </CardHeader>
      <CardContent>
        <Table>
          <TableHeader>
            <TableRow>
              <TableHead>Position</TableHead>
              <TableHead>Player</TableHead>
            </TableRow>
          </TableHeader>
          <TableBody>
            {[...Array(3)].map((_, i) => (
              <TableRow key={i}>
                <TableCell>
                  <Skeleton className='h-4 w-8' />
                </TableCell>
                <TableCell>
                  <Skeleton className='h-4 w-24' />
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </CardContent>
    </Card>
  );
}

function LoadingButtons() {
  return (
    <>
      <Skeleton className='h-10 w-32' />
      <Skeleton className='h-10 w-40' />
    </>
  );
}
