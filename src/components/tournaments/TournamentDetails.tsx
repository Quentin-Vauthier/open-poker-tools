'use client';

import { format } from 'date-fns';
import { CalendarIcon } from 'lucide-react';

import StructureModalButton from '@/components/modals/tournament/StructureModalButton';
import { Checkbox } from '@/components/ui/checkbox';
import { Input } from '@/components/ui/input';
import OPTReadOnlyField from '@/components/ui/OPTReadOnlyField';
import { Tournament, TournamentInfo } from '@/types';
import Editor from '@components/Lexical/Editor';
import { Card, CardContent, CardHeader, CardTitle } from '@components/ui/card';

interface TournamentDetails extends Tournament {
  series: string;
  structure: string;
}

export default function TournamentDetails({
  tournament,
  className,
}: {
  tournament: TournamentInfo;
  className?: string;
}) {
  if (!tournament) {
    return <section className='text-lg'>Tournament details not found</section>;
  }

  return (
    <section className={className}>
      <Card className='p-4'>
        <CardHeader>
          <CardTitle>Tournament details</CardTitle>
        </CardHeader>
        <CardContent>
          <div className='grid gap-4'>
            {/* Top section - Date, Location, Structure, Series */}
            <div className='grid gap-4 sm:grid-cols-2'>
              <OPTReadOnlyField label='Date'>
                <div className='relative flex items-center'>
                  <CalendarIcon className='absolute left-3 h-4 w-4' />
                  <Input
                    className='pl-10'
                    value={
                      tournament.date
                        ? format(tournament.date, 'PPP p')
                        : 'No date'
                    }
                    readOnly
                  />
                </div>
              </OPTReadOnlyField>
              <OPTReadOnlyField label='Location'>
                <Input value={tournament.location ?? 'N/A'} readOnly />
              </OPTReadOnlyField>
              <OPTReadOnlyField label='Structure'>
                <div className='flex items-center gap-2'>
                  <Input
                    className='flex-grow'
                    value={tournament.structure ?? 'N/A'}
                    readOnly
                  />
                  {!!tournament.structureId && (
                    <StructureModalButton
                      structureId={tournament.structureId}
                    />
                  )}
                </div>
              </OPTReadOnlyField>
              <OPTReadOnlyField label='Series'>
                <Input value={tournament.series ?? 'N/A'} readOnly />
              </OPTReadOnlyField>
            </div>

            {/* Buy-in and Starting stack */}
            <div className='grid gap-4 sm:grid-cols-2'>
              <OPTReadOnlyField label='Buy-in'>
                <Input
                  value={tournament.buyIn === 0 ? 'Free' : tournament.buyIn}
                  readOnly
                />
              </OPTReadOnlyField>
              <OPTReadOnlyField label='Starting stack'>
                <Input
                  value={tournament.startingStack}
                  type='number'
                  readOnly
                />
              </OPTReadOnlyField>
            </div>

            {/* Players and Knockout section */}
            <div className='grid gap-4 sm:grid-cols-2'>
              <OPTReadOnlyField label='Maximum players'>
                <Input
                  value={
                    tournament.maxPlayers === 0 ? '∞' : tournament.maxPlayers
                  }
                  readOnly
                />
              </OPTReadOnlyField>
              <div className='grid grid-cols-2 gap-4'>
                <div className='space-y-2 flex flex-col justify-end items-center'>
                  <OPTReadOnlyField
                    label='Knockout tournament'
                    className='flex items-center space-x-3 space-y-0'>
                    <Checkbox disabled checked={tournament.knockout} />
                  </OPTReadOnlyField>
                  <OPTReadOnlyField
                    label='Progressive ?'
                    className='flex items-center space-x-3 space-y-0'>
                    <Checkbox disabled checked={tournament.progressiveKo} />
                  </OPTReadOnlyField>
                </div>
                <OPTReadOnlyField label='Bounty'>
                  <Input
                    value={
                      tournament.knockoutBounty === 0
                        ? 'N/A'
                        : tournament.knockoutBounty
                    }
                    readOnly
                  />
                </OPTReadOnlyField>
              </div>
            </div>

            {/* Table sizes */}
            <div className='grid gap-4 sm:grid-cols-3'>
              <OPTReadOnlyField label='Table size'>
                <Input value={tournament.tableSize} type='number' readOnly />
              </OPTReadOnlyField>
              <OPTReadOnlyField label='Final table size'>
                <Input
                  value={tournament.finalTableSize}
                  type='number'
                  readOnly
                />
              </OPTReadOnlyField>
              <OPTReadOnlyField label='Minimum players per table'>
                <Input value={tournament.minTableSize} type='number' readOnly />
              </OPTReadOnlyField>
            </div>

            {/* Registration section */}
            <div className='grid gap-4 sm:grid-cols-3'>
              <div className='space-y-4'>
                <OPTReadOnlyField
                  label='Re-entry allowed'
                  className='flex items-center space-x-3 space-y-0'>
                  <Checkbox checked={tournament.canReentry} disabled />
                </OPTReadOnlyField>
                <OPTReadOnlyField label='Re-entry end' sup='(number of levels)'>
                  <Input
                    value={
                      tournament.canReentry ? tournament.reentryEnd ?? 0 : 'N/A'
                    }
                    readOnly
                  />
                </OPTReadOnlyField>
              </div>
              <div className='space-y-4'>
                <OPTReadOnlyField
                  label='Late registration'
                  className='flex items-center space-x-3 space-y-0'>
                  <Checkbox checked={tournament.canLateReg} disabled />
                </OPTReadOnlyField>
                <OPTReadOnlyField label='Late reg end' sup='(number of levels)'>
                  <Input
                    value={
                      tournament.canReentry ? tournament.lateRegEnd ?? 0 : 'N/A'
                    }
                    readOnly
                  />
                </OPTReadOnlyField>
              </div>
              <div className='flex flex-col justify-end'>
                <OPTReadOnlyField label='Variant'>
                  <Input value={tournament.variant} readOnly />
                </OPTReadOnlyField>
              </div>
            </div>

            {/* Description */}
            <OPTReadOnlyField label='Description'>
              <Editor
                initialValue={tournament.description ?? undefined}
                disabled
              />
            </OPTReadOnlyField>
          </div>
        </CardContent>
      </Card>
    </section>
  );
}
