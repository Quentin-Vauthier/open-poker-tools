'use client';

import axios from 'axios';
import { useState } from 'react';
import Modal from 'react-modal';
import { toast } from 'sonner';

import { Button } from '@/components/ui/button';
import { DynamicCombobox } from '@/components/ui/combobox';
import { Icons } from '@components/Icons';
import { useQueryClient } from '@tanstack/react-query';

interface AddPlayerButtonProps {
  tournamentId: number;
}

interface AddPlayerModalProps {
  tournamentId: number;
}

export default function AddPlayerButton({
  tournamentId,
}: AddPlayerButtonProps) {
  Modal.setAppElement('#main');
  const [isModalOpen, setIsModalOpen] = useState(false);

  return (
    <>
      <Button variant='default' onClick={() => setIsModalOpen(!isModalOpen)}>
        Add player
      </Button>
      <Modal
        isOpen={isModalOpen}
        onRequestClose={() => setIsModalOpen(false)}
        overlayClassName='fixed flex justify-center items-center top-0 left-0 w-full h-full bg-black bg-opacity-50 z-50'
        className='w-96 h-48 flex flex-col text-center bg-white px-4 py-4 gap-4 rounded-sm'>
        <AddPlayerModal tournamentId={tournamentId} />
      </Modal>
    </>
  );

  function AddPlayerModal({ tournamentId }: AddPlayerModalProps) {
    const [playerId, setPlayerId] = useState<number | null>(null);
    const [isRegistering, setIsRegistering] = useState(false);

    const queryClient = useQueryClient();

    return (
      <div className='h-full flex flex-col flex-grow'>
        <h2 className='text-lg'>Select a player to add</h2>
        <DynamicCombobox
          className='flex-grow mt-4'
          fetchItems={(query) => fetchPlayers(query)}
          placeholder='Select player'
          onSelect={(value) => setPlayerId(value ? parseInt(value) : null)}
          value={playerId?.toString()}
          emptyLabel='No player found'
          searchPlaceholder='Search players...'
          disabled={isRegistering}
        />
        <div className='flex items-end justify-end'>
          <Button
            className='flex gap-1.5'
            disabled={!playerId || isRegistering}
            onClick={() => registerPlayer(playerId!)}>
            {isRegistering && <Icons.spinner className='w-4 h-4' />}
            {isRegistering
              ? 'Adding player...'
              : playerId
              ? 'Add player'
              : 'Select a player'}
          </Button>
        </div>
      </div>
    );

    async function fetchPlayers(query: string) {
      const { data } = await axios.get<
        {
          id: number;
          nickname: string;
        }[]
      >(
        `/api/players?objectType=tournament&operator=notIn&limit=10&objectId=${tournamentId}&query=${query}`
      );
      return data.map((player) => ({
        value: player.id.toString(),
        label: player.nickname,
      }));
    }

    // TODO: Type safe this (server response)
    async function registerPlayer(playerId: number) {
      try {
        setIsRegistering(true);
        await axios.post(`/api/tournament/${tournamentId}/register`, {
          playerId,
        });
        setIsRegistering(false);
        setIsModalOpen(false);
        queryClient.invalidateQueries({
          queryKey: ['tournament', tournamentId],
        });
        toast.success('Player has been added to the tournament');
      } catch (e) {
        if (axios.isAxiosError(e) && e.response?.status === 400) {
          toast.error(e.response.data.message);
          return;
        }
        toast.error('An unexpected error occurred, please try again later');
        console.error(e);
      }
    }
  }
}
