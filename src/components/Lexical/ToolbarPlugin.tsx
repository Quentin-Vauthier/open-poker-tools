import { useLexicalComposerContext } from '@lexical/react/LexicalComposerContext';
import { mergeRegister } from '@lexical/utils';
import {
  $getSelection,
  $isRangeSelection,
  CAN_REDO_COMMAND,
  CAN_UNDO_COMMAND,
  FORMAT_ELEMENT_COMMAND,
  FORMAT_TEXT_COMMAND,
  REDO_COMMAND,
  SELECTION_CHANGE_COMMAND,
  UNDO_COMMAND,
} from 'lexical';
import {
  Bold,
  Italic,
  Redo2,
  Underline,
  Undo2,
  AlignLeft,
  AlignCenter,
  AlignRight,
  AlignJustify,
  Strikethrough,
} from 'lucide-react';
import { useCallback, useEffect, useRef, useState } from 'react';

const LowPriority = 1;

function Divider() {
  return <div className='border-l border-gray-300 mx-2' />;
}

export default function ToolbarPlugin() {
  const [editor] = useLexicalComposerContext();
  const toolbarRef = useRef(null);
  const [canUndo, setCanUndo] = useState(false);
  const [canRedo, setCanRedo] = useState(false);
  const [isBold, setIsBold] = useState(false);
  const [isItalic, setIsItalic] = useState(false);
  const [isUnderline, setIsUnderline] = useState(false);
  const [isStrikethrough, setIsStrikethrough] = useState(false);
  const [isEditable, setIsEditable] = useState(editor.isEditable());

  const $updateToolbar = useCallback(() => {
    const selection = $getSelection();
    if ($isRangeSelection(selection)) {
      // Update text format
      setIsBold(selection.hasFormat('bold'));
      setIsItalic(selection.hasFormat('italic'));
      setIsUnderline(selection.hasFormat('underline'));
      setIsStrikethrough(selection.hasFormat('strikethrough'));
    }
  }, []);

  useEffect(() => {
    return mergeRegister(
      editor.registerUpdateListener(({ editorState }) => {
        editorState.read(() => {
          $updateToolbar();
        });
      }),
      editor.registerCommand(
        SELECTION_CHANGE_COMMAND,
        (_payload, _newEditor) => {
          $updateToolbar();
          return false;
        },
        LowPriority
      ),
      editor.registerCommand(
        CAN_UNDO_COMMAND,
        (payload) => {
          setCanUndo(payload);
          return false;
        },
        LowPriority
      ),
      editor.registerCommand(
        CAN_REDO_COMMAND,
        (payload) => {
          setCanRedo(payload);
          return false;
        },
        LowPriority
      ),
      editor.registerEditableListener((editable) => {
        setIsEditable(editable);
      })
    );
  }, [editor, $updateToolbar]);

  return (
    <div
      className={`flex gap-1 items-center p-2 bg-gray-100 border-b border-gray-300 ${
        !isEditable ? 'opacity-50 pointer-events-none' : ''
      }`}
      ref={toolbarRef}>
      <button
        type='button'
        disabled={!canUndo}
        onClick={() => {
          editor.dispatchCommand(UNDO_COMMAND, undefined);
        }}
        className='p-2 disabled:opacity-50 rounded transition-transform transform active:scale-95'
        aria-label='Undo'>
        <Undo2 size={16} />
      </button>
      <button
        type='button'
        disabled={!canRedo}
        onClick={() => {
          editor.dispatchCommand(REDO_COMMAND, undefined);
        }}
        className='p-2 disabled:opacity-50 rounded transition-transform transform active:scale-95'
        aria-label='Redo'>
        <Redo2 size={16} />
      </button>
      <Divider />
      <button
        type='button'
        onClick={() => {
          editor.dispatchCommand(FORMAT_TEXT_COMMAND, 'bold');
        }}
        className={`p-2 rounded transition-transform transform active:scale-95 ${
          isBold ? 'bg-gray-300' : ''
        }`}
        aria-label='Format Bold'>
        <Bold size={16} />
      </button>
      <button
        type='button'
        onClick={() => {
          editor.dispatchCommand(FORMAT_TEXT_COMMAND, 'italic');
        }}
        className={`p-2 rounded transition-transform transform active:scale-95 ${
          isItalic ? 'bg-gray-300' : ''
        }`}
        aria-label='Format Italics'>
        <Italic size={16} />
      </button>
      <button
        type='button'
        onClick={() => {
          editor.dispatchCommand(FORMAT_TEXT_COMMAND, 'underline');
        }}
        className={`p-2 rounded transition-transform transform active:scale-95 ${
          isUnderline ? 'bg-gray-300' : ''
        }`}
        aria-label='Format Underline'>
        <Underline size={16} />
      </button>
      <button
        type='button'
        onClick={() => {
          editor.dispatchCommand(FORMAT_TEXT_COMMAND, 'strikethrough');
        }}
        className={`p-2 rounded transition-transform transform active:scale-95 ${
          isStrikethrough ? 'bg-gray-300' : ''
        }`}
        aria-label='Format Strikethrough'>
        <Strikethrough size={16} />
      </button>
      <Divider />
      <button
        type='button'
        onClick={() => {
          editor.dispatchCommand(FORMAT_ELEMENT_COMMAND, 'left');
        }}
        className='p-2 rounded transition-transform transform active:scale-95'
        aria-label='Left Align'>
        <AlignLeft size={16} />
      </button>
      <button
        type='button'
        onClick={() => {
          editor.dispatchCommand(FORMAT_ELEMENT_COMMAND, 'center');
        }}
        className='p-2 rounded transition-transform transform active:scale-95'
        aria-label='Center Align'>
        <AlignCenter size={16} />
      </button>
      <button
        type='button'
        onClick={() => {
          editor.dispatchCommand(FORMAT_ELEMENT_COMMAND, 'right');
        }}
        className='p-2 rounded transition-transform transform active:scale-95'
        aria-label='Right Align'>
        <AlignRight size={16} />
      </button>
      <button
        type='button'
        onClick={() => {
          editor.dispatchCommand(FORMAT_ELEMENT_COMMAND, 'justify');
        }}
        className='p-2 rounded transition-transform transform active:scale-95'
        aria-label='Justify Align'>
        <AlignJustify size={16} />
      </button>{' '}
    </div>
  );
}
