import { useEffect, useRef } from 'react';

import { LexicalComposer } from '@lexical/react/LexicalComposer';
import { ContentEditable } from '@lexical/react/LexicalContentEditable';
import { LexicalErrorBoundary } from '@lexical/react/LexicalErrorBoundary';
import { HistoryPlugin } from '@lexical/react/LexicalHistoryPlugin';
import { RichTextPlugin } from '@lexical/react/LexicalRichTextPlugin';
import {
  $getRoot,
  $insertNodes,
  $isTextNode,
  DOMConversionMap,
  DOMExportOutput,
  DOMExportOutputMap,
  isHTMLElement,
  Klass,
  LexicalEditor,
  LexicalNode,
  ParagraphNode,
  TextNode,
} from 'lexical';
import ToolbarPlugin from '@components/Lexical/ToolbarPlugin';
import ExampleTheme from '@/components/Lexical/EditorTheme';
import { useLexicalComposerContext } from '@lexical/react/LexicalComposerContext';
import { $generateNodesFromDOM } from '@lexical/html';

const MIN_ALLOWED_FONT_SIZE = 8;
const MAX_ALLOWED_FONT_SIZE = 72;

type EditorMode = 'html' | 'lexical';

export const parseAllowedFontSize = (input: string): string => {
  const match = input.match(/^(\d+(?:\.\d+)?)px$/);
  if (match) {
    const n = Number(match[1]);
    if (n >= MIN_ALLOWED_FONT_SIZE && n <= MAX_ALLOWED_FONT_SIZE) {
      return input;
    }
  }
  return '';
};

export function parseAllowedColor(input: string) {
  return /^rgb\(\d+, \d+, \d+\)$/.test(input) ? input : '';
}

// Catch any errors that occur during Lexical updates and log them
// or throw them as needed. If you don't throw them, Lexical will
// try to recover gracefully without losing user data.
function onError(error: any) {
  console.error(error);
}

const removeStylesExportDOM = (
  editor: LexicalEditor,
  target: LexicalNode
): DOMExportOutput => {
  const output = target.exportDOM(editor);
  if (output && isHTMLElement(output.element)) {
    // Remove all inline styles and classes if the element is an HTMLElement
    // Children are checked as well since TextNode can be nested
    // in i, b, and strong tags.
    for (const el of [
      output.element,
      ...output.element.querySelectorAll('[style],[class],[dir="ltr"]'),
    ]) {
      el.removeAttribute('class');
      el.removeAttribute('style');
      if (el.getAttribute('dir') === 'ltr') {
        el.removeAttribute('dir');
      }
    }
  }
  return output;
};

const exportMap: DOMExportOutputMap = new Map<
  Klass<LexicalNode>,
  (editor: LexicalEditor, target: LexicalNode) => DOMExportOutput
>([
  [ParagraphNode, removeStylesExportDOM],
  [TextNode, removeStylesExportDOM],
]);

const getExtraStyles = (element: HTMLElement): string => {
  // Parse styles from pasted input, but only if they match exactly the
  // sort of styles that would be produced by exportDOM
  let extraStyles = '';
  const fontSize = parseAllowedFontSize(element.style.fontSize);
  const backgroundColor = parseAllowedColor(element.style.backgroundColor);
  const color = parseAllowedColor(element.style.color);
  if (fontSize !== '' && fontSize !== '15px') {
    extraStyles += `font-size: ${fontSize};`;
  }
  if (backgroundColor !== '' && backgroundColor !== 'rgb(255, 255, 255)') {
    extraStyles += `background-color: ${backgroundColor};`;
  }
  if (color !== '' && color !== 'rgb(0, 0, 0)') {
    extraStyles += `color: ${color};`;
  }
  return extraStyles;
};

const constructImportMap = (): DOMConversionMap => {
  const importMap: DOMConversionMap = {};

  // Wrap all TextNode importers with a function that also imports
  // the custom styles implemented by the playground
  for (const [tag, fn] of Object.entries(TextNode.importDOM() || {})) {
    importMap[tag] = (importNode) => {
      const importer = fn(importNode);
      if (!importer) {
        return null;
      }
      return {
        ...importer,
        conversion: (element) => {
          const output = importer.conversion(element);
          if (
            output === null ||
            output.forChild === undefined ||
            output.after !== undefined ||
            output.node !== null
          ) {
            return output;
          }
          const extraStyles = getExtraStyles(element);
          if (extraStyles) {
            const { forChild } = output;
            return {
              ...output,
              forChild: (child, parent) => {
                const textNode = forChild(child, parent);
                if ($isTextNode(textNode)) {
                  textNode.setStyle(textNode.getStyle() + extraStyles);
                }
                return textNode;
              },
            };
          }
          return output;
        },
      };
    };
  }

  return importMap;
};

function OnChangePlugin({
  onChange,
  mode,
}: {
  onChange: (html: string | undefined) => void;
  mode: EditorMode;
}) {
  const [editor] = useLexicalComposerContext();
  useEffect(() => {
    return editor.registerUpdateListener(({ editorState }) => {
      if (mode === 'html') {
        const html = editor.getRootElement()?.innerHTML;
        onChange(html);
      } else if (mode === 'lexical') {
        const editorState = editor.toJSON().editorState;
        onChange(JSON.stringify(editorState));
      }
    });
  }, [editor, onChange, mode]);
  return null;
}

function InitialValuePlugin({
  initialValue,
  mode,
}: {
  initialValue: string;
  mode: EditorMode;
}) {
  const [editor] = useLexicalComposerContext();
  const isInitialValueSet = useRef(false);

  useEffect(() => {
    if (isInitialValueSet.current) return;

    editor.update(() => {
      if (!initialValue) return;
      if (mode === 'html') {
        // const root = editor.getRootElement();
        // if (root) {
        //   root.innerHTML = initialValue;
        //   isInitialValueSet.current = true;
        // }
        // In the browser you can use the native DOMParser API to parse the HTML string.
        const parser = new DOMParser();
        const dom = parser.parseFromString(initialValue, 'text/html');

        // Once you have the DOM instance it's easy to generate LexicalNodes.
        const nodes = $generateNodesFromDOM(editor, dom);

        // Select the root
        $getRoot().select();

        // Insert them at a selection.
        $insertNodes(nodes);
      } else if (mode === 'lexical') {
        try {
          const editorState = editor.parseEditorState(JSON.parse(initialValue));
          editor.setEditorState(editorState);
          isInitialValueSet.current = true;
        } catch (error) {
          console.error(
            'Failed to parse default value for lexical mode:',
            error
          );
        }
      }
    });
  }, [editor, initialValue, mode]);

  return null;
}

interface EditorProps {
  mode?: EditorMode;
  onChange?: (html: string | undefined) => void;
  initialValue?: string;
  disabled?: boolean;
}

export default function Editor({
  mode = 'lexical',
  onChange,
  initialValue = '',
  disabled = false,
}: EditorProps) {
  const placeholder = 'Type here...';

  const editorConfig = {
    editable: !disabled,
    html: {
      export: exportMap,
      import: constructImportMap(),
    },
    namespace: 'Open Poker Tools',
    nodes: [ParagraphNode, TextNode],
    onError(error: Error) {
      throw error;
    },
    theme: ExampleTheme,
  };

  return (
    <LexicalComposer initialConfig={editorConfig}>
      <div className='border border-border rounded-sm min-h-40'>
        <ToolbarPlugin />
        <div className='grid'>
          <RichTextPlugin
            contentEditable={
              <ContentEditable
                className='outline-none min-h-32 p-2'
                aria-placeholder={disabled ? '' : placeholder}
                placeholder={(isEditable) =>
                  isEditable ? (
                    <div className='editor-placeholder p-2 pl-2.5'>
                      {placeholder}
                    </div>
                  ) : null
                }
              />
            }
            ErrorBoundary={LexicalErrorBoundary}
          />
          <HistoryPlugin />
          {/* <AutoFocusPlugin /> */}
          {onChange && <OnChangePlugin onChange={onChange} mode={mode} />}
          {initialValue && (
            <InitialValuePlugin initialValue={initialValue} mode={mode} />
          )}
        </div>
      </div>
    </LexicalComposer>
  );
}
