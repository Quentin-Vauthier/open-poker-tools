'use client';

import { readOnlyColumns } from '@/components/structure/levels/Columns';
import { ReadOnlyDataTable } from '@/components/ReadOnlyDataTable';
import { Level } from '@/types';

export default function ViewLevelsGrid({
  data,
  className,
}: {
  data: Level[];
  className?: string;
}) {
  return (
    <ReadOnlyDataTable
      className={className}
      columns={readOnlyColumns}
      data={data}
    />
  );
}
