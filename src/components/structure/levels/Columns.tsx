import { Trash2 } from 'lucide-react';

import { Button } from '@/components/ui/button';
import { Checkbox } from '@/components/ui/checkbox';
import { Input } from '@/components/ui/input';
import { cn } from '@/lib/utils';
import { Level } from '@/types';
import { ColumnDef } from '@tanstack/react-table';

const editableColumns: ColumnDef<Level>[] = [
  {
    id: 'actions',
    header: 'Actions',
    cell: ({ row, table }) => {
      return (
        <div className='flex items-center justify-center gap-1'>
          <Button
            className='p-2 h-fit'
            variant='outline'
            title='Delete row'
            onClick={() => {
              table.options.meta?.deleteRow?.(row.index);
            }}>
            <Trash2 size={12} />
            <span className='sr-only'>Delete row button</span>
          </Button>
          <div className='flex flex-col gap-1'>
            <Button
              className='px-1 py-0.5 h-fit'
              variant='outline'
              title='Move row up'
              onClick={() => {
                table.options.meta?.moveUp?.(row.index);
              }}>
              ↑
            </Button>
            <Button
              className='px-1 py-0.5 h-fit'
              variant='outline'
              title='Move row down'
              onClick={() => {
                table.options.meta?.moveDown?.(row.index);
              }}>
              ↓
            </Button>
          </div>
        </div>
      );
    },
  },
  {
    accessorKey: 'isBreak',
    header: 'Break ?',
    cell: ({ row, table }) => {
      return (
        <div className='flex items-center justify-center w-full h-full'>
          <Checkbox
            checked={row.getValue('isBreak')}
            onCheckedChange={(v) => {
              table.options.meta?.updateData?.(row.index, 'isBreak', v);
            }}
          />
        </div>
      );
    },
  },
  {
    accessorKey: 'sb',
    header: 'Small blind',
    cell: ({ row, table }) => {
      return (
        <div className='flex items-center justify-center'>
          <Input
            disabled={row.getValue('isBreak')}
            type='number'
            inputMode='numeric'
            title={
              table.options.meta?.errors?.find((error) => {
                return error.path[0] === row.index && error.path[1] === 'sb';
              })?.message
            }
            className={cn('w-32 text-center', {
              'border-red-500': table.options.meta?.errors?.find((error) => {
                return error.path[0] === row.index && error.path[1] === 'sb';
              }),
            })}
            value={row.getValue('isBreak') ? '' : row.getValue('sb')}
            onChange={(e) => {
              table.options.meta?.updateData?.(
                row.index,
                'sb',
                parseInt(e.target.value)
              );
            }}
            onBlur={(e) => {
              if (row.getValue('bb') === 0) {
                table.options.meta?.updateData?.(
                  row.index,
                  'bb',
                  parseInt(e.target.value) * 2
                );
              }
            }}
          />
        </div>
      );
    },
  },
  {
    accessorKey: 'bb',
    header: 'Big blind',
    cell: ({ row, table }) => {
      return (
        <div className='flex items-center justify-center'>
          <Input
            disabled={row.getValue('isBreak')}
            type='number'
            inputMode='numeric'
            title={
              table.options.meta?.errors?.find((error) => {
                return error.path[0] === row.index && error.path[1] === 'bb';
              })?.message
            }
            className={cn('w-32 text-center', {
              'border-red-500': table.options.meta?.errors?.find((error) => {
                return error.path[0] === row.index && error.path[1] === 'bb';
              }),
            })}
            value={row.getValue('isBreak') ? '' : row.getValue('bb')}
            onChange={(e) => {
              table.options.meta?.updateData?.(
                row.index,
                'bb',
                parseInt(e.target.value)
              );
            }}
          />
        </div>
      );
    },
  },
  {
    accessorKey: 'ante',
    header: 'Ante',
    cell: ({ row, table }) => {
      return (
        <div className='flex items-center justify-center'>
          <Input
            disabled={row.getValue('isBreak')}
            type='number'
            inputMode='numeric'
            title={
              table.options.meta?.errors?.find((error) => {
                return error.path[0] === row.index && error.path[1] === 'ante';
              })?.message
            }
            className={cn('w-32 text-center', {
              'border-red-500': table.options.meta?.errors?.find((error) => {
                return error.path[0] === row.index && error.path[1] === 'ante';
              }),
            })}
            value={row.getValue('isBreak') ? '' : row.getValue('ante')}
            onChange={(e) => {
              table.options.meta?.updateData?.(
                row.index,
                'ante',
                parseInt(e.target.value)
              );
            }}
          />
        </div>
      );
    },
  },
  {
    accessorKey: 'duration',
    header: 'Duration (min)',
    cell: ({ row, table }) => {
      return (
        <div className='flex items-center justify-center'>
          <Input
            type='number'
            inputMode='numeric'
            title={
              table.options.meta?.errors?.find((error) => {
                return (
                  error.path[0] === row.index && error.path[1] === 'duration'
                );
              })?.message
            }
            className={cn('w-20 text-center', {
              'border-red-500': table.options.meta?.errors?.find((error) => {
                return (
                  error.path[0] === row.index && error.path[1] === 'duration'
                );
              }),
            })}
            value={row.getValue('duration')}
            onChange={(e) => {
              table.options.meta?.updateData?.(
                row.index,
                'duration',
                parseInt(e.target.value)
              );
            }}
          />
        </div>
      );
    },
  },
];

const readOnlyColumns: ColumnDef<Level>[] = [
  {
    accessorKey: 'isBreak',
    header: 'Break ?',
    cell: ({ row, table }) => {
      return (
        <div className='flex items-center justify-center w-full h-full'>
          <Checkbox
            disabled
            checked={row.getValue('isBreak')}
            onCheckedChange={(v) => {
              table.options.meta?.updateData?.(row.index, 'isBreak', v);
            }}
          />
        </div>
      );
    },
  },
  {
    accessorKey: 'sb',
    header: 'Small blind',
    cell: ({ row, table }) => {
      return (
        <div className='flex items-center justify-center'>
          <span>{row.getValue('isBreak') ? '' : row.getValue('sb')}</span>
        </div>
      );
    },
  },
  {
    accessorKey: 'bb',
    header: 'Big blind',
    cell: ({ row, table }) => {
      return (
        <div className='flex items-center justify-center'>
          <span>{row.getValue('isBreak') ? '' : row.getValue('bb')}</span>
        </div>
      );
    },
  },
  {
    accessorKey: 'ante',
    header: 'Ante',
    cell: ({ row, table }) => {
      return (
        <div className='flex items-center justify-center'>
          <span>{row.getValue('isBreak') ? '' : row.getValue('ante')}</span>
        </div>
      );
    },
  },
  {
    accessorKey: 'duration',
    header: 'Duration (min)',
    cell: ({ row, table }) => {
      return (
        <div className='flex items-center justify-center'>
          <span>{row.getValue('duration')}</span>
        </div>
      );
    },
  },
];

export { editableColumns, readOnlyColumns };
