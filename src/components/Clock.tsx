'use client';
import { cn } from '@/lib/utils';
import { useEffect, useState } from 'react';

export default function Clock({
  className = '',
  dateFormat = undefined,
  timeFormat = undefined,
  showDate = true,
  showTime = true,
}: {
  className?: string;
  dateFormat?: Intl.DateTimeFormatOptions;
  timeFormat?: Intl.DateTimeFormatOptions;
  showDate?: boolean;
  showTime?: boolean;
}) {
  const [currentTime, setCurrentTime] = useState<Date>(new Date());
  useEffect(() => {
    setTimeout(() => setCurrentTime(new Date()), 500); // Update every 500ms, not every second to avoid skipping seconds :(
  }, [currentTime]);

  useEffect(() => {
    // Function to update the time and handle the drift adjustment
    const tick = () => {
      const now = new Date();
      setCurrentTime(now);

      // Calculate the delay for the next update
      const nextTick = 1000 - now.getMilliseconds(); // Milliseconds left until the next second
      setTimeout(tick, nextTick); // Schedule the next tick
    };

    // Initialize the first tick
    const initialTimeout = setTimeout(
      tick,
      1000 - new Date().getMilliseconds()
    );

    // Cleanup function to clear timeout when the component unmounts
    return () => clearTimeout(initialTimeout);
  }, []);

  dateFormat ??= {
    weekday: 'long',
    month: 'long',
    day: 'numeric',
  };
  timeFormat ??= {
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit',
    hour12: false,
  };

  // Format the date part (e.g., "Wed Sep 5")
  const formattedDate = currentTime.toLocaleDateString(undefined, dateFormat);

  // Format the time part (e.g., "14:05")
  const formattedTime = currentTime.toLocaleString(undefined, timeFormat);

  return (
    <div className={className}>
      {showDate && formattedDate}
      {showDate && showTime && ' - '}
      {showTime && formattedTime}
    </div>
  );
}
