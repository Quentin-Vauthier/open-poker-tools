import { Skeleton } from '@/components/ui/skeleton';
import { cn } from '@/lib/utils';

export default function TableSkeleton({
  className,
  colsCount = 4,
}: {
  className?: string;
  colsCount?: number;
}) {
  const gridCols = colsCount >= 12 ? 12 : colsCount;
  const gridColsArray = Array.from({ length: gridCols });
  const classObj = {
    'grid-cols-1': gridCols === 1,
    'grid-cols-2': gridCols === 2,
    'grid-cols-3': gridCols === 3,
    'grid-cols-4': gridCols === 4,
    'grid-cols-5': gridCols === 5,
    'grid-cols-6': gridCols === 6,
    'grid-cols-7': gridCols === 7,
    'grid-cols-8': gridCols === 8,
    'grid-cols-9': gridCols === 9,
    'grid-cols-10': gridCols === 10,
    'grid-cols-11': gridCols === 11,
    'grid-cols-12': gridCols === 12,
  };
  return (
    <div
      className={cn(
        'border border-dashed border-gray-200 rounded-lg py-6 w-full flex items-center justify-center flex-col',
        className
      )}>
      <div
        className={cn(`grid w-full gap-6 px-6 justify-items-center`, classObj)}>
        {gridColsArray.map((_, i) => {
          const rand = Math.floor(Math.random() * 5);
          return (
            <Skeleton
              key={i}
              className={cn('h-4 w-full', {
                'w-2/3': rand % 5 === 0,
                'w-1/2': rand % 5 === 1,
                'w-1/3': rand % 5 === 2,
                'w-1/4': rand % 5 === 3,
                'w-1/5': rand % 5 === 4,
              })}
            />
          );
        })}
      </div>
      {Array.from({ length: 4 }).map((_, i) => (
        <div key={i} className='w-full'>
          <div className='h-px w-full my-6 bg-gray-100 dark:bg-gray-800' />
          <div
            className={cn(
              `grid w-full gap-6 px-6 justify-items-center`,
              classObj
            )}>
            {gridColsArray.map((_, j) => {
              const rand = Math.floor(Math.random() * 5);
              return (
                <Skeleton
                  key={4 * i + j}
                  className={cn('h-4 w-full bg-muted-foreground', {
                    'w-2/3': rand % 5 === 0,
                    'w-1/2': rand % 5 === 1,
                    'w-1/3': rand % 5 === 2,
                    'w-1/4': rand % 5 === 3,
                    'w-1/5': rand % 5 === 4,
                  })}
                />
              );
            })}
          </div>
        </div>
      ))}
    </div>
  );
}
