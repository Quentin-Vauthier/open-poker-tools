'use client';

import {
  ColumnDef,
  flexRender,
  getCoreRowModel,
  useReactTable,
} from '@tanstack/react-table';

import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from '@/components/ui/table';
import {
  ContextMenu,
  ContextMenuContent,
  ContextMenuItem,
  ContextMenuTrigger,
} from '@/components/ui/context-menu';
import { Copy, Trash } from 'lucide-react';

interface DataTableProps<TData, TValue> {
  columns: ColumnDef<TData, TValue>[];
  data: TData[];
}

declare module '@tanstack/table-core' {
  interface TableMeta<TData> {
    errors?: { path: (string | number)[]; message: string }[];
    updateData?: (rowIndex: number, column: string, value: any) => void;
    deleteRow?: (rowIndex: number) => void;
    duplicateRow?: (rowIndex: number) => void;
    moveUp?: (rowIndex: number) => void;
    moveDown?: (rowIndex: number) => void;
  }
}

export function EditableDataTable<TData, TValue>({
  columns,
  data,
  setData,
  emptyRowObject,
  diaplayNoResults = true,
  errors = [],
  updateErrors,
}: DataTableProps<TData, TValue> & {
  setData: React.Dispatch<React.SetStateAction<TData[]>>;
  emptyRowObject: TData;
  diaplayNoResults?: boolean;
  errors?: { path: (string | number)[]; message: string }[];
  updateErrors?: () => void;
}) {
  const table = useReactTable({
    data,
    columns,
    getCoreRowModel: getCoreRowModel(),
    meta: {
      errors,
      updateData: (rowIndex: number, column: string, value: any) => {
        setData((prev: TData[]) => {
          return prev.map((row, index) => {
            if (index === rowIndex) {
              return {
                ...prev[rowIndex],
                [column]: value,
              };
            }
            return row;
          });
        });
        updateErrors?.();
      },
      deleteRow: (rowIndex: number) => {
        setData((prev: TData[]) => {
          return prev.filter((_, index) => index !== rowIndex);
        });
      },
      duplicateRow: (rowIndex: number) => {
        setData((prev: TData[]) => {
          const copy = [...prev];
          copy.splice(rowIndex, 0, prev[rowIndex]);
          return copy;
        });
      },
      moveUp: (rowIndex: number) => {
        setData((prev: TData[]) => {
          if (rowIndex === 0) return prev;
          const copy = [...prev];
          const [removed] = copy.splice(rowIndex, 1);
          copy.splice(rowIndex - 1, 0, removed);
          return copy;
        });
      },
      moveDown: (rowIndex: number) => {
        setData((prev: TData[]) => {
          if (rowIndex === prev.length - 1) return prev;
          const copy = [...prev];
          const [removed] = copy.splice(rowIndex, 1);
          copy.splice(rowIndex + 1, 0, removed);
          return copy;
        });
      },
    },
  });

  return (
    <div className='text-center'>
      <div className='border rounded-md'>
        <Table>
          <TableHeader>
            {table.getHeaderGroups().map((headerGroup) => (
              <TableRow key={headerGroup.id}>
                {headerGroup.headers.map((header) => {
                  return (
                    <TableHead key={header.id} className='text-center'>
                      {header.isPlaceholder
                        ? null
                        : flexRender(
                            header.column.columnDef.header,
                            header.getContext()
                          )}
                    </TableHead>
                  );
                })}
              </TableRow>
            ))}
          </TableHeader>
          <TableBody>
            {table.getRowModel().rows?.length ? (
              table.getRowModel().rows.map((row) => (
                <ContextMenu key={row.id}>
                  <ContextMenuTrigger asChild>
                    <TableRow
                      key={row.id}
                      data-state={row.getIsSelected() && 'selected'}>
                      {row.getVisibleCells().map((cell) => (
                        <TableCell key={cell.id}>
                          {flexRender(
                            cell.column.columnDef.cell,
                            cell.getContext()
                          )}
                        </TableCell>
                      ))}
                    </TableRow>
                  </ContextMenuTrigger>
                  <ContextMenuContent>
                    <ContextMenuItem
                      onClick={() =>
                        setData((prev) => {
                          const copy = [...prev];
                          copy.splice(row.index, 0, emptyRowObject);
                          return copy;
                        })
                      }
                      inset>
                      ↑&nbsp;Insert row above
                    </ContextMenuItem>
                    <ContextMenuItem
                      onClick={() =>
                        setData((prev) => {
                          const copy = [...prev];
                          copy.splice(row.index + 1, 0, emptyRowObject);
                          return copy;
                        })
                      }
                      inset>
                      ↓&nbsp;Insert row below
                    </ContextMenuItem>
                    <ContextMenuItem
                      onClick={() => table.options.meta?.deleteRow?.(row.index)}
                      inset>
                      <Trash size={12} />
                      &nbsp;Delete row
                    </ContextMenuItem>
                    <ContextMenuItem
                      onClick={() =>
                        table.options.meta?.duplicateRow?.(row.index)
                      }
                      inset>
                      <Copy size={12} />
                      &nbsp;Duplicate row
                    </ContextMenuItem>
                  </ContextMenuContent>
                </ContextMenu>
              ))
            ) : (
              <TableRow>
                <TableCell
                  colSpan={columns.length}
                  className='h-24 text-center'>
                  {diaplayNoResults ? 'No results :(' : ''}
                </TableCell>
              </TableRow>
            )}
          </TableBody>
        </Table>
      </div>
      {errors.find((error) => error.path[0] === 'root') && (
        <p className='px-2 text-sm text-red-500'>
          {errors.find((error) => error.path[0] === 'root')?.message}
        </p>
      )}
    </div>
  );
}
