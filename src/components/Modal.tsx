import React, { ReactNode } from 'react';
import ReactModal from 'react-modal';

interface ModalProps {
  isOpen: boolean;
  onRequestClose: () => void;
  size?: 'small' | 'medium' | 'large' | 'extra-large';
  children: ReactNode;
}

const Modal: React.FC<ModalProps> = ({
  isOpen,
  onRequestClose,
  size = 'medium',
  children,
}) => {
  const sizeClass =
    size === 'small'
      ? 'modal-small'
      : size === 'medium'
      ? 'modal-medium'
      : size === 'large'
      ? 'modal-large'
      : 'modal-extra-large';

  return (
    <ReactModal
      isOpen={isOpen}
      onRequestClose={onRequestClose}
      overlayClassName='modal-overlay'
      className={`modal-content ${sizeClass}`}>
      {/* Casting because react-modal type look out of date */}
      {children as Exclude<bigint, ReactNode>}
    </ReactModal>
  );
};

export default Modal;
