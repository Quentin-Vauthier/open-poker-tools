import {
  FormControl,
  FormItem,
  FormLabel,
  FormMessage,
} from '@components/ui/form';

type OPTFormItemProps = {
  label: string;
  required?: boolean;
  children: React.ReactNode;
  className?: string;
  sup?: string;
};

export default function OPTFormItem({
  required = false,
  label,
  children,
  className,
  sup,
}: OPTFormItemProps) {
  return (
    <FormItem className={className}>
      <FormLabel>
        {label} <sup className='text-slate-500'>{sup}</sup>{' '}
        {required && <span className='text-red-500'>*</span>}
      </FormLabel>
      <FormControl>{children}</FormControl>
      <FormMessage />
    </FormItem>
  );
}
