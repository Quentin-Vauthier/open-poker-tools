import { cn } from '@/lib/utils';

import { Label } from '@components/ui/label';

type OPTFormItemProps = {
  label: string;
  required?: boolean;
  children: React.ReactNode;
  className?: string;
  sup?: string;
};

export default function OPTReadOnlyField({
  required = false,
  label,
  children,
  className,
  sup,
}: OPTFormItemProps) {
  return (
    <div className={cn('space-y-2', className)}>
      <Label className='text-sm font-medium'>
        {label} <sup className='text-slate-500'>{sup}</sup>
      </Label>
      {children}
    </div>
  );
}
