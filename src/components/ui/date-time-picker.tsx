'use client';

import * as React from 'react';
import { format } from 'date-fns';
import { Calendar as CalendarIcon } from 'lucide-react';

import { cn } from '@/lib/utils';
import { Button } from '@/components/ui/button';
import { Calendar } from '@/components/ui/calendar';
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from '@/components/ui/popover';
import { StaticCombobox } from '@components/ui/combobox';

const timeData = [
  { label: format('2000-01-01T00:00:00', 'p'), value: '00:00' },
  { label: format('2000-01-01T00:30:00', 'p'), value: '00:30' },
  { label: format('2000-01-01T01:00:00', 'p'), value: '01:00' },
  { label: format('2000-01-01T01:30:00', 'p'), value: '01:30' },
  { label: format('2000-01-01T02:00:00', 'p'), value: '02:00' },
  { label: format('2000-01-01T02:30:00', 'p'), value: '02:30' },
  { label: format('2000-01-01T03:00:00', 'p'), value: '03:00' },
  { label: format('2000-01-01T03:30:00', 'p'), value: '03:30' },
  { label: format('2000-01-01T04:00:00', 'p'), value: '04:00' },
  { label: format('2000-01-01T04:30:00', 'p'), value: '04:30' },
  { label: format('2000-01-01T05:00:00', 'p'), value: '05:00' },
  { label: format('2000-01-01T05:30:00', 'p'), value: '05:30' },
  { label: format('2000-01-01T06:00:00', 'p'), value: '06:00' },
  { label: format('2000-01-01T06:30:00', 'p'), value: '06:30' },
  { label: format('2000-01-01T07:00:00', 'p'), value: '07:00' },
  { label: format('2000-01-01T07:30:00', 'p'), value: '07:30' },
  { label: format('2000-01-01T08:00:00', 'p'), value: '08:00' },
  { label: format('2000-01-01T08:30:00', 'p'), value: '08:30' },
  { label: format('2000-01-01T09:00:00', 'p'), value: '09:00' },
  { label: format('2000-01-01T09:30:00', 'p'), value: '09:30' },
  { label: format('2000-01-01T10:00:00', 'p'), value: '10:00' },
  { label: format('2000-01-01T10:30:00', 'p'), value: '10:30' },
  { label: format('2000-01-01T11:00:00', 'p'), value: '11:00' },
  { label: format('2000-01-01T11:30:00', 'p'), value: '11:30' },
  { label: format('2000-01-01T12:00:00', 'p'), value: '12:00' },
  { label: format('2000-01-01T12:30:00', 'p'), value: '12:30' },
  { label: format('2000-01-01T13:00:00', 'p'), value: '13:00' },
  { label: format('2000-01-01T13:30:00', 'p'), value: '13:30' },
  { label: format('2000-01-01T14:00:00', 'p'), value: '14:00' },
  { label: format('2000-01-01T14:30:00', 'p'), value: '14:30' },
  { label: format('2000-01-01T15:00:00', 'p'), value: '15:00' },
  { label: format('2000-01-01T15:30:00', 'p'), value: '15:30' },
  { label: format('2000-01-01T16:00:00', 'p'), value: '16:00' },
  { label: format('2000-01-01T16:30:00', 'p'), value: '16:30' },
  { label: format('2000-01-01T17:00:00', 'p'), value: '17:00' },
  { label: format('2000-01-01T17:30:00', 'p'), value: '17:30' },
  { label: format('2000-01-01T18:00:00', 'p'), value: '18:00' },
  { label: format('2000-01-01T18:30:00', 'p'), value: '18:30' },
  { label: format('2000-01-01T19:00:00', 'p'), value: '19:00' },
  { label: format('2000-01-01T19:30:00', 'p'), value: '19:30' },
  { label: format('2000-01-01T20:00:00', 'p'), value: '20:00' },
  { label: format('2000-01-01T20:30:00', 'p'), value: '20:30' },
  { label: format('2000-01-01T21:00:00', 'p'), value: '21:00' },
  { label: format('2000-01-01T21:30:00', 'p'), value: '21:30' },
  { label: format('2000-01-01T22:00:00', 'p'), value: '22:00' },
  { label: format('2000-01-01T22:30:00', 'p'), value: '22:30' },
  { label: format('2000-01-01T23:00:00', 'p'), value: '23:00' },
  { label: format('2000-01-01T23:30:00', 'p'), value: '23:30' },
];

type DateTimePickerProps = {
  onChange: (date: Date) => void;
};

export default function DateTimePicker({ onChange }: DateTimePickerProps) {
  const [date, setDate] = React.useState<Date>();
  const [time, setTime] = React.useState<string>();

  const placeholder = 'Pick a date';

  return (
    <Popover>
      <PopoverTrigger asChild>
        <Button
          variant={'outline'}
          className={cn(
            'w-full justify-start text-left font-normal',
            !date && 'text-muted-foreground'
          )}>
          <CalendarIcon className='mr-2 h-4 w-4' />
          {date ? format(date, 'PPP p') : <span>{placeholder}</span>}
        </Button>
      </PopoverTrigger>
      <PopoverContent className='w-auto p-0'>
        <Calendar
          mode='single'
          selected={date}
          onSelect={(newDate) => {
            if (!newDate) return setDate(undefined);
            const [hours, minutes] = time?.split(':') ?? [];
            if (hours && minutes) {
              newDate.setHours(+hours, +minutes);
              onChange(newDate);
            }
            setDate(newDate);
          }}
          initialFocus
        />
        <StaticCombobox
          className='px-2 py-1 w-full text-left border-t border-gray-200'
          data={timeData}
          value={time}
          placeholder='Start time'
          onSelect={(value) => {
            setTime(value);
            if (date) {
              const [hours, minutes] = value.split(':');
              setDate(new Date(date.setHours(+hours, +minutes)));
              onChange(date);
            }
          }}
        />
      </PopoverContent>
    </Popover>
  );
}
