/* eslint-disable react-hooks/exhaustive-deps */
'use client';

import * as React from 'react';
import { ChevronDown, Check } from 'lucide-react';

import { cn, debounce } from '@/lib/utils';
import { Button } from '@/components/ui/button';
import {
  Command,
  CommandEmpty,
  CommandGroup,
  CommandInput,
  CommandItem,
  CommandList,
} from '@/components/ui/command';
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from '@/components/ui/popover';
import { ScrollArea } from '@/components/ui/scroll-area';

interface StaticComboboxProps {
  emptyLabel?: string;
  searchable?: boolean;
  searchPlaceholder?: string;
  placeholder: string;
  data: Array<{ value: string; label: string }>;
  onSelect: (value: string) => void;
  width?: React.CSSProperties['width'];
  className?: string;
  value?: string;
  disabled?: boolean;
}

export function StaticCombobox({
  emptyLabel = 'No item found',
  searchable = true,
  searchPlaceholder = 'Search item...',
  placeholder,
  data,
  onSelect,
  width,
  className,
  value = '',
}: StaticComboboxProps) {
  const [open, setOpen] = React.useState<boolean>(false);

  return (
    <div className={className}>
      <Popover open={open} onOpenChange={setOpen}>
        <PopoverTrigger asChild>
          <Button
            variant='outline'
            role='combobox'
            aria-expanded={open}
            className={cn('w-full justify-between', width)}>
            {value
              ? data.find((item) => item.value === value)?.label ?? placeholder
              : placeholder}
            <ChevronDown className='ml-2 h-4 w-4 shrink-0 opacity-50' />
          </Button>
        </PopoverTrigger>
        <PopoverContent className={cn('w-full min-w-32 max-w-sm p-0', width)}>
          <Command>
            {searchable && (
              <CommandInput placeholder={searchPlaceholder} className='h-9' />
            )}
            <CommandList>
              <CommandEmpty>{emptyLabel}</CommandEmpty>
              <CommandGroup>
                {data.map((item) => (
                  <CommandItem
                    key={item.value}
                    value={item.value}
                    onSelect={(currentValue) => {
                      onSelect(currentValue === value ? '' : currentValue);
                      setOpen(false);
                    }}>
                    {item.label}
                    <Check
                      className={cn(
                        'ml-auto h-4 w-4',
                        value === item.value ? 'opacity-100' : 'opacity-0'
                      )}
                    />
                  </CommandItem>
                ))}
              </CommandGroup>
            </CommandList>
          </Command>
        </PopoverContent>
      </Popover>
    </div>
  );
}

interface DynamicComboboxProps {
  emptyLabel?: string;
  searchable?: boolean;
  searchPlaceholder?: string;
  placeholder: string;
  fetchItems: (
    query: string
  ) => Promise<Array<{ value: string; label: string }>>;
  initialItems?: Array<{ value: string; label: string }>;
  onSelect: (value: string) => void;
  width?: React.CSSProperties['width'];
  value?: string;
  className?: string;
  disabled?: boolean;
}

export function DynamicCombobox({
  emptyLabel = 'No item found',
  searchable = true,
  searchPlaceholder = 'Search item...',
  placeholder,
  fetchItems,
  initialItems = [],
  onSelect,
  width,
  value = '',
  className,
  disabled = false,
}: DynamicComboboxProps) {
  const [open, setOpen] = React.useState<boolean>(false);
  const [items, setItems] = React.useState<
    Array<{ value: string; label: string }>
  >([]);
  const [query, setQuery] = React.useState<string>('');
  const [isFetching, setIsFetching] = React.useState<boolean>(false);

  React.useEffect(() => {
    async function fetchInitialItems() {
      setIsFetching(true);
      const initialItems = await fetchItems('');
      setItems(initialItems);
      setIsFetching(false);
    }
    if (initialItems.length) {
      setItems(initialItems);
    } else {
      fetchInitialItems();
    }
  }, []);

  async function fetchThenSetItems(q: string) {
    setIsFetching(true);
    const selectedItem = items.find((item) => item.value === value);
    const newItems = await fetchItems(q);
    if (!selectedItem) setItems(newItems);
    else
      setItems([
        selectedItem,
        ...newItems.filter((item) => item.value !== value),
      ]);
    setIsFetching(false);
  }

  const debouncedFetchItems = React.useCallback(
    debounce(fetchThenSetItems, 700),
    [items, value]
  );

  React.useEffect(() => {
    setIsFetching(true);
    debouncedFetchItems(query);
  }, [query]);

  return (
    <div className={className}>
      <Popover open={open} onOpenChange={setOpen}>
        <PopoverTrigger disabled={disabled} asChild>
          <Button
            variant='outline'
            role='combobox'
            aria-expanded={open}
            className={cn('w-full justify-between', width)}>
            {value
              ? items.find((item) => item.value === value)?.label
              : placeholder}
            <ChevronDown className='ml-2 h-4 w-4 shrink-0 opacity-50' />
          </Button>
        </PopoverTrigger>
        <PopoverContent className={cn('w-full min-w-32 max-w-sm p-0', width)}>
          <Command
            // Override default filter as we are fetching items from the server
            filter={() => 1}>
            {searchable && (
              <CommandInput
                placeholder={searchPlaceholder}
                className='h-9'
                onValueChange={(q) => setQuery(q)}
                value={query}
              />
            )}
            <CommandList>
              {!isFetching && items.length === 0 && (
                <CommandEmpty>{emptyLabel}</CommandEmpty>
              )}
              {isFetching && <CommandEmpty>Loading...</CommandEmpty>}
              <CommandGroup>
                <ScrollArea>
                  <div className='max-h-80'>
                    {isFetching && items.length > 0 && (
                      <CommandItem disabled>Filtering...</CommandItem>
                    )}
                    {items.map((item) => (
                      <CommandItem
                        key={item.value}
                        value={item.value}
                        onSelect={(currentValue) => {
                          onSelect(currentValue === value ? '' : currentValue);
                          setOpen(false);
                        }}>
                        {item.label}
                        <Check
                          className={cn(
                            'ml-auto h-4 w-4',
                            value === item.value ? 'opacity-100' : 'opacity-0'
                          )}
                        />
                      </CommandItem>
                    ))}
                  </div>
                </ScrollArea>
              </CommandGroup>
            </CommandList>
          </Command>
        </PopoverContent>
      </Popover>
    </div>
  );
}
