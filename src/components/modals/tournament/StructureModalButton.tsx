'use client';

import { Search } from 'lucide-react';
import { useState } from 'react';
import Modal from 'react-modal';

import { Button } from '@/components/ui/button';

import StructureModal from '@components/modals/structure/StructureModal';

interface StructureModalButtonProps {
  structureId: number;
}

export default function StructureModalButton({
  structureId,
}: StructureModalButtonProps) {
  Modal.setAppElement('#main');
  const [structureModalOpen, setStructureModalOpen] = useState(false);
  return (
    <>
      <Button
        type='button'
        disabled={!structureId}
        onClick={() => setStructureModalOpen(true)}>
        <Search size={16} />
      </Button>
      <Modal
        isOpen={structureModalOpen}
        onRequestClose={() => setStructureModalOpen(false)}
        overlayClassName='fixed flex justify-center items-center top-0 left-0 w-full h-full bg-black bg-opacity-50 z-50'
        className='w-2/3 h-[32rem] flex flex-col text-center bg-white px-4 py-4 gap-4 rounded-sm'>
        <StructureModal structureId={structureId} />
      </Modal>
    </>
  );
}
