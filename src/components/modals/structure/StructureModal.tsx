import axios, { AxiosError } from 'axios';

import { Icons } from '@/components/Icons';
import ViewLevelsGrid from '@/components/structure/levels/ViewLevelsGrid';
import { Level } from '@/types';
import { useQuery } from '@tanstack/react-query';

export default function StructureModal({
  structureId,
}: {
  structureId: number;
}) {
  const { data, isPending, isError } = useQuery({
    queryKey: ['structure', structureId],
    queryFn: () => {
      if (!structureId) return null;
      return fetchStructures();
    },
  });

  if (isPending) {
    return (
      <div className='flex flex-grow items-center justify-center text-lg gap-2'>
        <Icons.spinner className='animate-spin h-8 w-8' />
        Loading structure...
      </div>
    );
  }

  if (isError) {
    return (
      <div className='flex flex-grow items-center justify-center text-lg'>
        Error fetching structure, please try again later
      </div>
    );
  }

  if (!data) {
    return (
      <div className='flex flex-grow items-center justify-center text-lg'>
        Structure not found, there might have been an error, please try again
        later
      </div>
    );
  }

  return (
    <>
      <h1 className='text-2xl'>Structure: {data.name}</h1>
      <ViewLevelsGrid data={data.levels} className='h-[30rem]' />
    </>
  );

  async function fetchStructures(): Promise<{
    id: number;
    name: string;
    levels: Level[];
  } | null> {
    try {
      const { data } = await axios.get<{
        id: number;
        name: string;
        levels: Level[];
      }>(`/api/structure/${structureId}`);
      return data;
    } catch (error) {
      if (!(error instanceof AxiosError)) throw error;
      if (error.response?.status === 404) return null;
      throw error;
    }
  }
}
