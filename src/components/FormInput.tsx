// 'use client';

// import { Input } from '@/components/ui/input';
// import { Label } from '@/components/ui/label';
// import { HTMLInputTypeAttribute } from 'react';
// import { UseFormRegister } from 'react-hook-form';

// type BaseFormInputProps = {
//   name: string;
//   label: string;
//   register: UseFormRegister<any>;
//   placeholder?: string;
//   error?: any;
//   required?: boolean;
//   id?: string;
//   classname?: string;
// };

// export function FormTextInput({
//   name,
//   label,
//   register,
//   error,
//   required = false,
//   placeholder,
//   id,
//   classname,
// }: BaseFormInputProps) {
//   return (
//     <div className={classname ?? 'flex flex-col gap-1'}>
//       <Label htmlFor={`${id ?? name}`}>
//         {label} {required && <span className='text-red-500'>*</span>}
//       </Label>
//       <Input
//         id='location-address'
//         {...register(name)}
//         placeholder={placeholder}
//         type='text'
//       />
//       {error && <span className='text-sm text-red-500 px-2'>{error}</span>}
//     </div>
//   );
// }

// type NumberFormInputProps = BaseFormInputProps & {
//   min?: number;
//   max?: number;
//   step?: number;
// };

// export function FormNumberInput({
//   name,
//   label,
//   register,
//   error,
//   required = false,
//   placeholder,
//   id,
//   classname,
//   min,
//   max,
//   step,
// }: NumberFormInputProps) {
//   return (
//     <div className={classname ?? 'flex flex-col gap-1'}>
//       <Label htmlFor={`${id ?? name}`}>
//         {label} {required && <span className='text-red-500'>*</span>}
//       </Label>
//       <Input
//         id='location-address'
//         {...register(name)}
//         placeholder={placeholder}
//         type='number'
//         min={min}
//         max={max}
//         step={step ?? 1}
//       />
//       {error && <span className='text-sm text-red-500 px-2'>{error}</span>}
//     </div>
//   );
// }

// export function FormCheckboxInput({
//   name,
//   label,
//   register,
//   error,
//   required = false,
//   id,
//   classname,
// }: BaseFormInputProps) {
//   return (
//     <div className={classname ?? 'flex flex-col gap-1'}>
//       <Label htmlFor={`${id ?? name}`}>
//         {label} {required && <span className='text-red-500'>*</span>}
//       </Label>
//       <Checkb
//         id='location-address'
//         {...register(name)}
//         type='checkbox'
//       />
//       {error && <span className='text-sm text-red-500 px-2'>{error}</span>}
//     </div>
//   );
// }
