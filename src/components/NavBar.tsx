'use client';

import Link from 'next/link';
import { signOut } from '@/actions/auth';
import { Button } from '@/components/ui/button';
import {
  NavigationMenu,
  NavigationMenuItem,
  NavigationMenuList,
  navigationMenuTriggerStyle,
} from '@/components/ui/navigation-menu';
import { Avatar, AvatarFallback, AvatarImage } from '@/components/ui/avatar';
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from '@/components/ui/dropdown-menu';
import { User, LogOut } from 'lucide-react';
import { User as LuciaUser } from 'lucia';

export default function NavBar({
  isLoggedIn,
  user,
}: {
  isLoggedIn: boolean;
  user: LuciaUser | null;
}) {
  return (
    <NavigationMenu className='flex-end'>
      <NavigationMenuList className='gap-6 sm:gap-4'>
        <NavigationMenuItem>
          <Link href='/tournaments' className={navigationMenuTriggerStyle()}>
            Tournaments
          </Link>
        </NavigationMenuItem>
        <NavigationMenuItem>
          <Link
            href='/equity-calculator'
            className={navigationMenuTriggerStyle()}>
            Equity Calculator
          </Link>
        </NavigationMenuItem>
        <NavigationMenuItem>
          <Link href='#' className={navigationMenuTriggerStyle()}>
            Hand Tracker
          </Link>
        </NavigationMenuItem>
        {isLoggedIn ? (
          <NavigationMenuItem>
            <DropdownMenu>
              <DropdownMenuTrigger asChild>
                <Button
                  variant='ghost'
                  className='relative h-10 w-10 rounded-full'>
                  <Avatar className='h-10 w-10'>
                    <AvatarImage
                      src={
                        user?.avatar
                          ? `${process.env.NEXT_PUBLIC_GCS_BUCKET_URL}${user?.avatar}`
                          : undefined
                      }
                      alt={`${user?.nickname}'s profile picture`}
                    />
                    <AvatarFallback>
                      {user?.nickname?.charAt(0) || 'U'}
                    </AvatarFallback>
                  </Avatar>
                </Button>
              </DropdownMenuTrigger>
              <DropdownMenuContent className='w-56' align='end' forceMount>
                <DropdownMenuLabel className='font-normal'>
                  <div className='flex flex-col space-y-1'>
                    <p className='text-sm font-medium leading-none'>
                      {user?.nickname || 'User'}
                    </p>
                    <p className='text-xs leading-none text-muted-foreground'>
                      {user?.email}
                    </p>
                  </div>
                </DropdownMenuLabel>
                <DropdownMenuSeparator />
                <DropdownMenuItem asChild>
                  <Link href='/profile'>
                    <User className='mr-2 h-4 w-4' />
                    <span>My Account</span>
                  </Link>
                </DropdownMenuItem>
                <DropdownMenuItem onClick={() => signOut()}>
                  <LogOut className='mr-2 h-4 w-4' />
                  <span>Log out</span>
                </DropdownMenuItem>
              </DropdownMenuContent>
            </DropdownMenu>
          </NavigationMenuItem>
        ) : (
          <>
            <NavigationMenuItem>
              <Link href='/auth/sign-in'>
                <Button variant='outline'>Sign In</Button>
              </Link>
            </NavigationMenuItem>
            <NavigationMenuItem>
              <Link href='/auth/sign-up'>
                <Button>Sign Up</Button>
              </Link>
            </NavigationMenuItem>
          </>
        )}
      </NavigationMenuList>
    </NavigationMenu>
  );
}
