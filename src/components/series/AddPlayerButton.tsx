'use client';

import { Button } from '@/components/ui/button';
import axios, { AxiosError, HttpStatusCode } from 'axios';
import { useRouter } from 'next/navigation';
import { useState } from 'react';
import { toast } from 'sonner';
import Modal from 'react-modal';
import { DynamicCombobox } from '@/components/ui/combobox';

interface SeriesAddPlayerButtonProps {
  seriesId: number;
}

export default function SeriesAddPlayerButton({
  seriesId,
}: SeriesAddPlayerButtonProps) {
  Modal.setAppElement('#main');
  const [adding, setAdding] = useState<boolean>(false);
  const [selectedPlayer, setSelectedPlayer] = useState<number | null>(null);
  const router = useRouter();
  return (
    <>
      <div>
        <Button onClick={() => setAdding(true)}>Add player</Button>
      </div>
      <Modal
        isOpen={adding}
        onRequestClose={() => setAdding(false)}
        overlayClassName='fixed flex justify-center items-center top-0 left-0 w-full h-full bg-black bg-opacity-50 z-50'
        className='flex flex-col text-center bg-white px-8 py-6 gap-4 rounded-sm'>
        <h1 className='text-xl'>Select the player you want to add</h1>
        <DynamicCombobox
          placeholder='Select a player'
          onSelect={(value) =>
            setSelectedPlayer(value ? parseInt(value) : null)
          }
          fetchItems={(query) => fetchPlayers(query)}
          value={selectedPlayer?.toString() || ''}
          emptyLabel='No players found'
        />
        <div className='flex justify-end'>
          <Button onClick={addPlayer} disabled={!selectedPlayer}>
            Add player
          </Button>
        </div>
      </Modal>
    </>
  );

  async function addPlayer() {
    setAdding(true);
    try {
      const { data, status } = await axios.post(
        `/api/series/${seriesId}/register`,
        { playerId: selectedPlayer }
      );
      if (status !== HttpStatusCode.Ok) {
        toast.error(
          'Could not add player to the series, please try again later'
        );
        return;
      }
      toast.success('Player has been added to the series');
      router.refresh();
    } catch (e) {
      if (e instanceof AxiosError) {
        console.error(e);
        toast.error(
          'Could not add player to the series, please try again later'
        );
        return;
      }
      console.error(e);
      toast.error('An unexpected error occurred, please try again later');
    } finally {
      setAdding(false);
      setSelectedPlayer(null);
    }
  }

  async function fetchPlayers(query: string) {
    const { data } = await axios.get<
      {
        id: number;
        nickname: string;
      }[]
    >(
      `/api/players?query=${query}&objectType=series&operaotr=notIn&objectId=${seriesId}`
    );
    return data.map((player) => ({
      value: player.id.toString(),
      label: player.nickname,
    }));
  }
}
