'use client';

import { Button } from '@/components/ui/button';
import axios, { AxiosError, HttpStatusCode } from 'axios';
import { useState } from 'react';
import { toast } from 'sonner';
import { Icons } from '@/components/Icons';
import { useRouter } from 'next/navigation';

interface SeriesRegisterButtonProps {
  seriesId: number;
  registered: boolean;
}

export default function SeriesRegisterButton({
  seriesId,
  registered,
}: SeriesRegisterButtonProps) {
  const [registering, setRegistering] = useState(false);
  const router = useRouter();

  return (
    <div>
      <Button onClick={registered ? unRegisterPlayer : registerPlayer}>
        {registering && <Icons.spinner />}&nbsp;
        {registered ? 'Unregister' : 'Register'}
      </Button>
    </div>
  );

  async function registerPlayer() {
    setRegistering(true);
    try {
      const { data, status } = await axios.post(
        `/api/series/${seriesId}/register`
      );
      if (status !== HttpStatusCode.Ok) {
        toast.error(
          'Could not register into the series, please try again later'
        );
        return;
      }
      toast.success('You have been registered into the tournament');
      router.refresh();
    } catch (e) {
      if (e instanceof AxiosError) {
        console.error(e);
        toast.error(
          'Could not register into the tournament, please try again later'
        );
        return;
      }
      console.error(e);
      toast.error('An unexpected error occurred, please try again later');
    } finally {
      setRegistering(false);
    }
  }

  async function unRegisterPlayer() {
    setRegistering(true);
    try {
      const { data, status } = await axios.post(
        `/api/series/${seriesId}/unregister`
      );
      if (status !== HttpStatusCode.Ok) {
        toast.error(
          'Could not unregister from the series, please try again later'
        );
      }
      toast.success('You have been unregistered from the series');
      router.refresh();
    } catch (e) {
      if (e instanceof AxiosError) {
        console.error(e);
        toast.error(
          'Could not unregister from the series, please try again later'
        );
        return;
      }
      console.error(e);
      toast.error('An unexpected error occurred, please try again later');
    } finally {
      setRegistering(false);
    }
  }
}
