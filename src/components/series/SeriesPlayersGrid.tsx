'use client';

import { ReadOnlyDataTable } from '@/components/ReadOnlyDataTable';
import { Checkbox } from '@/components/ui/checkbox';
import { SeriesPlayerForGrid } from '@/types';
import { ColumnDef } from '@tanstack/react-table';

interface SeriesPlayersGridProps {
  seriesPlayers: SeriesPlayerForGrid[];
}

const columns: ColumnDef<Partial<SeriesPlayerForGrid>>[] = [
  {
    accessorKey: 'nickname',
    header: 'Name',
    cell: ({ row }) => {
      return (
        <div className='flex items-center justify-center w-full h-full'>
          {row.getValue('nickname')}
        </div>
      );
    },
  },
  {
    accessorKey: 'mod',
    header: 'Moderator ?',
    cell: ({ row }) => {
      return (
        <div className='flex items-center justify-center w-full h-full'>
          <Checkbox disabled checked={row.getValue('mod')} />
        </div>
      );
    },
  },
];

export default function SeriesPlayersGrid({
  seriesPlayers,
}: SeriesPlayersGridProps) {
  return <ReadOnlyDataTable columns={columns} data={seriesPlayers} />;
}
