import Link from 'next/link';

import { getUser } from '@/lib/auth';

import NavBar from '@/components/NavBar';
import Image from 'next/image';

export default async function Header() {
  const user = await getUser();
  return (
    <header className='px-4 lg:px-6 h-14 flex items-center border-b border-gray-200 dark:border-gray-800 gap-4 justify-between'>
      <div className='flex gap-6'>
        <Link href='/' className='flex gap-4 items-center'>
          <Image
            alt='Logo, link to home page'
            src='/assets/opt-icon.webp'
            width={40}
            height={40}
          />
          Home
        </Link>
      </div>
      <NavBar isLoggedIn={!!user} user={user} />
    </header>
  );
}
