import { count, eq } from 'drizzle-orm';
import {
  boolean,
  coerce,
  literal,
  nativeEnum,
  number,
  object,
  string,
} from 'zod';

import { db } from '@/db';
import { structuresTable } from '@/db/schema';
import { TournamentVisibility } from '@/types';

export const signInSchema = object({
  email: string({ message: 'Email or username is required' }).min(
    1,
    'Email or username is required'
  ),
  password: string({ message: 'Password is required' })
    .min(1, 'Password is required')
    .min(8, 'Password must be more than 8 characters'),
});

export const signUpSchema = object({
  email: string({ message: 'Email is required' })
    .min(1, 'Email is required')
    .email('Invalid email'),
  nickname: string({ message: 'Nickname is required' })
    .min(3, 'Nickname must be more than 3 characters')
    .max(32, 'Nickname must be less than 32 characters')
    .regex(
      /^[a-zA-Z0-9_-]*$/,
      'Nickname must contain only letters, numbers, underscores and hyphens'
    ),
  password: string({ message: 'Password is required' })
    .min(1, 'Password is required')
    .min(8, 'Password must be more than 8 characters'),
  confirmPassword: string({ message: 'Confirm password is required' }),
}).refine((data) => data.password === data.confirmPassword, {
  message: 'Passwords must match',
  path: ['confirmPassword'], // path of error
});

export const createStructureSchema = object({
  name: string({ message: 'Structure name is required' })
    .min(1, 'Name is required')
    .refine(
      async (structureName) => {
        const [query] = await db
          .select({ count: count() })
          .from(structuresTable)
          .where(eq(structuresTable.name, structureName))
          .execute();
        return query.count === 0;
      },
      {
        message: 'A structure with that name already exists',
        path: ['name'],
      }
    ),
  levels: object({
    sb: number().int({ message: 'SB must be an integer' }),
    bb: number().int({ message: 'BB must be an integer' }),
    ante: number()
      .int({ message: 'Ante must be an integer' })
      .min(0, 'Ante cannot be less than 0'),
    duration: number()
      .int({ message: 'Duration must be an integer' })
      .min(1, 'Duration cannot be less than 1 minute'),
    isBreak: boolean(),
  })
    .refine(
      (level) => {
        if (!level.isBreak) {
          return level.sb > 0 && level.bb > 0 && level.duration > 0;
        }
        return true;
      },
      {
        message: 'SB, BB and duration must be greater than 0',
      }
    )
    .array(),
});

export const createLocationSchema = object({
  name: string().min(1, 'Name is required'),
  address: string().min(1, 'Address is required'),
  city: string().optional(),
  zip: string().optional(),
});

export const createSeriesSchema = object({
  name: string({ message: 'Series name is required' }).min(
    1,
    'Name is required'
  ),
  locationId: number().nullable().optional(),
});

export const createTournamentSchema = object({
  name: string({ message: 'Tournament name is required' }).min(
    1,
    'Name is required'
  ),
  visibility: nativeEnum(TournamentVisibility, {
    message: 'Visibility is required',
  })
    .optional()
    .default(TournamentVisibility.NotListed),
  date: coerce.date({ message: 'Date is required' }),
  description: string().optional(),
  buyIn: coerce.number().min(0, 'Buy-in cannot be less than 0'),
  knockout: boolean().optional().default(false),
  progressiveKo: coerce.boolean().optional().default(false),
  knockoutBounty: coerce.number().min(0, 'Bounty cannot be less than 0'),
  canReentry: boolean().optional().default(false),
  reentryEnd: coerce
    .number()
    .min(0, 'Reentry end cannot be less than 0')
    .optional(),
  canLateReg: boolean().optional().default(false),
  lateRegEnd: coerce
    .number()
    .min(0, 'Late reg end cannot be less than 0')
    .optional(),
  tableSize: coerce.number().min(2, 'Table size cannot be less than 2'),
  minTableSize: coerce.number().min(2, 'Min table size cannot be less than 2'),
  maxPlayers: coerce
    .number()
    .min(2, 'Max players cannot be less than 2')
    .or(literal(0)),
  variant: string().optional().default('NLHE'),
  startingStack: coerce.number().min(0, 'Starting stack cannot be less than 0'),
  finalTableSize: coerce
    .number()
    .min(2, 'Final table size cannot be less than 2'),
  seriesId: coerce.number().nullable().optional(),
  structureId: coerce.number().nullable().optional(),
  locationId: coerce.number().nullable().optional(),
})
  .transform((data) => ({
    ...data,
    progressiveKo: data.knockout ? data.progressiveKo : false,
    reentryEnd: data.canReentry ? data.reentryEnd : null,
    lateRegEnd: data.canLateReg ? data.lateRegEnd : null,
    knockoutBounty: data.knockout ? data.knockoutBounty : 0,
    locationId: data.seriesId ? null : data.locationId, // if seriesId is set, locationId is taken from series, so nulled in the tournament
  }))
  .refine((data) => !data.knockout || (data.knockoutBounty ?? 0) > 0, {
    message: 'Bounty must be greater than 0',
    path: ['knockoutBounty'],
  })
  .refine((data) => !data.knockout || data.buyIn >= data.knockoutBounty, {
    message: 'Bounty cannot be higher than the buy-in',
    path: ['knockoutBounty'],
  })
  .refine((data) => !data.canReentry || (data.reentryEnd ?? 0) > 0, {
    message: 'Reentry end must be greater than 0',
    path: ['reentryEnd'],
  })
  .refine((data) => !data.canLateReg || (data.lateRegEnd ?? 0) > 0, {
    message: 'Late reg end must be greater than 0',
    path: ['lateRegEnd'],
  })
  .refine((data) => !data.knockout || data.buyIn > 0, {
    message: 'Buy-in must be higher than 0 for knockout tournaments',
    path: ['buyIn'],
  })
  .refine((data) => data.tableSize >= data?.minTableSize, {
    message: 'Table size must be greater than or equal to min table size',
    path: ['tableSize'],
  })
  .refine(
    (data) => data.maxPlayers === 0 || data.maxPlayers >= data.tableSize,
    {
      message: 'Max players must be greater than or equal to table size',
      path: ['maxPlayers'],
    }
  )
  .refine((data) => data.finalTableSize >= data.tableSize, {
    message: 'Final table size must be greater than or equal to table size',
    path: ['finalTableSize'],
  })
  .refine(
    (data) => data.maxPlayers === 0 || data.maxPlayers >= data.finalTableSize,
    {
      message: 'Max players must be greater than or equal to final table size',
      path: ['maxPlayers'],
    }
  )
  .refine((data) => data.startingStack > 0, {
    message: 'Starting stack must be greater than 0',
    path: ['startingStack'],
  });
