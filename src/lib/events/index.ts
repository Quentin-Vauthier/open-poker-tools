import { and, eq } from 'drizzle-orm';

import { db } from '@/db';
import {
  notificationEndpointsTable,
  notificationsTable,
  notificationSubscriptionsTable,
} from '@/db/schema';
import EmailNotifier from '@/lib/events/notifiers/EmailNotifier';
import {
  EventArgs,
  NotificationData,
  Notifier,
  OPTEndpointMethod,
  OPTEvent,
} from '@/lib/events/types';

import DiscordNotifier from '@lib/events/notifiers/DiscordNotifier';

export default class EventManager {
  private static instance: EventManager;

  private constructor() {
    // Private to prevent direct instantiation
  }

  // Private method to provide a single global instance
  private static getInstance(): EventManager {
    if (!EventManager.instance) {
      EventManager.instance = new EventManager();
    }
    return EventManager.instance;
  }

  // Static method named trigger taking 3 arguments
  public static trigger(event: OPTEvent, data: EventArgs): void {
    const instance = EventManager.getInstance();
    instance.triggerInstance(event, data);
  }

  // Instance method to handle the trigger logic
  private async triggerInstance(
    event: OPTEvent,
    args: EventArgs
  ): Promise<void> {
    const [notifications, endpoints] = await Promise.all([
      db
        .select({
          code: notificationsTable.code,
          subject: notificationsTable.subject,
          message: notificationsTable.message,
          html: notificationsTable.html,
          color: notificationsTable.color,
        })
        .from(notificationsTable)
        .where(eq(notificationsTable.eventCode, event)),
      db
        .select({
          notifCode: notificationsTable.code,
          method: notificationEndpointsTable.method,
          endpoint: notificationEndpointsTable.endpoint,
        })
        .from(notificationSubscriptionsTable)
        .innerJoin(
          notificationsTable,
          eq(
            notificationSubscriptionsTable.notificationCode,
            notificationsTable.code
          )
        )
        .innerJoin(
          notificationEndpointsTable,
          eq(
            notificationSubscriptionsTable.endpointId,
            notificationEndpointsTable.id
          )
        )
        .where(
          and(
            eq(notificationsTable.eventCode, event),
            eq(notificationEndpointsTable.active, true),
            eq(notificationSubscriptionsTable.active, true),
            event.startsWith('g_')
              ? eq(notificationSubscriptionsTable.objectId, '')
              : eq(notificationSubscriptionsTable.objectId, args.objectId)
          )
        ),
    ]);

    if (!notifications.length) {
      // TODO: Logger
      console.warn('No notification found for event', event);
      return;
    }

    if (!endpoints.length) return;

    const endpointsByMethodAndNotif = endpoints.reduce((acc, endpoint) => {
      // Create the top-level group for method if it doesn't exist
      if (!acc[endpoint.method]) {
        acc[endpoint.method] = {};
      }
      // Create the sub-group for notifCode if it doesn't exist
      if (!acc[endpoint.method][endpoint.notifCode]) {
        acc[endpoint.method][endpoint.notifCode] = [endpoint];
      } else {
        // Add the endpoint to the respective group
        acc[endpoint.method][endpoint.notifCode].push(endpoint);
      }
      return acc;
    }, {} as Record<OPTEndpointMethod, Record<string, typeof endpoints>>);

    const notificationMap = new Map<string, NotificationData>();
    for (const notif of notifications) {
      // Replace placeholders
      notif.subject = replacePlaceholders(notif.subject, args);
      notif.message = replacePlaceholders(notif.message, args);
      notif.html = replacePlaceholders(notif.html, args);
      notificationMap.set(notif.code, notif);
    }

    const endpointsMethods = Object.keys(
      endpointsByMethodAndNotif
    ) as OPTEndpointMethod[];
    for (const method of endpointsMethods) {
      try {
        const notifier = getNotifier(method);
        const endpointsByNotif = endpointsByMethodAndNotif[method]!;
        for (const notifCode in endpointsByNotif) {
          const notif = notificationMap.get(notifCode)!;
          await notifier.sendNotifications(
            notif,
            endpointsByNotif[notifCode].map((e) => e.endpoint)
          );
        }
      } catch (error) {
        // TODO: Logger
        console.error(
          `Error sending notifications for method ${method}`,
          error
        );
      }
    }
  }
}

function replacePlaceholders(message: string, args: EventArgs): string {
  return message.replace(/{{([^{}]*)}}/g, (_match, key) => args[key] || key);
}

function getNotifier(method: OPTEndpointMethod): typeof Notifier {
  switch (method) {
    case OPTEndpointMethod.EMAIL:
      return EmailNotifier;
    case OPTEndpointMethod.DISCORD_DM:
      return DiscordNotifier;
    default:
      throw new Error(`Unsupported notifier method: ${method}`);
  }
}
