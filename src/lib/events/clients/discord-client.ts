import { REST } from '@discordjs/rest';
import {
  RESTPostAPIChannelMessageResult,
  Routes,
  type APIEmbed,
  type RESTPostAPICurrentUserCreateDMChannelResult,
} from 'discord-api-types/v10';

export default class DiscordClient {
  private rest: REST;

  constructor() {
    if (!process.env.DISCORD_TOKEN)
      throw new Error('DISCORD_TOKEN is not defined');
    this.rest = new REST({ version: '10' }).setToken(process.env.DISCORD_TOKEN);
  }

  async createDM(
    userId: string
  ): Promise<RESTPostAPICurrentUserCreateDMChannelResult> {
    return this.rest.post(Routes.userChannels(), {
      body: {
        recipient_id: userId,
      },
    }) as Promise<RESTPostAPICurrentUserCreateDMChannelResult>;
  }

  async sendDM(
    userId: string,
    content: string
  ): Promise<RESTPostAPIChannelMessageResult> {
    const dm = await this.createDM(userId);
    if (!dm) throw new Error('Failed to create DM channel');
    return this.rest.post(Routes.channelMessages(dm.id), {
      body: {
        content,
      },
    }) as Promise<RESTPostAPIChannelMessageResult>;
  }

  async sendEmbed(
    userId: string,
    embed: APIEmbed
  ): Promise<RESTPostAPIChannelMessageResult> {
    const dm = await this.createDM(userId);
    if (!dm) throw new Error('Failed to create DM channel');
    return this.rest.post(Routes.channelMessages(dm.id), {
      body: {
        embeds: [embed],
      },
    }) as Promise<RESTPostAPIChannelMessageResult>;
  }
}
