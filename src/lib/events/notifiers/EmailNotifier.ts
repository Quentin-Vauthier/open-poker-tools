import { Resend } from 'resend';

import { NotificationData, Notifier } from '@/lib/events/types';
import { sanitizeInput } from '@/lib/utils';

export default abstract class EmailNotifier extends Notifier {
  private static resend: Resend | null = null;

  public static async sendNotifications(
    notification: NotificationData,
    recipents: string[]
  ): Promise<void> {
    if (!this.resend) {
      if (!process.env.RESEND_APIKEY) {
        return console.error('RESEND_APIKEY is not set');
      }
      this.resend = new Resend(process.env.RESEND_APIKEY);
    }
    const emails = recipents.map((recipent) => ({
      from: 'Open Poker Tools <opt@vthr.fr>',
      to: recipent,
      subject: notification.subject,
      html: sanitizeInput(notification.html),
    }));
    const { error } = await this.resend.batch.send(emails);
    if (error) {
      // TODO: Logger
      console.error('Error sending email', error);
    }
  }
}
