import DiscordClient from '@/lib/events/clients/discord-client';
import { NotificationData, Notifier } from '@/lib/events/types';

export default class DiscordNotifier extends Notifier {
  public static async sendNotifications(
    notification: NotificationData,
    userIds: string[]
  ): Promise<void> {
    try {
      const client = new DiscordClient();
      for (const userId of userIds) {
        client
          .sendEmbed(userId, {
            title: notification.subject,
            description: notification.message,
            color: notification.color
              ? hexToDecimal(
                  notification.color.startsWith('#')
                    ? notification.color.slice(1)
                    : notification.color
                )
              : undefined,
            author: {
              name: 'Open Poker Tools',
              url: 'https://open-poker-tools.fr',
            },
          })
          .catch((error) => {
            // TODO: Logger
            console.error('Error sending Discord DM', error);
          });
      }
    } catch (error) {
      // Can throw if Discord token is not set
      // TODO: Logger
      console.error('Error sending Discord DM', error);
    }
  }
}

function hexToDecimal(hex: string): number {
  return parseInt(hex, 16);
}
