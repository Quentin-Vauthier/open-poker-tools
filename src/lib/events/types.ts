export enum OPTEvent {
  // Tournament events (tournament)
  GLOBAL_TOURNAMENT_WIN = 'g_tournament_win',
  GLOBAL_TOURNAMENT_CREATE = 'g_tournament_create',
  TOURNAMENT_START = 'tournament_start',
  TOURNAMENT_END = 'tournament_end',

  // Player events (player)
  GLOBAL_PLAYER_CREATED = 'g_player_created',
  GLOBAL_PLAYER_DELETED = 'g_player_deleted',
  PLAYER_JOIN_TOURNAMENT = 'player_join_tournament',
  PLAYER_INVITED_TO_TOURNAMENT = 'player_invited_to_tournament',
  PLAYER_LEAVE_TOURNAMENT = 'player_leave_tournament',
  PLAYER_WIN_TOURNAMENT = 'player_win_tournament',
  PLAYER_ELIMINATED_FROM_TOURNAMENT = 'player_eliminated_from_tournament',

  // Tournament player events (tournament_player)
  TOURNAMENT_PLAYER_WIN = 'tournament_player_win',
  TOURNAMENT_PLAYER_ELIMINATED = 'tournament_player_eliminated',
  TOURNAMENT_PLAYER_WAITLIST_UPDATE = 'tournament_player_waitlist_update',

  // User events (user)
  GLOBAL_USER_CREATED = 'g_user_created',
  GLOBAL_USER_DELETED = 'g_user_deleted',
}

export enum OPTEndpointMethod {
  EMAIL = 'email',
  DISCORD_DM = 'discord_dm',
}

export type NotificationData = {
  code: string;
  subject: string;
  message: string;
  html: string;
  color: string | null;
};

export type EventArgs = {
  objectId: any;
} & Record<string, any>;

export abstract class Notifier {
  static sendNotifications: (
    notification: NotificationData,
    endpoints: string[]
  ) => Promise<void>;
}
