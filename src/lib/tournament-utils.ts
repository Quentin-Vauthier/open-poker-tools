import { db } from '@/db';
import {
  playersTable,
  tournamentInvitationsTable,
  tournamentPlayersTable,
  tournamentsTable,
} from '@/db/schema';
import { Role, TournamentVisibility } from '@/types';
import { and, eq, sql } from 'drizzle-orm';

export const isTournamentAccessible = async (tournamentId: number) => {
  if (!tournamentId) return false;
  const [tournament] = await db
    .select({
      visibility: tournamentsTable.visibility,
    })
    .from(tournamentsTable)
    .where(eq(tournamentsTable.id, tournamentId));
  if (!tournament) return false;
  return (
    tournament.visibility === TournamentVisibility.Public ||
    tournament.visibility === TournamentVisibility.NotListed
  );
};

export const playerHasAccessToTournament = async (
  playerId: number | undefined,
  tournamentId: number
): Promise<boolean> => {
  if (!tournamentId) return false;
  if (!playerId) return await isTournamentAccessible(tournamentId);
  const [tournamentPlayer] = await db
    .select({
      tournamentId: tournamentsTable.id,
      visibility: tournamentsTable.visibility,
      playerId: tournamentPlayersTable.playerId,
      invitationPlayerId: tournamentInvitationsTable.playerId,
      role: playersTable.role,
    })
    .from(tournamentsTable)
    .leftJoin(
      tournamentPlayersTable,
      and(
        eq(tournamentsTable.id, tournamentPlayersTable.tournamentId),
        eq(tournamentPlayersTable.playerId, playerId)
      )
    )
    .leftJoin(
      tournamentInvitationsTable,
      and(
        eq(tournamentsTable.id, tournamentInvitationsTable.tournamentId),
        eq(tournamentInvitationsTable.playerId, playerId)
      )
    )
    .leftJoin(
      playersTable,
      eq(
        sql`COALESCE(${tournamentPlayersTable.playerId}, ${tournamentInvitationsTable.playerId})`,
        playersTable.id
      )
    )
    .where(eq(tournamentsTable.id, tournamentId));

  if (tournamentPlayer.visibility === TournamentVisibility.Public) return true;
  if (tournamentPlayer.visibility === TournamentVisibility.NotListed)
    return true;

  if (tournamentPlayer.role === Role.Admin) return true;
  // If the tournament in private, check if the player is registered or invited
  return (
    tournamentPlayer.playerId === playerId ||
    tournamentPlayer.invitationPlayerId === playerId
  );
};

export const playerIsTournamentMod = async (
  playerId: number,
  tournamentId: number
): Promise<boolean> => {
  if (!playerId || !tournamentId) return false;
  const [tournamentPlayer] = await db
    .select({
      isMod: tournamentPlayersTable.isModerator,
      role: playersTable.role,
    })
    .from(playersTable)
    .leftJoin(
      tournamentPlayersTable,
      and(
        eq(tournamentPlayersTable.playerId, playersTable.id),
        eq(tournamentPlayersTable.tournamentId, tournamentId)
      )
    )
    .where(eq(tournamentPlayersTable.playerId, playerId));
  return (
    (tournamentPlayer?.role === Role.Admin || tournamentPlayer?.isMod) ?? false
  );
};

export const playerIsRegisteredInTournament = async (
  playerId: number,
  tournamentId: number
): Promise<boolean> => {
  const [tournamentPlayer] = await db
    .select({
      playerId: tournamentPlayersTable.playerId,
    })
    .from(tournamentPlayersTable)
    .where(
      and(
        eq(tournamentPlayersTable.tournamentId, tournamentId),
        eq(tournamentPlayersTable.playerId, playerId ?? -1)
      )
    );
  return !!tournamentPlayer;
};

export const playerIsInvitedToTournament = async (
  playerId: number,
  tournamentId: number
): Promise<boolean> => {
  const [tournamentInvitation] = await db
    .select({
      playerId: tournamentInvitationsTable.playerId,
    })
    .from(tournamentInvitationsTable)
    .where(
      and(
        eq(tournamentInvitationsTable.tournamentId, tournamentId),
        eq(tournamentInvitationsTable.playerId, playerId ?? -1)
      )
    );
  return !!tournamentInvitation;
};

export const playerIsRegisteredOrInvitedToTournament = async (
  playerId: number,
  tournamentId: number
): Promise<boolean> => {
  const [tournamentPlayer] = await db
    .select({
      playerId: tournamentPlayersTable.playerId,
      invitationPlayerId: tournamentInvitationsTable.playerId,
    })
    .from(tournamentsTable)
    .leftJoin(
      tournamentPlayersTable,
      and(
        eq(tournamentsTable.id, tournamentPlayersTable.tournamentId),
        eq(tournamentPlayersTable.playerId, playerId ?? -1)
      )
    )
    .leftJoin(
      tournamentInvitationsTable,
      and(
        eq(
          tournamentPlayersTable.tournamentId,
          tournamentInvitationsTable.tournamentId
        ),
        eq(tournamentInvitationsTable.playerId, playerId ?? -1)
      )
    )
    .where(eq(tournamentPlayersTable.tournamentId, tournamentId));
  return (
    tournamentPlayer?.playerId === playerId ||
    tournamentPlayer?.invitationPlayerId === playerId
  );
};
