import { type ClassValue, clsx } from 'clsx';
import sanitizeHtml from 'sanitize-html';
import { twMerge } from 'tailwind-merge';
import { z } from 'zod';

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs));
}

export const debounce = <F extends (...args: any[]) => any>(
  func: F,
  waitFor: number
) => {
  let timeout: ReturnType<typeof setTimeout> | null = null;

  const debounced = (...args: Parameters<F>) => {
    if (timeout !== null) {
      clearTimeout(timeout);
      timeout = null;
    }
    timeout = setTimeout(() => func(...args), waitFor);
  };

  return debounced as (...args: Parameters<F>) => ReturnType<F>;
};

export const throttle = <F extends (...args: any[]) => any>(
  fn: F,
  wait: number = 300
) => {
  let inThrottle: boolean = false;
  let lastFn: ReturnType<typeof setTimeout>;
  let lastTime: number = 0;

  const throttled = (...args: Parameters<F>) => {
    const context = this;
    if (!inThrottle) {
      fn.apply(context, args);
      lastTime = Date.now();
      inThrottle = true;
    } else {
      clearTimeout(lastFn);
      lastFn = setTimeout(() => {
        if (Date.now() - lastTime >= wait) {
          fn.apply(context, args);
          lastTime = Date.now();
        }
      }, Math.max(wait - (Date.now() - lastTime), 0));
    }
  };

  return throttled as (...args: Parameters<F>) => ReturnType<F>;
};

export const formatTime = (ms: number): string => {
  const seconds = Math.floor(ms / 1000);
  if (seconds === Infinity) return '∞';
  const hours = Math.floor(seconds / 3_600)
    .toString()
    .padStart(2, '0');
  const minutes = Math.floor((seconds % 3_600) / 60)
    .toString()
    .padStart(2, '0');
  const sec = Math.floor(seconds % 60)
    .toString()
    .padStart(2, '0');

  if (hours === '00') return `${minutes}:${sec}`;
  return `${hours}:${minutes}:${sec}`;
};

export function enumToPgEnum<T extends Record<string, any>>(
  myEnum: T
): [T[keyof T], ...T[keyof T][]] {
  return Object.values(myEnum).map((value: any) => `${value}`) as any;
}

export const sanitizeInput = (dirtyHtml: string) => {
  return sanitizeHtml(dirtyHtml, {
    allowedTags: [
      'b',
      'i',
      'em',
      'strong',
      'a',
      'p',
      'ul',
      'ol',
      'li',
      'br',
      'h1',
      'h2',
      'h3',
      'h4',
      'h5',
      'h6',
      'blockquote',
      'code',
      'pre',
      'span',
      'div',
      'img',
    ],
    allowedAttributes: {
      a: ['href', 'title', 'target'],
      img: ['src', 'alt', 'title'],
      '*': ['style'], // Allow basic inline styles
    },
    allowedSchemes: ['http', 'https', 'mailto'],
  });
};

export function formatZodError(err: z.ZodError) {
  return err.errors.map((error) => `${error.path} -> ${error.message}`);
}

export function formatSearchParamsForParsing(rawSearchParams: URLSearchParams) {
  return Object.fromEntries(Array.from(rawSearchParams.entries()));
}

export enum ConsoleColor {
  Reset = '\x1b[0m',
  Bright = '\x1b[1m',
  Dim = '\x1b[2m',
  Underscore = '\x1b[4m',
  Blink = '\x1b[5m',
  Reverse = '\x1b[7m',
  Hidden = '\x1b[8m',

  FgBlack = '\x1b[30m',
  FgRed = '\x1b[31m',
  FgGreen = '\x1b[32m',
  FgYellow = '\x1b[33m',
  FgBlue = '\x1b[34m',
  FgMagenta = '\x1b[35m',
  FgCyan = '\x1b[36m',
  FgWhite = '\x1b[37m',
  FgGray = '\x1b[90m',

  BgBlack = '\x1b[40m',
  BgRed = '\x1b[41m',
  BgGreen = '\x1b[42m',
  BgYellow = '\x1b[43m',
  BgBlue = '\x1b[44m',
  BgMagenta = '\x1b[45m',
  BgCyan = '\x1b[46m',
  BgWhite = '\x1b[47m',
  BgGray = '\x1b[100m',
}
function colorize(
  color: ConsoleColor,
  message: string,
  fn: typeof console.log | typeof console.error | typeof console.warn
) {
  fn(`${color}${message}${ConsoleColor.Reset}`);
}
export const colorLog = (color: ConsoleColor, message: string) =>
  colorize(color, message, console.log);
export const colorError = (color: ConsoleColor, message: string) =>
  colorize(color, message, console.error);
export const colorWarn = (color: ConsoleColor, message: string) =>
  colorize(color, message, console.warn);
