export enum CardRank {
  None = 0,
  Two = 2,
  Three = 3,
  Four = 4,
  Five = 5,
  Six = 6,
  Seven = 7,
  Eight = 8,
  Nine = 9,
  Ten = 10,
  Jack = 11,
  Queen = 12,
  King = 13,
  Ace = 14,
}

export enum CardSuit {
  None = 0,
  Spades = 1,
  Hearts = 2,
  Diamonds = 3,
  Clubs = 4,
}

export enum HandType {
  HighCard = 0,
  Pair = 1,
  TwoPairs = 2,
  ThreeOfAKind = 3,
  Straight = 4,
  Flush = 5,
  FullHouse = 6,
  FourOfAKind = 7,
  StraightFlush = 8,
}

export type HandRank = {
  rank: HandType;
  main: CardRank;
  secondaries?: CardRank[];
};

export class Card {
  rank: CardRank;
  suit: CardSuit;

  static unknown = new Card(CardRank.None, CardSuit.None);

  private static shortRank = {
    [CardRank.None]: '?',
    [CardRank.Two]: '2',
    [CardRank.Three]: '3',
    [CardRank.Four]: '4',
    [CardRank.Five]: '5',
    [CardRank.Six]: '6',
    [CardRank.Seven]: '7',
    [CardRank.Eight]: '8',
    [CardRank.Nine]: '9',
    [CardRank.Ten]: 'T',
    [CardRank.Jack]: 'J',
    [CardRank.Queen]: 'Q',
    [CardRank.King]: 'K',
    [CardRank.Ace]: 'A',
  } as const;

  constructor(rank: CardRank, suit: CardSuit) {
    this.rank = rank;
    this.suit = suit;
  }

  isUnknown() {
    return this.rank === CardRank.None && this.suit === CardSuit.None;
  }

  toShortString() {
    return this.toString(true);
  }

  toString(short: boolean = false) {
    if (!short) return `${CardRank[this.rank]} of ${CardSuit[this.suit]}`;
    return `${Card.shortRank[this.rank]}${CardSuit[this.suit][0]}`;
  }

  equals(card: Card) {
    return this.rank === card.rank && this.suit === card.suit;
  }

  static fromJSON(json: any) {
    return new Card(json.rank, json.suit);
  }
}

export type Hand = [Card, Card];
export type Cards = Card[];

export enum Position {
  UTG = 'UTG',
  UTG1 = 'UTG +1',
  UTG2 = 'UTG +2',
  Button = 'Button',
  LowJack = 'Low Jack',
  HighJack = 'High Jack',
  CutOff = 'CutOff',
  SmallBlind = 'Small Blind',
  BigBlind = 'Big Blind',
}

export enum Action {
  Fold = 'Fold',
  Call = 'Call',
  Raise = 'Raise',
  Check = 'Check',
  Bet = 'Bet',
}

export type OddResult = {
  win: number;
  tie: number;
  description?: string;
};

/**
 * Odds of winning and tying for one or more hands
 */
export class Odds {
  odds: Map<Hand, OddResult>;

  /**
   * Constructor, accepting an odds map to initialize the object with or creating an empty map
   * @param odds the odds to initialize the object with
   */
  constructor(odds?: Map<Hand, OddResult>) {
    this.odds = odds ?? new Map();
  }

  /**
   * Set the odds for a hand
   * @param hand The hand to set the odds for
   * @param result The odds for the hand
   */
  set(hand: Hand, result: OddResult) {
    this.odds.set(hand, result);
  }

  /**
   * Get the odds for a hand
   * @param hand The hand to get the odds for
   * @returns the odds for the hand
   */
  get(hand: Hand) {
    return this.odds.get(hand);
  }

  /**
   * Iterate over the odds
   * @param callback the callback to call for each hand and its odds
   */
  forEach(callback: (result: OddResult, hand: Hand) => void) {
    this.odds.forEach(callback);
  }

  /**
   * Get the number of hands
   */
  get size() {
    return this.odds.size;
  }

  /**
   * Get the hands
   */
  get hands() {
    return Array.from(this.odds.keys());
  }

  /**
   * Get the results
   */
  get results() {
    return Array.from(this.odds.values());
  }

  /**
   * Convert the odds to a readable string
   */
  toString() {
    return Array.from(this.odds.entries())
      .map(([hand, result]) => {
        return `${hand.map((c) => c.toShortString()).join(', ')}: win=${(
          result.win * 100
        ).toFixed(2)}%, tie=${(result.tie * 100).toFixed(2)}%${
          result.description ? `  |  ${result.description}` : ''
        }`;
      })
      .join('\n');
  }
}

/**
 * Returns a full deck of cards
 * @returns a full deck of cards
 */
export function getFullDeck(): Cards {
  const deck: Cards = [];
  Object.values(CardRank).forEach((rank) => {
    if (rank === CardRank.None) return;
    if (typeof rank !== 'number') return;
    Object.values(CardSuit).forEach((suit) => {
      if (suit === CardSuit.None) return;
      if (typeof suit !== 'number') return;
      deck.push(new Card(rank, suit));
    });
  });
  return deck;
}

/**
 * Shuffles a deck of cards
 * @param deck the deck to shuffle
 * @returns the shuffled deck (copy), the original deck is not modified
 */
export function shuffleDeck(
  deck: Cards,
  increasedRandomness: boolean = false
): Cards {
  if (!increasedRandomness) return [...deck].sort(() => Math.random() - 0.5);
  const shuffledOrder = crypto.getRandomValues(new Uint32Array(deck.length));
  const shuffledDeck = deck.map((card, index) => ({
    card,
    order: shuffledOrder[index],
  }));
  shuffledDeck.sort((a, b) => a.order - b.order);
  return shuffledDeck.map((shuffled) => shuffled.card);
}
