import { hash, verify } from '@node-rs/argon2';

export async function hashPassword(password: string): Promise<string> {
  return await hash(password, {
    // recommended minimum parameters
    memoryCost: 19456,
    timeCost: 10,
    outputLen: 64,
    parallelism: 1,
  });
}

export async function comparePasswords(
  password: string,
  hash: string
): Promise<boolean> {
  return await verify(hash, password, {
    memoryCost: 19456,
    timeCost: 10,
    outputLen: 64,
    parallelism: 1,
  });
}
