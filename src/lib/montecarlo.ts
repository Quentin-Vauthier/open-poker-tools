import {
  type Hand,
  type Cards,
  type HandRank,
  Odds,
  CardSuit,
  CardRank,
  HandType,
} from '@/lib/poker-utils';
import { getFullDeck, shuffleDeck } from '@/lib/poker-utils';

/**
 * Simulates a given number of games and returns the odds of winning/tie for each hand
 * @param hand1 The first hand to compare
 * @param hand2 The second hand to compare
 * @param board The current board
 * @param deck The current deck
 * @param simulations Number of simulations to run
 * @param progressCallback Callback function to call with the progress of the simulation (updates every 1%)
 * @returns Odds of winning/tie for each hand
 */
export async function monteCarloSimulation(
  hand1: Hand,
  hand2: Hand,
  board: Cards | null = [],
  deck: Cards | null = getFullDeck(),
  simulations = 10_000,
  progressCallback: (progress: number) => void = () => {}
): Promise<Odds> {
  // Initialize variables if null
  board ??= [];
  deck ??= getFullDeck();

  const remainingDeck = deck.filter(
    (card) =>
      !hand1
        .concat(hand2)
        .concat(board)
        .some((c) => c.equals(card))
  );

  let wins = 0;
  let ties = 0;
  let losses = 0;
  let progress = 0;
  function simulateGame(board: Cards): void {
    ++progress;
    const shuffledDeck = shuffleDeck(remainingDeck, true);
    const draw = shuffledDeck.slice(0, 5 - board.length);
    const hand1Rank = evaluateHand(hand1.concat(board).concat(draw));
    const hand2Rank = evaluateHand(hand2.concat(board).concat(draw));
    const result = compareHandRanks(hand1Rank, hand2Rank);
    if (result > 0) ++wins;
    else if (result === 0) ++ties;
    else ++losses;
  }

  const promises = progressCallback
    ? new Array(simulations).fill(null).map(async () => {
        simulateGame(board);
        if (progress % Math.floor(simulations / 100) === 0)
          progressCallback(progress / simulations);
      })
    : new Array(simulations).fill(null).map(() => simulateGame(board));

  await Promise.all(promises);

  const odds: Odds = new Odds();

  odds.set(hand1, {
    win: wins / simulations,
    tie: ties / simulations,
  });
  odds.set(hand2, {
    win: losses / simulations,
    tie: ties / simulations,
  });
  return odds;
}

export function evaluateHand(hand: Cards): HandRank {
  const suits = new Map<CardSuit, number>();
  const ranks = new Map<CardRank, number>();
  for (const card of hand) {
    suits.set(card.suit, (suits.get(card.suit) || 0) + 1);
    ranks.set(card.rank, (ranks.get(card.rank) || 0) + 1);
  }
  const isFlush = Array.from(suits.values()).some((count) => count >= 5);
  if (isFlush) {
    // Check for straight flush
    const flushCards = hand.filter((card) => (suits.get(card.suit) ?? 0) >= 5);
    const straightFlush = checkStraight(flushCards);
    if (straightFlush) {
      return {
        rank: HandType.StraightFlush,
        main: straightFlush[0].rank,
      };
    }
    return {
      rank: HandType.Flush,
      main: Math.max(...flushCards.map((card) => card.rank)),
      secondaries: flushCards
        .map((card) => card.rank)
        .sort((a, b) => b - a)
        .slice(1),
    };
  }
  const straight = checkStraight(hand);
  if (straight) {
    return {
      rank: HandType.Straight,
      main: straight[0].rank,
    };
  }

  const rankCounts = Array.from(ranks.values());
  if (rankCounts.includes(4)) {
    let fourOfAKindRank = CardRank.None;
    ranks.forEach((count, rank) => {
      if (count === 4) fourOfAKindRank = rank;
    });
    return {
      rank: HandType.FourOfAKind,
      main: fourOfAKindRank,
    };
  }

  let threeOfAKindRank = CardRank.None;
  let pairRanks: CardRank[] = [];
  if (rankCounts.includes(3)) {
    const threeOfAKindRanks: CardRank[] = [];
    ranks.forEach((count, rank) => {
      if (count === 3) threeOfAKindRanks.push(rank);
    });
    threeOfAKindRank = Math.max(...threeOfAKindRanks);
  }

  if (rankCounts.filter((count) => count >= 2).length) {
    ranks.forEach((count, rank) => {
      if (count >= 2) pairRanks.push(rank);
    });
  }

  if (threeOfAKindRank !== CardRank.None && pairRanks.length > 1) {
    return {
      rank: HandType.FullHouse,
      main: threeOfAKindRank,
      secondaries: pairRanks
        .filter((pr) => pr !== threeOfAKindRank)
        .sort((a, b) => b - a),
    };
  }

  if (threeOfAKindRank !== CardRank.None) {
    return {
      rank: HandType.ThreeOfAKind,
      main: threeOfAKindRank,
      secondaries: hand
        .filter((c) => c.rank !== threeOfAKindRank)
        .map((card) => card.rank)
        .sort((a, b) => b - a)
        .slice(0, 2),
    };
  }

  if (pairRanks.length >= 2) {
    return {
      rank: HandType.TwoPairs,
      main: Math.max(...pairRanks),
      secondaries: pairRanks
        .sort((a, b) => b - a)
        .concat(
          hand
            .filter((card) => !pairRanks.includes(card.rank))
            .map((card) => card.rank)
            .sort((a, b) => b - a)[0]
        ),
    };
  }

  if (pairRanks.length) {
    return {
      rank: HandType.Pair,
      main: pairRanks[0],
      secondaries: hand.map((card) => card.rank).sort((a, b) => b - a),
    };
  }

  return {
    rank: HandType.HighCard,
    main: Math.max(...hand.map((card) => card.rank)),
    secondaries: hand.map((card) => card.rank).sort((a, b) => b - a),
  };
}

function checkStraight(cards: Cards) {
  const sortedCards = cards.sort((a, b) => b.rank - a.rank);
  let straightCards: Cards = [sortedCards[0]];
  for (let i = 1; i < sortedCards.length; i++) {
    if (sortedCards[i].rank === sortedCards[i - 1].rank - 1) {
      straightCards.push(sortedCards[i]);
    } else if (sortedCards[i].rank !== sortedCards[i - 1].rank) {
      straightCards = [sortedCards[i]];
    }
    if (straightCards.length === 5) break;
  }
  // Check for A2345 straight
  if (
    sortedCards[0].rank === CardRank.Ace &&
    straightCards[straightCards.length - 1].rank === CardRank.Two
  ) {
    straightCards.push(sortedCards[0]);
  }
  return straightCards.length >= 5 ? straightCards : null;
}

function compareHandRanks(hand1: HandRank, hand2: HandRank) {
  if (hand1.rank > hand2.rank) return 1;
  if (hand1.rank < hand2.rank) return -1;
  if (hand1.main > hand2.main) return 1;
  if (hand1.main < hand2.main) return -1;
  if (hand1.secondaries && hand2.secondaries) {
    for (let i = 0; i < hand1.secondaries.length; i++) {
      if (hand1.secondaries[i] > hand2.secondaries[i]) return 1;
      if (hand1.secondaries[i] < hand2.secondaries[i]) return -1;
    }
  }
  return 0;
}
