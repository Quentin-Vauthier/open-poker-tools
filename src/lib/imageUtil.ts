import sharp from 'sharp';
export const compressAndConvertToWebp = async (
  buffer: Buffer
): Promise<Buffer> => {
  const sourceImage = sharp(buffer);
  const metadata = await sourceImage.metadata();
  if (metadata.width && metadata.height) {
    const ratio = metadata.width / metadata.height;
    if (metadata.width > 800) {
      return sourceImage
        .resize(800, Math.round(800 / ratio))
        .webp({ quality: 80 })
        .toBuffer();
    }
  }
  return sourceImage.webp({ quality: 80 }).toBuffer();
};
