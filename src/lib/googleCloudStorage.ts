import { Storage, Bucket } from '@google-cloud/storage';
import path from 'path';

const keyFilename = path.join(process.cwd(), process.env.GCS_KEY_FILE);
const bucketName = process.env.GCS_BUCKET_NAME;

// Initialize Google Cloud Storage client
const storage = new Storage({ keyFilename });
const bucket: Bucket = storage.bucket(bucketName);

export const uploadFileToGCS = async (
  buffer: Buffer,
  fileName: string,
  mimeType: string
): Promise<string> => {
  const file = bucket.file(fileName);
  const stream = file.createWriteStream({
    resumable: false,
    contentType: mimeType,
  });

  return new Promise((resolve, reject) => {
    stream.on('error', (err) => reject(err));
    stream.on('finish', () => {
      const publicUrl = `https://storage.googleapis.com/${bucketName}/${fileName}`;
      resolve(publicUrl);
    });
    stream.end(buffer);
  });
};

export const deleteFileFromGCS = async (fileName: string) => {
  const file = bucket.file(fileName);
  const [exists] = await file.exists();
  if (!exists) return;
  await file.delete();
};
