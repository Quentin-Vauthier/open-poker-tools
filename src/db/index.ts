import { drizzle, NeonDatabase } from 'drizzle-orm/neon-serverless';

import { Pool } from '@neondatabase/serverless';

const pool = new Pool({ connectionString: process.env.DATABASE_URL });

declare global {
  var cachedDrizzle: NeonDatabase;
}
let drizzleDb: NeonDatabase;

if (process.env.NODE_ENV === 'production') {
  drizzleDb = drizzle(pool);
} else {
  if (!global.cachedDrizzle) {
    global.cachedDrizzle = drizzle(pool);
  }
  drizzleDb = global.cachedDrizzle;
}

export const db = drizzleDb;
