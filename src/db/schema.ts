import {
  boolean,
  integer,
  pgEnum,
  pgTable,
  primaryKey,
  serial,
  text,
  timestamp,
} from 'drizzle-orm/pg-core';
import { OPTEndpointMethod, OPTEvent } from '@/lib/events/types';
import { enumToPgEnum } from '@/lib/utils';
import { Role, TournamentVisibility } from '@/types';

// Table of users, a user is someone who can log in to the application, a user can be linked to a player
export const roleEnum = pgEnum('role_enum', enumToPgEnum(Role));
export const playersTable = pgTable('players', {
  id: serial('id').primaryKey(),
  email: text('email').notNull().unique(),
  nickname: text('nickname').notNull().unique(),
  password: text('password').notNull(),
  role: roleEnum('role').notNull().default(Role.Player),
  avatar: text('avatar'), // URL of the Google cloud storage object
  createdAt: timestamp('created_at', {
    withTimezone: true,
    mode: 'date',
  }).defaultNow(),
  updatedAt: timestamp('updated_at', {
    withTimezone: true,
    mode: 'date',
  }).defaultNow(),
});
export type InsertPlayer = typeof playersTable.$inferInsert;
export type SelectPlayer = typeof playersTable.$inferSelect;

export const sessionTable = pgTable('sessions', {
  id: text('id').primaryKey(),
  userId: integer('player_id') // Must be named userId for lucia
    .notNull()
    .references(() => playersTable.id),
  expiresAt: timestamp('expires_at', {
    withTimezone: true,
    mode: 'date',
  }).notNull(),
});
export type InsertSession = typeof sessionTable.$inferInsert;
export type SelectSession = typeof sessionTable.$inferSelect;

// Table of locations, a location is a place where a tournament can be held
export const locationsTable = pgTable('locations', {
  id: serial('id').primaryKey(),
  name: text('name').unique().notNull(),
  address: text('address').notNull(),
  city: text('city'),
  zip: text('zip'),
  createdAt: timestamp('created_at', {
    withTimezone: true,
    mode: 'date',
  }).defaultNow(),
  updatedAt: timestamp('updated_at', {
    withTimezone: true,
    mode: 'date',
  }).defaultNow(),
});
export type InsertLocation = typeof locationsTable.$inferInsert;
export type SelectLocation = typeof locationsTable.$inferSelect;

// Table of levels
export const levelsTable = pgTable(
  'levels',
  {
    structureId: integer('structure_id')
      .notNull()
      .references(() => structuresTable.id),
    position: integer('position').notNull(),
    sb: integer('sb').notNull(),
    bb: integer('bb').notNull(),
    ante: integer('ante').notNull().default(0),
    isBreak: boolean('break').notNull().default(false),
    duration: integer('duration').notNull(),
    createdAt: timestamp('created_at', {
      withTimezone: true,
      mode: 'date',
    }).defaultNow(),
    updatedAt: timestamp('updated_at', {
      withTimezone: true,
      mode: 'date',
    }).defaultNow(),
  },
  (table) => [primaryKey({ columns: [table.structureId, table.position] })]
);
export type InsertLevel = typeof levelsTable.$inferInsert;
export type SelectLevel = typeof levelsTable.$inferSelect;

// Table of structures, a structure is a group of levels
export const structuresTable = pgTable('structures', {
  id: serial('id').primaryKey(),
  name: text('name').unique().notNull(),
  createdAt: timestamp('created_at', {
    withTimezone: true,
    mode: 'date',
  }).defaultNow(),
  updatedAt: timestamp('updated_at', {
    withTimezone: true,
    mode: 'date',
  }).defaultNow(),
});
export type InsertStructure = typeof structuresTable.$inferInsert;
export type SelectStructure = typeof structuresTable.$inferSelect;

// Series table, a series is a group of tournaments
export const seriesTable = pgTable('series', {
  id: serial('id').primaryKey(),
  name: text('name').unique().notNull(),
  locationId: integer('location_id').references(() => locationsTable.id),
  createdAt: timestamp('created_at', {
    withTimezone: true,
    mode: 'date',
  }).defaultNow(),
  updatedAt: timestamp('updated_at', {
    withTimezone: true,
    mode: 'date',
  }).defaultNow(),
});
export type InsertSeries = typeof seriesTable.$inferInsert;
export type SelectSeries = typeof seriesTable.$inferSelect;

// Table of players list of a series
export const seriesPlayersTable = pgTable(
  'series_player',
  {
    seriesId: integer('series_id')
      .notNull()
      .references(() => seriesTable.id),
    playerId: integer('player_id')
      .notNull()
      .references(() => playersTable.id),
    isModerator: boolean('is_moderator').notNull().default(false),
    createdAt: timestamp('created_at', {
      withTimezone: true,
      mode: 'date',
    }).defaultNow(),
  },
  (table) => [primaryKey({ columns: [table.seriesId, table.playerId] })]
);
export type InsertSeriesPlayer = typeof seriesPlayersTable.$inferInsert;
export type SelectSeriesPlayer = typeof seriesPlayersTable.$inferSelect;

export const tournamentVisibilityEnum = pgEnum(
  'tournament_visibility_enum',
  enumToPgEnum(TournamentVisibility)
);
export const tournamentsTable = pgTable('tournaments', {
  id: serial('id').primaryKey(),
  visibility: tournamentVisibilityEnum('visibility')
    .notNull()
    .default(TournamentVisibility.NotListed),
  name: text('name').notNull(),
  date: timestamp('date', { withTimezone: true, mode: 'date' }).notNull(),
  description: text('description'),
  waitlist: boolean('waitlist').notNull().default(true),
  startTime: timestamp('start_time', { withTimezone: true, mode: 'date' }),
  endTime: timestamp('end_time', { withTimezone: true, mode: 'date' }),
  buyIn: integer('buy_in').notNull().default(0),
  knockout: boolean('knockout').notNull().default(false),
  progressiveKo: boolean('progressive_ko').notNull().default(false),
  knockoutBounty: integer('knockout_bounty').notNull().default(0),
  canReentry: boolean('can_reentry').notNull().default(false),
  reentryEnd: integer('reentry_end').default(4), // Number of levels
  canLateReg: boolean('can_late_reg').notNull().default(true),
  lateRegEnd: integer('late_reg_end').default(4), // Number of levels
  tableSize: integer('table_size').notNull().default(9),
  minTableSize: integer('min_table_size').notNull().default(3),
  maxPlayers: integer('max_players').notNull().default(0),
  variant: text('variant').notNull().default('NLHE'),
  startingStack: integer('starting_stack').notNull().default(10_000),
  finalTableSize: integer('final_table_size').notNull().default(9),
  seriesId: integer('series_id').references(() => seriesTable.id),
  locationId: integer('location_id').references(() => locationsTable.id),
  structureId: integer('structure_id').references(() => structuresTable.id),
  createdAt: timestamp('created_at', {
    withTimezone: true,
    mode: 'date',
  }).defaultNow(),
  updatedAt: timestamp('updated_at', {
    withTimezone: true,
    mode: 'date',
  }).defaultNow(),
});
export type InsertTournament = typeof tournamentsTable.$inferInsert;
export type SelectTournament = typeof tournamentsTable.$inferSelect;

// Table of players list of a tournament
export const tournamentPlayersTable = pgTable(
  'tournament_player',
  {
    tournamentId: integer('tournament_id')
      .notNull()
      .references(() => tournamentsTable.id),
    playerId: integer('player_id')
      .notNull()
      .references(() => playersTable.id),
    isModerator: boolean('is_moderator').notNull().default(false),
    createdAt: timestamp('created_at', {
      withTimezone: true,
      mode: 'date',
    }).defaultNow(),
  },
  (table) => [primaryKey({ columns: [table.tournamentId, table.playerId] })]
);
export type InsertTournamentPlayer = typeof tournamentPlayersTable.$inferInsert;
export type SelectTournamentPlayer = typeof tournamentPlayersTable.$inferSelect;

export const tournamentRankingsTable = pgTable(
  'tournament_ranking',
  {
    tournamentId: integer('tournament_id')
      .notNull()
      .references(() => tournamentsTable.id),
    playerId: integer('player_id')
      .notNull()
      .references(() => playersTable.id),
    position: integer('position'),
    prize: integer('prize'),
    bounty: integer('bounty'),
    koCount: integer('ko_count'),
    createdAt: timestamp('created_at', {
      withTimezone: true,
      mode: 'date',
    }).defaultNow(),
    updatedAt: timestamp('updated_at', {
      withTimezone: true,
      mode: 'date',
    }).defaultNow(),
  },
  (table) => [primaryKey({ columns: [table.tournamentId, table.playerId] })]
);
export type InsertTournamentRanking =
  typeof tournamentRankingsTable.$inferInsert;
export type SelectTournamentRanking =
  typeof tournamentRankingsTable.$inferSelect;

const tournamentPrizesTable = pgTable(
  'tournament_prize',
  {
    tournamentId: integer('tournament_id')
      .notNull()
      .references(() => tournamentsTable.id),
    position: integer('position').notNull(),
    prize: integer('prize').notNull(),
    createdAt: timestamp('created_at', {
      withTimezone: true,
      mode: 'date',
    }).defaultNow(),
    updatedAt: timestamp('updated_at', {
      withTimezone: true,
      mode: 'date',
    }).defaultNow(),
  },
  (table) => [primaryKey({ columns: [table.tournamentId, table.position] })]
);
export type InsertTournamentPrize = typeof tournamentPrizesTable.$inferInsert;
export type SelectTournamentPrize = typeof tournamentPrizesTable.$inferSelect;

// Table of events, an event is something that happens in the application
export const eventCodeEnum = pgEnum('event_code_enum', enumToPgEnum(OPTEvent));
export const eventsTable = pgTable('event', {
  code: eventCodeEnum('code').primaryKey(),
  title: text('title').notNull(),
  description: text('description'),
});
export type InsertEvent = typeof eventsTable.$inferInsert;
export type SelectEvent = typeof eventsTable.$inferSelect;

// Table of notifications, a notification is a type of message that can be sent to a user
export const notificationsTable = pgTable('notification', {
  code: text('code').primaryKey(),
  eventCode: eventCodeEnum('event_code')
    .notNull()
    .references(() => eventsTable.code),
  title: text('title').notNull(),
  description: text('description'),
  subject: text('subject').notNull(),
  message: text('message').notNull(),
  html: text('html').notNull(),
  color: text('color'),
});
export type InsertNotification = typeof notificationsTable.$inferInsert;
export type SelectNotification = typeof notificationsTable.$inferSelect;

// Table of notification endpoints, a notification endpoint is a way to send a notification to a user
export const endpointMethodEnum = pgEnum(
  'endpoint_method_enum',
  enumToPgEnum(OPTEndpointMethod)
);
export const notificationEndpointsTable = pgTable('notification_endpoint', {
  id: serial('id').primaryKey(),
  playerId: integer('player_id')
    .notNull()
    .references(() => playersTable.id),
  method: endpointMethodEnum('method').notNull(),
  endpoint: text('endpoint').notNull(),
  name: text('name').notNull(),
  active: boolean('active').notNull().default(true),
  createdAt: timestamp('created_at', {
    withTimezone: true,
    mode: 'date',
  }).defaultNow(),
  updatedAt: timestamp('updated_at', {
    withTimezone: true,
    mode: 'date',
  }).defaultNow(),
});
export type InsertNotificationEndpoint =
  typeof notificationEndpointsTable.$inferInsert;
export type SelectNotificationEndpoint =
  typeof notificationEndpointsTable.$inferSelect;

// Table of notification subscriptions, a notification subscription is a way to subscribe to a notification
export const notificationSubscriptionsTable = pgTable(
  'notification_subscription',
  {
    notificationCode: text('notif_code').references(
      () => notificationsTable.code,
      { onUpdate: 'cascade', onDelete: 'cascade' }
    ),
    endpointId: integer('endpoint_id').references(
      () => notificationEndpointsTable.id
    ),
    objectId: text('object_id'), // No foreign key because it can be anything, nullable because it can be a global subscription
    active: boolean('active').notNull().default(true),
    createdAt: timestamp('created_at', {
      withTimezone: true,
      mode: 'date',
    }).defaultNow(),
  },
  (table) => [
    primaryKey({
      columns: [table.notificationCode, table.endpointId, table.objectId],
    }),
  ]
);
export type InsertNotificationSubscription =
  typeof notificationSubscriptionsTable.$inferInsert;
export type SelectNotificationSubscription =
  typeof notificationSubscriptionsTable.$inferSelect;

export const tournamentInvitationsTable = pgTable(
  'tournament_invitation',
  {
    tournamentId: integer('tournament_id')
      .notNull()
      .references(() => tournamentsTable.id),
    playerId: integer('player_id')
      .notNull()
      .references(() => playersTable.id),
    invitedBy: integer('invited_by').references(() => playersTable.id),
    invitedAt: timestamp('invited_at', {
      withTimezone: true,
      mode: 'date',
    }).defaultNow(),
  },
  (table) => [primaryKey({ columns: [table.tournamentId, table.playerId] })]
);
export type InsertTournamentInvitation =
  typeof tournamentInvitationsTable.$inferInsert;
export type SelectTournamentInvitation =
  typeof tournamentInvitationsTable.$inferSelect;
