CREATE TYPE "public"."endpoint_method_enum" AS ENUM('email', 'discord_dm');--> statement-breakpoint
CREATE TYPE "public"."event_code_enum" AS ENUM('g_tournament_win', 'g_tournament_create', 'tournament_start', 'tournament_end', 'g_player_created', 'g_player_deleted', 'player_join_tournament', 'player_leave_tournament', 'player_win_tournament', 'player_eliminated_from_tournament', 'tournament_player_win', 'tournament_player_eliminated', 'tournament_player_waitlist_update', 'g_user_created', 'g_user_deleted');--> statement-breakpoint
CREATE TYPE "public"."role_enum" AS ENUM('admin', 'player');--> statement-breakpoint
CREATE TYPE "public"."tournament_visibility_enum" AS ENUM('public', 'not_listed', 'private');--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "event" (
	"code" "event_code_enum" PRIMARY KEY NOT NULL,
	"title" text NOT NULL,
	"description" text
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "levels" (
	"structure_id" integer NOT NULL,
	"position" integer NOT NULL,
	"sb" integer NOT NULL,
	"bb" integer NOT NULL,
	"ante" integer DEFAULT 0 NOT NULL,
	"break" boolean DEFAULT false NOT NULL,
	"duration" integer NOT NULL,
	"created_at" timestamp with time zone DEFAULT now(),
	"updated_at" timestamp with time zone DEFAULT now(),
	CONSTRAINT "levels_structure_id_position_pk" PRIMARY KEY("structure_id","position")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "locations" (
	"id" serial PRIMARY KEY NOT NULL,
	"name" text NOT NULL,
	"address" text NOT NULL,
	"city" text,
	"zip" text,
	"created_at" timestamp with time zone DEFAULT now(),
	"updated_at" timestamp with time zone DEFAULT now(),
	CONSTRAINT "locations_name_unique" UNIQUE("name")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "notification_endpoint" (
	"id" serial PRIMARY KEY NOT NULL,
	"player_id" integer NOT NULL,
	"method" "endpoint_method_enum" NOT NULL,
	"endpoint" text NOT NULL,
	"name" text NOT NULL,
	"active" boolean DEFAULT true NOT NULL,
	"created_at" timestamp with time zone DEFAULT now(),
	"updated_at" timestamp with time zone DEFAULT now()
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "notification_subscription" (
	"notif_code" text,
	"endpoint_id" integer,
	"object_id" text,
	"active" boolean DEFAULT true NOT NULL,
	"created_at" timestamp with time zone DEFAULT now(),
	CONSTRAINT "notification_subscription_notif_code_endpoint_id_object_id_pk" PRIMARY KEY("notif_code","endpoint_id","object_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "notification" (
	"code" text PRIMARY KEY NOT NULL,
	"event_code" "event_code_enum" NOT NULL,
	"title" text NOT NULL,
	"description" text,
	"subject" text NOT NULL,
	"message" text NOT NULL,
	"html" text NOT NULL,
	"color" text
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "players" (
	"id" serial PRIMARY KEY NOT NULL,
	"email" text NOT NULL,
	"nickname" text NOT NULL,
	"password" text NOT NULL,
	"role" "role_enum" DEFAULT 'player' NOT NULL,
	"avatar" text,
	"created_at" timestamp with time zone DEFAULT now(),
	"updated_at" timestamp with time zone DEFAULT now(),
	CONSTRAINT "players_email_unique" UNIQUE("email"),
	CONSTRAINT "players_nickname_unique" UNIQUE("nickname")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "series_player" (
	"series_id" integer NOT NULL,
	"player_id" integer NOT NULL,
	"is_moderator" boolean DEFAULT false NOT NULL,
	"created_at" timestamp with time zone DEFAULT now(),
	CONSTRAINT "series_player_series_id_player_id_pk" PRIMARY KEY("series_id","player_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "series" (
	"id" serial PRIMARY KEY NOT NULL,
	"name" text NOT NULL,
	"location_id" integer,
	"created_at" timestamp with time zone DEFAULT now(),
	"updated_at" timestamp with time zone DEFAULT now(),
	CONSTRAINT "series_name_unique" UNIQUE("name")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "sessions" (
	"id" text PRIMARY KEY NOT NULL,
	"player_id" integer NOT NULL,
	"expires_at" timestamp with time zone NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "structures" (
	"id" serial PRIMARY KEY NOT NULL,
	"name" text NOT NULL,
	"created_at" timestamp with time zone DEFAULT now(),
	"updated_at" timestamp with time zone DEFAULT now(),
	CONSTRAINT "structures_name_unique" UNIQUE("name")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "tournament_player" (
	"tournament_id" integer NOT NULL,
	"player_id" integer NOT NULL,
	"is_moderator" boolean DEFAULT false NOT NULL,
	"created_at" timestamp with time zone DEFAULT now(),
	CONSTRAINT "tournament_player_tournament_id_player_id_pk" PRIMARY KEY("tournament_id","player_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "tournament_ranking" (
	"tournament_id" integer NOT NULL,
	"player_id" integer NOT NULL,
	"position" integer,
	"prize" integer,
	"bounty" integer,
	"ko_count" integer,
	"created_at" timestamp with time zone DEFAULT now(),
	"updated_at" timestamp with time zone DEFAULT now(),
	CONSTRAINT "tournament_ranking_tournament_id_player_id_pk" PRIMARY KEY("tournament_id","player_id")
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "tournaments" (
	"id" serial PRIMARY KEY NOT NULL,
	"visibility" "tournament_visibility_enum" DEFAULT 'not_listed' NOT NULL,
	"name" text NOT NULL,
	"date" timestamp with time zone NOT NULL,
	"waitlist" boolean DEFAULT true NOT NULL,
	"start_time" timestamp with time zone,
	"end_time" timestamp with time zone,
	"buy_in" integer DEFAULT 0 NOT NULL,
	"knockout" boolean DEFAULT false NOT NULL,
	"progressive_ko" boolean DEFAULT false NOT NULL,
	"knockout_bounty" integer DEFAULT 0 NOT NULL,
	"can_reentry" boolean DEFAULT false NOT NULL,
	"reentry_end" integer DEFAULT 4,
	"can_late_reg" boolean DEFAULT true NOT NULL,
	"late_reg_end" integer DEFAULT 4,
	"table_size" integer DEFAULT 9 NOT NULL,
	"min_table_size" integer DEFAULT 3 NOT NULL,
	"max_players" integer DEFAULT 0 NOT NULL,
	"variant" text DEFAULT 'NLHE' NOT NULL,
	"starting_stack" integer DEFAULT 10000 NOT NULL,
	"final_table_size" integer DEFAULT 9 NOT NULL,
	"series_id" integer,
	"location_id" integer,
	"structure_id" integer,
	"created_at" timestamp with time zone DEFAULT now(),
	"updated_at" timestamp with time zone DEFAULT now()
);
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "levels" ADD CONSTRAINT "levels_structure_id_structures_id_fk" FOREIGN KEY ("structure_id") REFERENCES "public"."structures"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "notification_endpoint" ADD CONSTRAINT "notification_endpoint_player_id_players_id_fk" FOREIGN KEY ("player_id") REFERENCES "public"."players"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "notification_subscription" ADD CONSTRAINT "notification_subscription_notif_code_notification_code_fk" FOREIGN KEY ("notif_code") REFERENCES "public"."notification"("code") ON DELETE cascade ON UPDATE cascade;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "notification_subscription" ADD CONSTRAINT "notification_subscription_endpoint_id_notification_endpoint_id_fk" FOREIGN KEY ("endpoint_id") REFERENCES "public"."notification_endpoint"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "notification" ADD CONSTRAINT "notification_event_code_event_code_fk" FOREIGN KEY ("event_code") REFERENCES "public"."event"("code") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "series_player" ADD CONSTRAINT "series_player_series_id_series_id_fk" FOREIGN KEY ("series_id") REFERENCES "public"."series"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "series_player" ADD CONSTRAINT "series_player_player_id_players_id_fk" FOREIGN KEY ("player_id") REFERENCES "public"."players"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "series" ADD CONSTRAINT "series_location_id_locations_id_fk" FOREIGN KEY ("location_id") REFERENCES "public"."locations"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "sessions" ADD CONSTRAINT "sessions_player_id_players_id_fk" FOREIGN KEY ("player_id") REFERENCES "public"."players"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "tournament_player" ADD CONSTRAINT "tournament_player_tournament_id_tournaments_id_fk" FOREIGN KEY ("tournament_id") REFERENCES "public"."tournaments"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "tournament_player" ADD CONSTRAINT "tournament_player_player_id_players_id_fk" FOREIGN KEY ("player_id") REFERENCES "public"."players"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "tournament_ranking" ADD CONSTRAINT "tournament_ranking_tournament_id_tournaments_id_fk" FOREIGN KEY ("tournament_id") REFERENCES "public"."tournaments"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "tournament_ranking" ADD CONSTRAINT "tournament_ranking_player_id_players_id_fk" FOREIGN KEY ("player_id") REFERENCES "public"."players"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "tournaments" ADD CONSTRAINT "tournaments_series_id_series_id_fk" FOREIGN KEY ("series_id") REFERENCES "public"."series"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "tournaments" ADD CONSTRAINT "tournaments_location_id_locations_id_fk" FOREIGN KEY ("location_id") REFERENCES "public"."locations"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "tournaments" ADD CONSTRAINT "tournaments_structure_id_structures_id_fk" FOREIGN KEY ("structure_id") REFERENCES "public"."structures"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
