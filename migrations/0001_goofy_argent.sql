CREATE TABLE IF NOT EXISTS "tournament_invitation" (
	"tournament_id" integer NOT NULL,
	"player_id" integer NOT NULL,
	"invited_by" integer,
	"invited_at" timestamp with time zone DEFAULT now(),
	CONSTRAINT "tournament_invitation_tournament_id_player_id_pk" PRIMARY KEY("tournament_id","player_id")
);
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "tournament_invitation" ADD CONSTRAINT "tournament_invitation_tournament_id_tournaments_id_fk" FOREIGN KEY ("tournament_id") REFERENCES "public"."tournaments"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "tournament_invitation" ADD CONSTRAINT "tournament_invitation_player_id_players_id_fk" FOREIGN KEY ("player_id") REFERENCES "public"."players"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "tournament_invitation" ADD CONSTRAINT "tournament_invitation_invited_by_players_id_fk" FOREIGN KEY ("invited_by") REFERENCES "public"."players"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
