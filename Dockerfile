FROM node:latest AS base
WORKDIR /usr/src/app

FROM base AS install
RUN mkdir -p /temp/dev
COPY package.json /temp/dev/
RUN cd /temp/dev && yarn install

RUN mkdir -p /temp/prod
COPY package.json /temp/prod/
RUN cd /temp/prod && yarn install --production

FROM base AS build
COPY --from=install /temp/dev/node_modules ./node_modules
COPY . .
RUN yarn build

ENV NODE_ENV=production

# copy production dependencies and source code into final image
FROM base AS release
COPY --from=install /temp/prod/node_modules node_modules
COPY --from=build /usr/src/app/.next/ ./.next
COPY --from=build /usr/src/app/package.json .

ENV NEXT_TELEMETRY_DISABLED 1

# run the app
EXPOSE 3000/tcp
ENTRYPOINT [ "npm", "start" ]

