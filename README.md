# Open Poker Tools

## Introduction

The goal of this website is to provide tools for managing poker tournaments.

It is a work in progress and is being developed by a single person at the moment.

The project is open source and contributions are welcome.

## Features

- Tournament manager
- Equity calculator
- Hand tracker

## Technologies

- [Next.js](https://nextjs.org/)
- [TypeScript](https://www.typescriptlang.org/)
- [Tailwind CSS](https://tailwindcss.com/)
- [PostgreSQL](https://www.postgresql.org/)
- [Drizzle ORM](https://orm.drizzle.team)

## Contributing

If you would like to contribute to this project, please read the [CONTRIBUTING.md](CONTRIBUTING.md) file.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
